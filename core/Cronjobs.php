<?php

namespace Cronjobs;

class Cronjobs extends \ControllerExtended {

  private $description = null;
  private $logging_globally = true;
  protected $logging_each = true;
  private $logfile = null;
  protected $key;

  public function __construct($init = true) {
    if ($init === true) {
      /* used this trick to avoid constructing any cronjob class
        to retrieve the $description property in the cronjobs settings page. See Cronjobs cp controller. */
      parent::__construct();
      $this->key = $this->key ?? env('cronjob_key');
      $this->logfile = strtolower(explode('\\', get_class($this))[1]);

      if (!isset(getallheaders()['x-cronjob-key'])) {
        die("Error: Please provide a valid Cronjob key.");
      }
      else if ($this->key != getallheaders()['x-cronjob-key']) {
        die("Error: Invalid Cronjob key.");
      }
      else {
        // passed
        if ($this->logging_globally) {
          $this->_log("'{$this->current_path}' triggered.", 'info', 'cronjobs');
        }
      }
    }
  }

  protected function logger(string $message, string $level, string|null $filename = null, array|object $array = []) {
    if ($this->logging_each) {
      $this->_log($message, $level, $filename, $array);
    }
  }

  private function _log(string $message, string $level, string|null $filename = null, array|object $array = []) {
    /**
     * LogLevel::EMERGENCY => 0,
     * LogLevel::ALERT     => 1,
     * LogLevel::CRITICAL  => 2,
     * LogLevel::ERROR     => 3,
     * LogLevel::WARNING   => 4,
     * LogLevel::NOTICE    => 5,
     * LogLevel::INFO      => 6,
     * LogLevel::DEBUG     => 7
     */

    $filename = $filename ?? $this->logfile;

    if (filesize(".logs/{$filename}.log") >= 9*1024*1024) {
      // 9MB is the maximum log file size
      $now = date('Y-m-d--H-i-s');
      mkdir(directory: ".backups/logs/{$filename}/", recursive: true);
      rename(".logs/{$filename}.log", ".backups/logs/{$filename}/{$filename}_{$now}.log");
    }

    $logger = new \Katzgrau\KLogger\Logger(
      logDirectory: '.logs',
      options: [
        'extension'   => 'log',
        'filename'    => "{$filename}.log",
        'dateFormat'  => 'Y-m-d H:i:s',
      ]
    );

    $logger->{$level}($message, $array);
  }

  public function set_description($description) {
    $this->description = $description;
  }

  public function get_description() {
    return $this->description;
  }
}