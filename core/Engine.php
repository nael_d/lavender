<?php

if (!function_exists('array_key_first')) {
  function array_key_first(array $arr) {
    foreach($arr as $key => $unused) {
      return $key;
    }
    return null;
  }
}

function select(array $select) {
  $cells    = $select['cells'] ?? '*';
  $table    = $select['table'] ?? '';
  $joins    = $select['joins'] ?? '';
  $where    = $select['where'] ?? '';
  $having   = $select['having'] ?? '';
  $group_by = $select['group_by'] ?? '';
  $order_by = $select['order_by'] ?? '';
  $limit    = $select['limit'] ?? '';
  $query    = 'select ';

  $query .= (is_array($cells) ? implode(', ', $cells) : $cells);
  $query .= ' from ' . (is_array($table) ? implode(', ', $table) : $table) . ' ';

  if (is_array($joins)) {
    $j = '';

    foreach ($joins as $relation => $structure) {
      foreach ($structure as $k => $relation_map) {
        $j .= "{$relation} {$relation_map['joined_table']} on ";

        foreach ($relation_map['join_case'] as $base_table_key => $related_table_key) {
          $j .= "{$base_table_key} " . ($relation_map['join_factor'] ?? '=') . " {$related_table_key} ";
        }
      }
    }

    $query .= "{$j} ";
  }
  else {
    $query .= "{$joins} ";
  }

  if (is_array($where)) {
    $w = '';
    $i = 0;

    foreach ($where as $key => $value) {
      $i++;

      if (!is_array($value)) {
        // it's just a normal key=>value where clause
        $w .= $key . '=' . (is_numeric($value) ? $value : "'{$value}'");
      }
      else if (count($value) > 0) {
        // value is array, the 'value' has more options
        if (!isset($value['factor'])) {
          // it is just a normal array with multi values.
          $w .= "{$key} in (" . implode(', ', array_map(fn($v) => "'{$v}'", $value)) . ")";
        }
        else {
          // it is an array with some pro options...
          // $next   = $value['next_criteria'] != '' ? " {$value['next_criteria']} " : ' and ';
          $factor = $value['factor'] ?? '=';
          
          $w .= "{$key}{$factor}{$value['value']}"/* . ($i < count($where) ? $next : '')*/;
        }
      }

      $w .= $i < count($where) ? ' and ' : ''; // each where appended with conditioned 'and'
    }

    $query .= "where {$w} ";
  }
  else {
    $query .= "where {$where} ";
  }

  $query .= $group_by ? " group by {$group_by} " : '';
  $query .= $having ? "having {$having} " : '';

  if ($order_by) {
    if (is_array($order_by)) {
      $i = 0;
      $o = 'order by ';

      foreach ($order_by as $column => $order) {
        $i++;
        $o .= "{$column} {$order}" . ($i < count($order_by) ? ', ' : '');
      }

      $query .= $o;
    }
    else {
      $query .= "order by {$order_by}";
    }
  }

  $query .= $limit != '' ? (" limit " . (is_array($limit) ? $limit[0] : $limit)) : '';

  return $query;
}

function insert(array $insert) {
  $table  = $insert['table'] ?? '';
  $data   = $insert['data'] ?? '';
  $names  = '';
  $values = '';
  $i      = 0;

  foreach ($data as $name => $value) {
    $i++;
    $names  .= $name . ($i < count($data) ? ', ' : '');
    $values .= "'{$value}'" . ($i < count($data) ? ', ' : '');
  }

  return "insert into {$table} ({$names}) values ({$values})";
}

function update(array $update) {
  $i      = 0;
  $table  = $update['table'] ?? '';
  $data   = $update['data'] ?? '';
  $where  = $update['where'] ?? '';
  $query  = "update {$table} set ";

  foreach ($data as $key => $datum) {
    $i++;
    $query .= "{$key}=" . (is_numeric($datum)
      ? $datum
      : (is_array($datum) ? implode(', ', $datum) : "'{$datum}'") . ($i < count($data) ? ", " : ''));
  }

  $i = 0;
  $query .= ' where ';

  if (is_array($where)) {
    foreach ($where as $key => $value) {
      if (is_array($value)) {
        $query .= "{$key} in (" . implode(', ', array_map(fn($v) => "'{$v}'", $value)) . ")";
      }
      else {
        $i++;
        $query .= "{$key}='{$value}'" . ($i < count($where) ? ' and ' : '');
      }
    }
  }
  else {
    $query .= $where;
  }

  return $query;
}

function delete(array $delete) {
  // this is a logical deletion using {table}_is_deleted column to be set to 1 instead of 0
  $table        = $delete['table'] ?? '';
  $deleted_key  = $delete['deleted_key'] ?? '';
  $where        = $delete['where'] ?? '';

  return update([
    'table' => $table,
    'data'  => [' ' . $deleted_key => 1,],
    'where' => $where,
  ]);
}

