<?php

class Cache {
  /**
   * @var string Cache directory path.
   */
  private static $cacheDirectory = '.cache/';

  /**
   * @var string Namespace for cache keys.
   */
  private static $namespace = '';

  /**
   * @var int Default time-to-live for cache entries (in seconds). Default is 300 seconds.
   */
  private static $defaultTtl = 300;

  /**
   * @var int Default grace period for cache entries (in seconds). Default is 0 seconds.
   */
  private static $gracePeriod = 0;

  /**
   * @var bool Caching modes: true => using file-based; false => using in-memory.
   */
  private static $enabled = true;

  /**
   * @var array In-memory cache array.
   */
  private static $inMemoryCache = [];

  /**
   * @var array Array with built-in events.
   */
  private static $builtInEvents = [
    'cache_set'               => null,
    'cache_clear'             => null,
    'cache_truncate'          => null,
    'cache_clear_by_tag'      => null,
    'cache_event_registered'  => null,
  ];

  /**
   * @var array Array to store registered events and their callbacks.
   */
  private static $events = [];

  /**
   * Enable or disable the cache.
   *
   * @param bool $enabled Enabled to use file-based driver, or Disabled to use in-memory driver.
   * @return bool
   */
  public static function enable(bool $enabled = true) {
    self::$enabled = $enabled;
    return true;
  }

  /**
   * Check the caching driver status.
   *
   * @return bool True if file-based driver is enabled, False for the in-memory driver.
   */
  public static function isEnabled() {
    return self::$enabled;
  }

  /**
   * Set the namespace for cache keys.
   *
   * @param string $namespace The namespace to set
   * @return bool
   */
  public static function setNamespace(string $namespace) {
    self::$namespace = $namespace;
    return true;
  }

  /**
   * Set the default time-to-live (TTL) in seconds.
   *
   * @param int $seconds The TTL in seconds.
   * @return bool
   */
  public static function setDefaultTtl(int $seconds) {
    self::$defaultTtl = $seconds;
    return true;
  }

  /**
   * Set the default grace period in seconds.
   *
   * @param int $seconds The grace period in seconds.
   * @return bool
   */
  public static function setGracePeriod(int $seconds) {
    self::$gracePeriod = max(0, (int) $seconds);
    return true;
  }

  /**
   * Get the default grace period in seconds.
   *
   * @return int The default grace period.
   */
  public static function getGracePeriod() {
    return self::$gracePeriod;
  }

  /**
   * Store data in the cache.
   *
   * @param string $key The cache key.
   * @param mixed $value The data to cache.
   * @param int|null $ttl The time-to-live in seconds.
   * @param string|null $version The version of the cache entry.
   * @param array $tags The tags associated with the cache entry.
   * @return bool
   */
  public static function set(string $key, mixed $value, int|null $ttl = null, string|null $version = null, array $tags = []) {
    if (!self::isEnabled()) {
      // Cache is disabled, store in-memory
      self::$inMemoryCache[$key] = $value;
      return true;
    }
    else {
      /**
       * file-based is chosen, no need to destroy the $inMemoryCache array
       * because it may contain data while using file-based driver temporarily.
       */ 
      // self::$inMemoryCache = [];
      if (!is_dir(self::$cacheDirectory)) {
        mkdir(self::$cacheDirectory, 0755, true);
      }
    }

    $cacheFile = self::getCacheFilePath($key);
    $expirationTime = time() + ($ttl ?? self::$defaultTtl);
    $data = ['value' => $value, 'expiration' => $expirationTime, 'version' => $version, 'tags' => $tags];
    file_put_contents($cacheFile, serialize($data), LOCK_EX);

    // Trigger cache set event
    self::triggerEvent('cache_set', $key);

    return true;
  }

  /**
   * Retrieve data from the cache.
   *
   * @param string $key The cache key.
   * @param string|null $expectedVersion The expected version of the cache entry.
   * @param int|null $gracePeriod The grace period in seconds.
   * @return mixed The cached data or null if not found.
   */
  public static function get(string $key, string|null $expectedVersion = null, int|null $gracePeriod = null) {
    if (!self::isEnabled()) {
      // Cache is disabled, check in-memory
      return self::$inMemoryCache[$key] ?? null;
    }
    else {
      /**
       * file-based is chosen, no need to destroy the $inMemoryCache array
       * because it may contain data while using file-based driver temporarily.
       */ 
      // self::$inMemoryCache = [];
    }

    $cacheFile = self::getCacheFilePath($key);

    // Use the specified grace period or the default grace period
    $grace = ($gracePeriod !== null) ? max(0, (int) $gracePeriod) : self::$gracePeriod;

    if (file_exists($cacheFile)) {
      $data = unserialize(file_get_contents($cacheFile));

      // Check if the expected version matches the stored version
      if ($expectedVersion !== null && isset($data['version']) && $data['version'] !== $expectedVersion) {
        // Data is outdated, but force refresh is not requested
        // Return the outdated data for now if within the grace period
        if ($grace > 0 && (time() < ($data['expiration'] + $grace))) {
          return $data['value'];
        }
        else {
          // Data is beyond the grace period, remove expired cache file
          unlink($cacheFile);
          // Return null
          return null;
        }
      }

      if (time() < $data['expiration']) {
        // Return the cached data
        return $data['value'];
      }
      else {
        // Remove expired cache file
        unlink($cacheFile);
      }
    }

    // Return null because here means that the $key is not found in cache
    return null;
  }

  /**
   * Set the cache and then get the cached data.
   *
   * @param string $key The cache key.
   * @param mixed $value The data to cache.
   * @param int|null $ttl Time-to-live in seconds.
   * @param string|null $version Optional version for cache invalidation.
   * @param array $tags Optional tags for cache management.
   * @return mixed The cached data.
   */
  public static function setThenGet(string $key, mixed $value, int|null $ttl = null, string|null $version = null, array $tags = []) {
    self::set($key, $value, $ttl, $version, $tags);
    return self::get($key);
  }

  public static function getOrSet(string $key, mixed $value, int|null $ttl = null, string|null $version = null, array $tags = [], int|null $gracePeriod = null) {
    return self::get($key, $version, $gracePeriod)
      ?? self::setThenGet($key, is_callable($value) ? $value() : $value, $ttl, $version, $tags);
  }

  /**
   * Clear a specific cache entry.
   *
   * @param string $key The cache key.
   * @return bool
   */
  public static function clear(string $key) {
    $cacheFile = self::getCacheFilePath($key);
    unlink($cacheFile);

    // Trigger cache clear event
    self::triggerEvent('cache_clear', $key);

    return true;
  }

  /**
   * Clear all cache entries.
   *
   * @return bool
   */
  public static function clearAll() {
    $cacheFiles = [
      ...glob(self::$cacheDirectory . '*.cache'),
      ...glob(self::$cacheDirectory . '**/*.cache'),
      ...glob(self::$cacheDirectory . '**/*/*.cache'),
    ];

    foreach ($cacheFiles as $cacheFile) {
      unlink($cacheFile);
    }

    self::triggerEvent('cache_truncate');

    return true;
  }

  /**
   * Clear cache entries by tag.
   *
   * @param string|array $tags The tags to clear.
   * @return bool
   */
  public static function clearByTag(string|array $tags) {
    $cacheFiles = [
      ...glob(self::$cacheDirectory . '*.cache'),
      ...glob(self::$cacheDirectory . '**/*.cache'),
      ...glob(self::$cacheDirectory . '**/*/*.cache'),
    ];

    if (!is_array($tags)) {
      $tags = [$tags]; // Convert a single tag to an array for consistency
    }

    foreach ($cacheFiles as $key => $cacheFile) {
      $data = unserialize(file_get_contents($cacheFile));
      if (isset($data['tags']) && array_intersect($tags, $data['tags'])) {
        unlink($cacheFile);
        self::triggerEvent('cache_clear_by_tag', $tags[$key]);
      }
    }

    return true;
  }

  /**
   * Get the full path to the cache file based on the key.
   *
   * @param string $key The cache key.
   * @return string The full path to the cache file.
   */
  private static function getCacheFilePath(string $key) {
    // Check if the namespace contains "/" to create a folder structure
    if (strpos(self::$namespace, '/') !== false) {
      $folderPath = rtrim(self::$cacheDirectory, '/') . '/' . trim(self::$namespace, '/');

      if (!is_dir($folderPath)) {
        mkdir($folderPath, 0755, true);
      }

      return $folderPath . '/' . md5(base64_encode($key)) . '.cache';
    }
    else {
      return self::$cacheDirectory . md5(base64_encode(self::$namespace . '_' . $key)) . '.cache';
    }
  }

  /**
   * Trigger a registered event, executing all associated callbacks.
   *
   * @param string $eventName The name of the event to trigger.
   * @param mixed ...$args Optional arguments to pass to the event callbacks.
   */
  public static function triggerEvent(string $eventName, mixed ...$args) {
    // Check if there are any callbacks registered for the specified event
    if (isset(self::$events[$eventName])) {
      // Loop through each registered callback and execute it with the provided arguments
      foreach (self::$events[$eventName] as $callback) {
        call_user_func_array($callback, $args);
      }
    }
  }

  /**
   * Register a custom event with a callback.
   *
   * @param string $eventName The name of the event.
   * @param callable $callback The callback function to be executed when the event is triggered.
   * @return bool
   */
  public static function registerEvent(string $eventName, callable $callback) {
    // Add the callback to the array of callbacks for the specified event
    self::$events[$eventName][] = $callback;
    if (!in_array($eventName, array_keys(self::$builtInEvents))) {
      self::triggerEvent('cache_event_registered', $eventName);
    }

    return true;
  }

  /**
   * Get the names of all registered events.
   *
   * @return array An array containing the names of registered events.
   */
  public static function getRegisteredEvents() {
    self::$events = [...self::$builtInEvents, ...self::$events];

    // Extract the keys (event names) from the events array
    return array_keys(self::$events);
  }
}
