<?php

ob_start();
if (session_status() == PHP_SESSION_NONE) session_start();
@date_default_timezone_set(env('date_timezone') ?? 'UTC');

if (env('debug') == 'true') {
  ini_set('error_reporting', E_ALL & ~E_WARNING & ~E_DEPRECATED);
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
}
else {
  ini_set('error_reporting', ~E_ALL);
}

class Core extends Database {

  public $langs                           = [];
  public $default_lang                    = null;
  public $default_lang_web                = null;
  public $default_lang_api                = null;
  public $default_lang_cp                 = null;
  public $fallback_lang                   = null;
  public $fallback_lang_web               = null;
  public $fallback_lang_api               = null;
  public $fallback_lang_cp                = null;
  //-------------------------------------------//
  public $upload_path                     = null;
  public $storage_path                    = null;
  public $base_path                       = null;
  public $base_dir                        = null;
  public $isLocalhost                     = null;
  protected $load_error_core_layout       = true;
  //-------------------------------------------//
  public $base_title                      = null;
  public $platform                        = null;
  public $use_encrypia                    = null;
  protected $encrypia_key                 = null;

  public function __construct() {
    parent::__construct();

    $this->langs              = (array) json_decode(env('langs'));

    $this->default_lang       = session('core', 'default_lang') ?? env('default_lang');
    $this->default_lang_web   = session('core', 'default_lang_web') ?? (env('default_lang_web') ?? $this->default_lang);
    $this->default_lang_api   = session('core', 'default_lang_api') ?? (env('default_lang_api') ?? $this->default_lang);
    $this->default_lang_cp    = session('core', 'default_lang_cp') ?? (env('default_lang_cp') ?? $this->default_lang);

    $this->fallback_lang      = env('fallback_lang');
    $this->fallback_lang_web  = env('fallback_lang_web') ?? $this->fallback_lang;
    $this->fallback_lang_api  = env('fallback_lang_api') ?? $this->fallback_lang;
    $this->fallback_lang_cp   = env('fallback_lang_cp')  ?? $this->fallback_lang;

    $this->base_title         = env('base_title');
    $this->use_encrypia       = env('use_encrypia');
    $this->encrypia_key       = env('encrypia_key');

    $this->base_path          = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']);
    $this->isLocalhost        = parse_url($this->base_path, PHP_URL_HOST) == 'localhost';
    $this->base_dir           = dirname(__DIR__);

    $this->base_path          = str_replace("\\", "", $this->base_path);
    $this->base_path          = !str_ends_with($this->base_path, '/') ? "{$this->base_path}/" : $this->base_path;

    if (!file_exists('views/welcome.php') and env('database') != '') {
      // welcome setup is done or skipped
      $this->db_tables = array_diff(
        R::inspect(),
        ['cp_users', 'cp_todos', 'settings', /*'mail', */'apis', 'maillist_mails', 'maillist_subs']
      );
    }

    if ($_SERVER['SERVER_NAME'] == 'localhost') {
      if (!defined('__LOCALHOST__')) { define('__LOCALHOST__', true); }
    }
    else {
      if (!defined('__LOCALHOST__')) { define('__LOCALHOST__', false); }
    }

    $this->upload_path  = dirname($_SERVER['SCRIPT_FILENAME']) . '/storage/';
    $this->storage_path = $this->base_path . 'storage/';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if (!defined('__REQUEST__')) { define('__REQUEST__', 'post'); }
    }
    else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
      if (!defined('__REQUEST__')) { define('__REQUEST__', 'get'); }
    }
    else {
      if (!defined('__REQUEST__')) { define('__REQUEST__', 'else'); }
    }

    if (!__LOCALHOST__) {
      // ip-based option works only on remote server, not in localhost
      /*if (in_array('ip-based', [$this->default_lang, $this->default_lang_web, $this->default_lang_api, $this->default_lang_cp])) {
        pre('ip-based detected');
        $ar_countries = ['SA', 'IL', 'JO', 'AE', 'PS', 'SD', 'IQ', 'KW', 'YE', 'SY', 'OM', 'QA', 'LB', 'LY', 'EG', 'BH',];
        // $ip = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . ip()));
        // pre($ip);
      }*/
    }
    else {
      $this->default_lang_web = $this->default_lang_web != 'ip-based' ? $this->default_lang_web : $this->fallback_lang_web;
      $this->default_lang_api = $this->default_lang_api != 'ip-based' ? $this->default_lang_api : $this->fallback_lang_api;
      $this->default_lang_cp  = $this->default_lang_cp != 'ip-based' ? $this->default_lang_cp : $this->fallback_lang_cp;
    }

    session('core', [
      'langs'             => $this->langs,
      'default_lang'      => $this->default_lang,
      'default_lang_web'  => $this->default_lang_web,
      'default_lang_api'  => $this->default_lang_api,
      'default_lang_cp'   => $this->default_lang_cp,
      'fallback_lang'     => $this->fallback_lang,
      'fallback_lang_web' => $this->fallback_lang_web,
      'fallback_lang_api' => $this->fallback_lang_api,
      'fallback_lang_cp'  => $this->fallback_lang_cp,
    ]);

    if ($this->use_encrypia == 'true') {
      Encrypia::setKey($this->encrypia_key);
    }

    /**
     * true => use file-based driver, false => use in-memory driver.
     * This logic works well in this way:
     * - Checking that the developer had enabled Caching globally for all queries
     * -- Then we'll use file-based driver in whole Ornata.
     * - Else, that means the developer hadn't enabled it globally
     * -- That means Model won't cache any queries
     *    ALTHOUGH if the developer uses file-based caching for other purposes
     *    because the env key 'use_cache_globally' is set to false and Model checks
     *    if it's true to do caching stuff.
     */
    Cache::enable(env('use_cache_globally') == 'true');
    Cache::setDefaultTtl((int) env('cache_ttl') ?? 60);
    Cache::setGracePeriod((int) env('cache_grace_period') ?? 15);

    if (__REQUEST__ == 'get') {
      // only get routes to let Ornata installs .env file
      $this->welcome();
    }
  }

  public function registerPlugin(string $plugin_path) {
    return require_once $plugin_path . '.php';
  }

  public function upload(array $file, string $folder, string $type = 'image', string|null $given_name = null) {
    if ($type == 'image') {
      $types = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'];
    }
    else if ($type == 'video') {
      $types = ['video/mp4', 'application/x-mpegURL', 'video/MP2T', 'video/3gpp'];
    }
    else {
      $types = [
        'application/pdf',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/vnd.ms-excel',
        'application/vnd.sun.xml.base',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.oasis.opendocument.spreadsheet',
        'application/vnd.oasis.opendocument.text',
        'application/vnd.rar',
        'application/octet-stream',
        'application/zip',
        'application/x-zip-compressed',
        // 'application/x-msdownload', // exe file
        'text/plain',
        'image/png',
        'image/jpg',
        'image/jpeg',
        'audio/mpeg',
        'audio/mp3',
      ];
    }

    if ($file['size'] > 0 and in_array($file['type'], $types) and $file['error'] == 0 and $file['name'] != '') {
      $ext = '.' . strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
      $random_txt = rand_str(8);
      $new_name = ($given_name ?? $random_txt) . $ext;

      if (!is_dir($this->upload_path . $folder)) { mkdir($this->upload_path . $folder, 755, true); }
      $uploaded_file = $this->upload_path . $folder . '/' . $new_name;
      copy($file['tmp_name'], $this->upload_path . $folder . '/' . $new_name);

      if (in_array($type, ['image'])) {
        $thumb_name = ($given_name ?? $random_txt) . '_thumb' . $ext;
        $thumb = new \Gumlet\ImageResize($uploaded_file);
        $thumb->scale(10)->save($this->upload_path . $folder . '/' . $thumb_name);
      }

      return $new_name;
    }
    else {
      return false;
    }
  }

  protected function error(int|null $code = 422, $title = 'Application Error', $details = '', $show_btn = false, $force_error = false) {
    http_response_code($code ?? 422);

    $details = !($force_error or ($code != 404 and env('debug') == 'true'))
      ? "Error in {$this->base_title} website. Please contact the admin."
      : $details;

    if ($this->load_error_core_layout) require_once('views/core/header.php');
    require_once('views/core/error.php');
    if ($this->load_error_core_layout) require_once('views/core/footer.php');
  }

  private function welcome() {
    if (file_exists('views/welcome.php')) {
      Corviz\Crow\Crow::setDefaultPath('views');
      Corviz\Crow\Crow::setExtension('');
      Corviz\Crow\Crow::disableCodeMinifying();
      Corviz\Crow\Crow::render('welcome', ['base_path' => $this->base_path]);
      die();
    }
  }

  public function backup($filename = null) {
    $filename ??= date('Y-m-d_H-i-s') . '_' . rand_str();
    $return = [];

    if (!is_dir('.backups/databases')) {
      mkdir('.backups/databases', 0755, true);
    }

    if ($this->db_type == 'sqlite3') {
      // sqlite 3
      copy(env('database'), ".backups/databases/{$filename}.sqlite3");
      $return = ['status' => 'ok', 'name' => "{$filename}.sqlite3"];
    }
    else {
      // mysql
      $server = [
        'server'    => strpos(env('server'), ':') ? explode(':', env('server'))[0] : 'localhost',
        'port'      => strpos(env('server'), ':') ? explode(':', env('server'))[1] : 3306,
        'username'  => env('username'),
        'password'  => env('password'),
        'database'  => env('database'),
        'output'    => ".backups/databases/{$filename}.sql" . ($this->isLocalhost ? '' : 'backup'),
      ];

      $command = "mysqldump --host={$server['server']} --port={$server['port']} --user={$server['username']} --password={$server['password']} {$server['database']} --result-file={$server['output']}";
      exec($command, $output, $result_code);

      switch ($result_code) {
        case 0:
          $return = ['status' => 'ok', 'name' => "{$filename}.sql" . ($this->isLocalhost ? '' : 'backup')];
          break;
        case 2:
          $return = ['status' => 'error', 'reason' => 2];
          break;
        case 126:
          $return = ['status' => 'error', 'reason' => 126];
          break;
        case 128:
          $return = ['status' => 'error', 'reason' => 128];
          break;
        case 130:
          $return = ['status' => 'error', 'reason' => 130];
          break;
        case 255:
          $return = ['status' => 'error', 'reason' => 255];
          break;
        case 1:
        default:
          $return = ['status' => 'error', 'reason' => 1];
          break;
      }
    }

    return $return;
  }

}