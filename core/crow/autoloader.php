<?php

require_once 'core/crow/src/Traits/SelfCreate.php';

$files = array_filter(
  iterator_to_array(
    new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator('core/crow/src')),
    \RecursiveIteratorIterator::SELF_FIRST
  ),
  fn($file) => $file !== '.' && $file !== '..' && strtolower(pathinfo($file, PATHINFO_EXTENSION)) == "php"
);

foreach ($files as $file) { require_once $file; }

