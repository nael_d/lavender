<?php

namespace Csrf;

class Csrf {

  private static $length = 32;
  private static $expiry_time = 1800;

  /**
   * Generate and store a token in session based on the current URL.
   * @return string
   */
  public static function generate() {
    $url = self::getCurrentUrl();
    $tokenKey = self::getTokenKey($url);

    if (
      isset($_SESSION['csrf'][$tokenKey]) and
      (time() - $_SESSION['csrf'][$tokenKey]['time']) <= self::$expiry_time
    ) {
      return $_SESSION['csrf'][$tokenKey]['token'];
    }
    else {
      self::clear($tokenKey);

      $token = substr(bin2hex(random_bytes(self::$length)), 0, self::$length);
      $_SESSION['csrf'][$tokenKey] = [
        'token' => $token,
        'time' => time(),
        'ip' => self::getIp(),
        'user_agent' => $_SERVER['HTTP_USER_AGENT']
      ];

      // Set token as a cookie with HttpOnly and Secure flags
      setcookie($tokenKey, $token, time() + self::$expiry_time, '/', '', true, true);

      return $token;
    }
  }

  /**
   * Validate the token based on the current URL.
   * @param mixed $token
   * @return bool
   */
  public static function validate($token) {
    $url = self::getCurrentUrl();
    $tokenKey = self::getTokenKey($url);
    $validity = false;

    if (isset($_SESSION['csrf'][$tokenKey])) {
      if (
        ((time() - $_SESSION['csrf'][$tokenKey]['time']) <= self::$expiry_time) and
        $_SESSION['csrf'][$tokenKey]['ip'] == self::getIp() and
        $_SESSION['csrf'][$tokenKey]['user_agent'] == $_SERVER['HTTP_USER_AGENT'] and
        (
          hash_equals($_SESSION['csrf'][$tokenKey]['token'], $token) and
          ((isset($_COOKIE[$tokenKey]) and hash_equals($_COOKIE[$tokenKey], $token)) or !isset($_COOKIE[$tokenKey]))
        )
      ) {
        $validity = true;
      }
    }

    self::clear($tokenKey);
    return $validity;
  }

  /**
   * Embed CSRF token in a hidden form input.
   * @return string
   */
  public static function embed() {
    $token = self::generate();
    return '<input type="hidden" name="csrf_token" value="' . htmlspecialchars($token, ENT_QUOTES, 'UTF-8') . '" />';
  }

  /**
   * Clear the token for a specific URL.
   * @param string $tokenKey
   * @return void
   */
  private static function clear($tokenKey = null) {
    if ($tokenKey) {
      unset($_SESSION['csrf'][$tokenKey]);
      setcookie($tokenKey, '', time() - 3600, '/', '', true, true);
    }
    else {
      unset($_SESSION['csrf']);
      setcookie('csrf_token', '', time() - 3600, '/', '', true, true);
    }
  }

  /**
   * Get the current URL.
   * @return string
   */
  private static function getCurrentUrl() {
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $host = $_SERVER['HTTP_HOST'];
    $requestUri = $_SERVER['REQUEST_URI'];
    return "{$protocol}{$host}{$requestUri}";
  }

  /**
   * Get a unique token key for the current URL.
   * @param string $url
   * @return string
   */
  private static function getTokenKey($url) {
    return 'csrf_' . md5($url);
  }

  /**
   * Get the client IP address.
   * @return string
   */
  private static function getIp() {
    return $_SERVER['REMOTE_ADDR'] ?? '0.0.0.0';
  }
}
