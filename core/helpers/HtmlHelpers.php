<?php

function copyright(string|null $title) {
  if (!$title) return '';

  $year = date("Y");
  $y = (env('base_launch_year') != '' and $year > (int) env('base_launch_year')) ? (env('base_launch_year') . '-' . $year) : $year;

  return "&copy; {$y} :: {$title}";
}

function powered_by(string|null $title) {
  if (!$title) return '';

  return "{$title} is powered by " . a('https://www.ornata.dev', 'Ornata', open_new_tab: true);
}

function a(string|null $link, string|null $text, array|string $class = [], bool $open_new_tab = false, array $attrs = [], bool $match_url = true, bool $match_url_exact = true, bool $if = true) {
  if (!$if or $link === null) return;

  $open_new_tab       = $open_new_tab ? "target='_blank'" : '';
  $path_exploded      = session('core', 'current_path_array') ?? '';
  $current_path_full  = session('core', 'current_path_full') ?? '';
  $link_full          = str_contains($link, ':') ? $link : path($link);
  $attrs_str          = '';

  if ($link != '//') {
    $link = substr($link, 0, 1) == '/' ? substr($link, 1) : $link;
  }
  else {
    $link = 'javascript://';
  }

  $class = is_string($class) ? explode(' ', $class) : $class;

  if ($match_url_exact and $current_path_full == $link_full) {
    $class[] = 'active-exact';
  }

  if ($match_url) {
    $link_exploded = explode('/', $link);
    if ($link_exploded[0] == $path_exploded[0]) {
      $class[] = 'active';
    }
  }

  $class = is_array($class) ? implode(' ', $class) : $class;
  foreach ($attrs as $key => $value) { $attrs_str .= "{$key}='{$value}' "; }

  return "<a href='{$link_full}' class='{$class}' {$open_new_tab} {$attrs_str}>{$text}</a>";
}

function a_icon(string|null $link, string|array|null $icon, ...$params) {
  if (!$link or !$icon) return;

  $text = '';
  if (is_array($icon)) {
    // first index is icon, second if exist is text
    $text = "<i class='{$icon[0]}'></i>";
    if ($icon[1]) { $text .= "<span class='mx-2'>{$icon[1]}</span>"; }
  }
  else {
    // text: only icon
    $text = "<i class='{$icon}'></i>";
  }

  return a($link, $text, ...$params);
}

function option(string|null $value, string|null $title, bool $selected = false, bool $if = true) {
  if (!$if) return;

  $selected = $selected ? 'selected' : '';
  return "<option value='$value' {$selected}>{$title}</option>";
}

//

function tag(string|null $tag, string|null $content = null, string|array|null $class = null, array $attrs = [], bool $if = true) {
  if (!$if or !$tag) return;

  $self_close_tags = ['base', 'br', 'hr', 'img', 'input', 'link', 'meta'];

  $attrs = implode(' ', array_map(
    function ($attr, $value) { return "{$attr}=\"{$value}\""; },
    array_keys($attrs),
    $attrs,
  ));

  if ($attrs) { $attrs = " {$attrs}"; }

  if ($class) {
    if (is_string($class)) { $class = " class='{$class}'"; }
    else if (is_array($class) and count($class) > 0) {
      $class = " class='" . implode(' ', $class) . "'";
    }
  }
  else {
    $class = null;
  }

  return !in_array($tag, $self_close_tags)
    ? "<{$tag}{$attrs}{$class}>{$content}</{$tag}>"
    : "<{$tag}{$attrs}{$class} />";
}

function h(int $level = 1, string|null $content = null, ...$tagparams) {
  $tag = ($level >= 1 and $level <= 6) ? "h{$level}" : 'div';

  return tag($tag, $content, ...$tagparams);
}

function i(string|null $content = null, ...$tagparams) {
  return tag('i', $content, ...$tagparams);
}

function u(string|null $content = null, ...$tagparams) {
  return tag('u', $content, ...$tagparams);
}

function icon($icon) {
  return i(class: $icon);
}

function b(string|null $content = null, ...$tagparams) {
  return tag('b', $content, ...$tagparams);
}

function hr(...$tagparams) {
  return tag('hr', ...$tagparams);
}

function br(int $amount = 1) {
  $list = [];
  for ($i = 1; $i <= $amount; $i++) { $list[] = tag('br'); }
  return implode($list);
}

function img(string|null $src, string|array $class = [], string $alt = '', array $attrs = [], bool $if = true) {
  if (!$if or !$src) return;

  return tag('img', class: $class, attrs: ['alt' => $alt, 'src' => $src, ...$attrs], if: $if);
}

function meta(string|null $name, string|null $content, bool $if = true) {
  return tag('meta', attrs: ['name' => $name, 'content' => $content], if: $if);
}

function metagraph(string|null $property, string|null $content, bool $if = true) {
  return tag('meta', attrs: ['property' => $property, 'content' => $content], if: $if);
}

function _link(string|null $rel, string|null $href, bool $if = true) {
  return tag('link', attrs: ['rel' => $rel, 'href' => $href], if: $if);
}

function base(string|null $target = null, string|null $href = null, bool $if = true) {
  $attrs = [];

  if ($href)    $attrs['href']    = $href;
  if ($target)  $attrs['target']  = $target;

  return tag('link', attrs: $attrs, if: $if);
}

function input(string $type, string $name, string|null $value = null, ...$tagparams) {
  $attrs = ['type' => $type, 'name' => $name, 'value' => $value, ...$tagparams['attrs']];
  $tagparams = [...$tagparams, 'attrs' => $attrs];

  return tag('input', ...$tagparams);
}

function button(string $type = 'button', string|null $content = null, string|null $name = null, string|null $value = null, ...$tagparams) {
  $tagparams['attrs']['type'] = $type;
  if ($name)   $tagparams['attrs']['name']  = $name;
  if ($value)  $tagparams['attrs']['value'] = $value;

  return tag('button', $content, ...$tagparams);
}
