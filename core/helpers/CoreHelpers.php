<?php

function ip() {
  return $_SERVER['REMOTE_ADDR'];
}

// to use this, enable intl extension in php.ini first.
function getLanguage(string|null $country) {
  if (!$country) return;

  $subtags = \ResourceBundle::create('likelySubtags', 'ICUDATA', false);
  $country = \Locale::canonicalize('und_'.$country);
  $locale = $subtags->get($country) ?: $subtags->get('und');
  return \Locale::getPrimaryLanguage($locale);
}

function lang(string|null $key, string|null $lang = null) {
  if (!$key) return;
  if (!session('web', 'current_lang')) return;

  $current_lang = $lang ?? session('web', 'current_lang');
  $keys = explode('/', $key);
  $page = $keys[0];
  unset($keys[0]);
  require "i18n/{$current_lang}/{$page}.php"; // you'll get $i18n ready
  
  foreach ($keys as $k => $v) {
    $i18n = $i18n[$v];
  }

  return $i18n;
}

function lang_json(string|object|null $json, string|null $language = null) {
  if (!$json) return;

  $language = $language ?? session('web', 'current_lang');
  $json = is_string($json) ? (object) json_decode($json, true) : $json;
  return $json->$language ?? null;
}

function value_json(string|object|null $json, string $key = '') {
  if (!$json) return;

  $json = is_string($json) ? json_decode($json) : $json;
  return $json->$key;
}

function limit_text(string|null $text, int|null $length = null) {
  if (!$text) return;

  return ($length and $length > 0) ? array_slice(explode(' ', sanitizer($text)), 0, $length) : $text;
}

function sanitizer(array|string|object $inputs) {
  $search = [
    '@<script[^>]*?>.*?</script>@si',   // Strip out script tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip out style tags
    '@<![\s\S]*?--[ \t\n\r]*>@',        // Strip out multi-line comments
    /*'@<[\/\!]*?[^<>]*?>@si',*/            // Strip out HTML tags
  ];

  if (is_string($inputs)) {
    return preg_replace($search, '', $inputs);
  }
  else {
    $arr = [];
    foreach ($inputs as $key => $input) { $arr[$key] = sanitizer($input); }
    return $arr;
  }
}

function format_date(string|null $timestamp) {
  if (!$timestamp) return;
  if (!is_numeric($timestamp)) $timestamp = strtotime($timestamp);

  // Get time difference and setup arrays
  $difference = time() - $timestamp;
  $periods = ["second", "minute", "hour", "day", "week", "month", "years"];
  $lengths = ["60", "60", "24", "7", "4.35", "12"];

  // Past or present
  if ($difference >= 0)  {
    $ending = "ago";
  }
  else {
    $difference = -$difference;
    $ending = "to go";
  }

  // Figure out difference by looping while less than array length
  // and difference is larger than lengths.
  $arr_len = count($lengths);
  for($j = 0; $j < $arr_len && $difference >= $lengths[$j]; $j++) {
    $difference /= $lengths[$j];
  }

  // Round up		
  $difference = round($difference);

  // Make plural if needed
  if($difference != 1) {
    $periods[$j].= "s";
  }

  // Default format
  $text = "$difference $periods[$j] $ending";

  // over 24 hours
  if($j > 2) {
    // future date over a day formate with year
    if($ending == "to go") {
      if($j == 3 && $difference == 1) {
        $text = "Tomorrow at ". date("g:i a", $timestamp);
      }
      else {
        $text = date("F j, Y \a\\t g:i a", $timestamp);
      }
      //
      return $text;
    }

    if ($j == 3 and $difference == 1) {
      // Yesterday
      $text = "Yesterday at ". date("H:i", $timestamp);
    }
    else if ($j == 3) {
      // Less than a week display -- Monday at 5:28pm
      $text = date("l, \a\\t H:i", $timestamp);
    }
    else if ($j < 6 && !($j == 5 and $difference == 12)) {
      // Less than a year display -- June 25 at 5:23am
      $text = date("F j, \a\\t H:i", $timestamp);
    }
    else {
      // if over a year or the same month one year ago -- June 30, 2010 at 5:34pm
      $text = date("F j, Y, \a\\t H:i", $timestamp);
    }
  }

  return $text;
}

//

function pre(mixed $arr, int $theme = 1) {
  $arr = print_r($arr, true);

  switch ($theme) {
    case 1:   $color='#2295bc'; $background='#e4e7e7'; break;
    case 2:   $color='#e4e7e7'; $background='#2295bc'; break;
    case 3:   $color='#064439'; $background='#51bba8'; break;
    case 4:   $color='#efc75e'; $background='#324d5b'; break;
    case 5:   $color='#000000'; $background='#b1eea1'; break;
    case 6:   $color='#fff';    $background='#e2574c'; break;
    default:  $color='#2295bc'; $background='#e4e7e7'; break;
  }

  echo <<<pre
  <pre style="
    direction: ltr;
    background-color: {$background};
    color: {$color};
    max-width: 90%;
    margin: 30px auto;
    padding: 20px;
    overflow:auto;
    font-family: Monaco, Consolas, 'Lucida Console';
    font-size: 16px;
  ">{$arr}</pre>
  pre;
}

function dpre($arr, $theme = 1) {
  pre($arr, $theme);
  die;
}

function vd(...$vars) {
  var_dump(...$vars);
}

