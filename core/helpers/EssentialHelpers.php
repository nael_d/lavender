<?php

function rand_str(int $length = 6, $source = null) {
  $str = '';

  if ($source) {
    return substr(str_shuffle($source), 0, $length);
  }

  for ($i = 0; $i < $length; $i++) { 
    $str .= match (mt_rand(0, 2)) {
      0 => chr(mt_rand(97, 122)), // a-z
      1 => chr(mt_rand(65, 90)),  // A-Z
      2 => chr(mt_rand(48, 57)),  // upper 0-9 numbers
    };
  }

  return $str;
}

function env(string|null $key = null) {
  $env_array = [];
  $env = explode("\n", @file_get_contents('.env'));

  foreach ($env as $key_loop => $value) {
    if (!in_array(ord(substr($value, 0, 1)), [13, 35])) {
      $v = explode('=', $value, 2);
      
      if (ord($v[0]) != 0) {
        // remove newlines from being treated as an empty array keys
        $env_array[$v[0]] = $v[1];
      }
    }
  }

  if ($key == null) { return $env_array; }
  return trim($env_array[$key] ?? '');
}

function env_backup() {
  // checking that there's no .env file, otherwise store it as backup
  if (file_exists('.env')) {
    rename('.env', '.backup.' . rand_str() . '.env');
  }
}

function path(string|null $path = '') {
  global $base_path;

  $path = $path != '/' ? $path : '';
  $path = ($path != '//' and str_starts_with($path, '/')) ? substr($path, 1) : $path;
  return !in_array($path, ['//']) ? ($base_path . $path) : 'javascript://';
}

function storage(string|null $storage) {
  if (!$storage) return;

  global $storage_path;
  return $storage_path . $storage;
}

function asset(string|null $asset) {
  if (!$asset) return;

  global $base_path;
  return $base_path . 'assets/' . $asset;
}

function css(string|null $css, bool $is_local = true) {
  if (!$css) return;

  global $base_path;

  $href = $is_local ? ($base_path . "assets/{$css}") : $css;
  return "<link rel='stylesheet' href='{$href}' />
";
}

function js(string|null $js, bool $is_local = true) {
  if (!$js) return;

  global $base_path;

  $src = $is_local ? ($base_path . "assets/{$js}") : $js;
  return "<script src='{$src}'></script>
";
}

function redirect(string|null $dest) {
  if (!$dest) return;

  global $base_path;
  header("Location: " . (!isset(parse_url($dest)['scheme']) ? ($base_path . $dest) : $dest));
  die();
}

function deleteFolder(string $folderPath) {
  if (!is_dir($folderPath)) {
    return false; // Not a directory
  }

  $contents = glob($folderPath . '/*');
  foreach ($contents as $content) {
    if (is_dir($content)) {
      // Recursively delete subfolder
      deleteFolder($content);
    }
    else {
      // Delete file
      unlink($content);
    }
  }

  // Delete the main folder
  rmdir($folderPath);

  return true;
}

function truncate_text(string $text, int $max_length, bool $show_ellipsis = true, bool $truncate_middle = false) {
  if (!$truncate_middle) {
    $truncated_text = substr($text, 0, $max_length);
    return $show_ellipsis && strlen($text) > $max_length ? $truncated_text . "..." : $truncated_text;
  }

  if (strlen($text) <= $max_length) {
    return $text;
  }

  $ellipsis = ($show_ellipsis && $max_length >= 3) ? '...' : '';

  $start_length = floor(($max_length - strlen($ellipsis)) / 2);
  $end_length = ceil(($max_length - strlen($ellipsis)) / 2);

  $start = substr($text, 0, $start_length);
  $end = substr($text, -$end_length);

  return $start . $ellipsis . $end;
}

/**
 * Create a zip file.
 * @param string $source The path to the folder you want to zip.
 * @param string $destination The name and location of the zip file to create.
 * @param array $excluded An array containing names of files/folders to exclude.
 * @return bool|string
 */
function zip(string $source, string $destination, array $excluded = []) {
  if (!extension_loaded('zip')) {
    return "Zip extension not loaded";
  }

  if (!file_exists($source)) {
    return "Source folder not found: {$source}";
  }

  $zip = new \ZipArchive();
  if (!$zip->open($destination, \ZIPARCHIVE::CREATE | \ZIPARCHIVE::OVERWRITE)) {
    return "Failed to create zip file: {$destination}";
  }

  // Get the absolute path for the source folder
  $source = realpath($source);
  $iterator = new \RecursiveIteratorIterator(
    new \RecursiveDirectoryIterator($source),
    \RecursiveIteratorIterator::SELF_FIRST
  );

  // Convert exclusion list to lowercase for case-insensitive matching
  $excluded = count($excluded) > 0 ? array_map('strtolower', $excluded) : $excluded;

  foreach ($iterator as $file) {
    $filePath = $file->getRealPath();
    $relativePath = substr($filePath, strlen($source) + 1);
    $fileName = strtolower($file->getFilename()); // Get filename in lowercase for case-insensitive matching

    if (in_array($fileName, $excluded, true) || count(array_intersect(explode(DIRECTORY_SEPARATOR, $relativePath), $excluded)) > 0) {
      continue; // Skip excluded files and folders
    }

    // Check for complete subfolder path match (case-insensitive)
    $isExcludedSubfolder = false;
    foreach ($excluded as $excludedPath) {
      if (strpos(str_replace(DIRECTORY_SEPARATOR, '/', $relativePath), $excludedPath) === 0) {
        $isExcludedSubfolder = true;
        break;
      }
    }

    if ($isExcludedSubfolder) {
      continue; // Skip excluded files and folders
    }

    if ($file->isDir()) {
      // Add empty directory
      $zip->addEmptyDir($relativePath);
    }
    else {
      // Add file with relative path
      $zip->addFile($filePath, $relativePath);
    }
  }

  $zip->close();
  return true;
}

/**
 * Outputs human-readable period depending on seconds.
 * @param mixed $seconds
 * @param mixed $separator
 * @return string
 */
function humanize_time($seconds, $separator = ', ') {
  $periods = [
    'year'    => 31536000,
    'month'   => 2592000,
    'week'    => 604800,
    'day'     => 86400,
    'hour'    => 3600,
    'minute'  => 60,
    'second'  => 1,
  ];

  $parts = [];
  foreach ($periods as $period => $duration) {
    if ($seconds >= $duration) {
      $count = floor($seconds / $duration);
      $seconds -= $count * $duration;
      $parts[] = $count . ' ' . $period . ($count > 1 ? 's' : '');
    }
  }

  return implode($separator, $parts);
}

