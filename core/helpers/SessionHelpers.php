<?php

function session(string|null $section, string|array|null $key = null, mixed $value = null) {
  if (!$section) return;

  if ($value == null) {
    if ($key == null) {
      return $_SESSION[$section] ?? [];
    }
    else if (is_array($key)) {
      // this is a key-value array to set
      if (count(session($section)) > 0) {
        /**
         * We used spreading to keep the original value and overwrite any passed changes.
         */
        $_SESSION[$section] = [...$_SESSION[$section], ...$key];
      }
      else {
        $_SESSION[$section] = $key;
      }

      return $_SESSION[$section];
    }
    else {
      return !empty($_SESSION[$section][$key]) ? $_SESSION[$section][$key] : null;
    }
  }
  else {
    if (is_string($key)) {
      // this is a string key
      $_SESSION[$section][$key] = $value;
      return session($section, $key);
    }
    else {
      return null;
    }
  }
}

function session_remove(string|null $section, string|array|null $key) {
  if (!$section or !$key) return;

  if (is_string($key)) {
    unset($_SESSION[$section][$key]);
  }
  else {
    foreach ($key as $k => $v) { session_remove($section, $v); }
  }

  return true;
}



