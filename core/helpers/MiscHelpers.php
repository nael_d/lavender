<?php

function youtube_parser(string|null $url) {
  if (!$url) return;

  preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
  return $matches[1];
}

function vimeo_parser(string|null $url) {
  if (!$url) return;

  preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $url, $output);
  return $output[5];
}

function youtube_thumb(string|null $url) {
  if (!$url) return;

  $id = youtube_parser($url);
  return "https://img.youtube.com/vi/{$id}/0.jpg";
}

function vimeo_thumb(string|null $url) {
  if (!$url) return;

  $imgid = vimeo_parser($url);
  $hash = unserialize(file_get_contents("https://vimeo.com/api/v2/video/{$imgid}.php"));
  return $hash[0]['thumbnail_medium']; 
}

function youtube_iframe(string|null $url) {
  if (!$url) return;

  $yt_id = youtube_parser($url);
  //
  return "
    <iframe
      width='100%' height='315' allowfullscreen frameborder='0'
      src='https://www.youtube.com/embed/{$yt_id}'
      allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'></iframe>
  ";
}

function vimeo_iframe(string|null $url) {
  if (!$url) return;

  $vid = vimeo_parser($url);
  //
  return "
    <iframe
      width='100%' height='315' allowfullscreen frameborder='0'
      src='https://player.vimeo.com/video/{$vid}'
      allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'></iframe>
  ";
}

function arabic_date_format(string|null $timestamp) {
  if (!$timestamp) return;

  $periods = array(
    "second"  => "ثانية", 
    "seconds" => "ثواني", 
    "minute"  => "دقيقة", 
    "minutes" => "دقائق", 
    "hour"    => "ساعة", 
    "hours"   => "ساعات", 
    "day"     => "يوم", 
    "days"    => "أيام", 
    "month"   => "شهر", 
    "months"  => "شهور", 
  );
  $difference = (int) abs(time() - $timestamp);
  $plural = array(3,4,5,6,7,8,9,10);
  $readable_date = "منذ ";
  //
  if ($difference < 60) {
    // less than a minute
    $readable_date .= $difference . " ";
    if (in_array($difference, $plural)) {
      $readable_date .= $periods["seconds"];
    }
    else {
      $readable_date .= $periods["second"];
    }
  } 
  else if ($difference < (60*60)) {
    // less than an hour
    $diff = (int) ($difference / 60);
    $readable_date .= $diff . " ";
    if (in_array($diff, $plural)) {
      $readable_date .= $periods["minutes"];
    }
    else {
      $readable_date .= $periods["minute"];
    }
  } 
  else if ($difference < (24*60*60)) {
    // less than a day
    $diff = (int) ($difference / (60*60));
    $readable_date .= $diff . " ";
    if (in_array($diff, $plural)) {
      $readable_date .= $periods["hours"];
    }
    else {
      $readable_date .= $periods["hour"];
    }
  } 
  else if ($difference < (30*24*60*60)) {
    // less than a month 
    $diff = (int) ($difference / (24*60*60));
    $readable_date .= $diff . " ";
    if (in_array($diff, $plural)) {
      $readable_date .= $periods["days"];
    }
    else {
      $readable_date .= $periods["day"];
    }
  } 
  else if ($difference < (365*24*60*60)) {
    // less than a year
    $diff = (int) ($difference / (30*24*60*60));
    $readable_date .= $diff . " ";
    //
    if (in_array($diff, $plural)) {
      $readable_date .= $periods["months"];
    }
    else {
      $readable_date .= $periods["month"];
    }
  } 
  else {
    $readable_date = date("d-m-Y", $timestamp);
  }

  return $readable_date;
}

function singularize(string|null $params) {
  if (!$params) return;

  if (is_string($params)) {
    $word = $params;
  }
  else if (!$word = $params['word']) {
    return false;
  }

  $singular = array (
    '/(quiz)zes$/i'                                                     => '\\1',
    '/(matr)ices$/i'                                                    => '\\1ix',
    '/(vert|ind)ices$/i'                                                => '\\1ex',
    '/^(ox)en/i'                                                        => '\\1',
    '/(alias|status)es$/i'                                              => '\\1',
    '/([octop|vir])i$/i'                                                => '\\1us',
    '/(cris|ax|test)es$/i'                                              => '\\1is',
    '/(shoe)s$/i'                                                       => '\\1',
    '/(o)es$/i'                                                         => '\\1',
    '/(bus)es$/i'                                                       => '\\1',
    '/([m|l])ice$/i'                                                    => '\\1ouse',
    '/(x|ch|ss|sh)es$/i'                                                => '\\1',
    '/(m)ovies$/i'                                                      => '\\1ovie',
    '/(s)eries$/i'                                                      => '\\1eries',
    '/([^aeiouy]|qu)ies$/i'                                             => '\\1y',
    '/([lr])ves$/i'                                                     => '\\1f',
    '/(tive)s$/i'                                                       => '\\1',
    '/(hive)s$/i'                                                       => '\\1',
    '/([^f])ves$/i'                                                     => '\\1fe',
    '/(^analy)ses$/i'                                                   => '\\1sis',
    '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i'  => '\\1\\2sis',
    '/([ti])a$/i'                                                       => '\\1um',
    '/(n)ews$/i'                                                        => '\\1ews',
    '/s$/i'                                                             => '',
  );

  $irregular = array(
    'person'  => 'people',
    'man'     => 'men',
    'child'   => 'children',
    'sex'     => 'sexes',
    'move'    => 'moves',
    'datum'   => 'data',
  );

  $ignore = array(
    'equipment',
    'information',
    'rice',
    'money',
    'species',
    'series',
    'fish',
    'sheep',
    'press',
    'sms',
  );

  $lower_word = strtolower($word);

  foreach ($ignore as $ignore_word) {
    if (substr($lower_word, (-1 * strlen($ignore_word))) == $ignore_word) {
      return $word;
    }
  }

  foreach ($irregular as $singular_word => $plural_word) {
    if (preg_match('/('.$plural_word.')$/i', $word, $arr)) {
      return preg_replace('/('.$plural_word.')$/i', substr($arr[0],0,1).substr($singular_word,1), $word);
    }
  }

  foreach ($singular as $rule => $replacement) {
    if (preg_match($rule, $word)) {
      return preg_replace($rule, $replacement, $word);
    }
  }

  return $word;
}

function qr(string|null $data, $size = '200x200', $logo = false) {
  if (!$data) return;

  ob_clean();
  header('Content-type: image/png');
  // Get QR Code image from Google Chart API
  // http://code.google.com/apis/chart/infographics/docs/qr_codes.html
  $QR = imagecreatefrompng('https://chart.googleapis.com/chart?cht=qr&chld=H|1&chs='.$size.'&chl='.urlencode($data));

  if ($logo) {
    $logo = imagecreatefromstring(file_get_contents($logo));

    $QR_width = imagesx($QR);
    $QR_height = imagesy($QR);
    
    $logo_width = imagesx($logo);
    $logo_height = imagesy($logo);
    
    // Scale logo to fit in the QR Code
    $logo_qr_width = $QR_width / 3;
    $scale = $logo_width / $logo_qr_width;
    $logo_qr_height = $logo_height / $scale;
    
    imagecopyresampled(
      $QR,
      $logo,
      $QR_width / 3,
      $QR_height / 3,
      0,
      0,
      $logo_qr_width,
      $logo_qr_height,
      $logo_width,
      $logo_height
    );
  }

  imagepng($QR);
  imagedestroy($QR);
}

