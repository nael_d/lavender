<?php

$plugins = [
  // Google Authenticator
  // Plugin is loaded via Composer, files kept for fallback.
  /*
  'GoogleAuthenticator-2.x/src/FixedBitNotation.php',
  'GoogleAuthenticator-2.x/src/GoogleAuthenticatorInterface.php',
  'GoogleAuthenticator-2.x/src/GoogleAuthenticator.php',
  'GoogleAuthenticator-2.x/src/GoogleQrUrl.php',
  */

  // Crow Template Engine (forked version: nael94)
  'crow/autoloader.php',

  // Base32 generator
  'base32-master/src/Base32.php',

  // ImageResize
  'ImageResize.php',

  // Encrypia
  'Encrypia.php',
];

$required_extensions = [
  'pdo_sqlite'  => extension_loaded('pdo_sqlite'),
  'pdo_mysql'   => extension_loaded('pdo_mysql'),
  'mysqli'      => extension_loaded('mysqli'),
  'zip'         => extension_loaded('zip'),
  'gd'          => extension_loaded('gd'),
];

$autoloaders = ['controllers', 'middlewares', 'schemas', 'models', 'components', 'directives'];

$required_presets = [
  ini_get('allow_url_fopen')    => "Enable <kbd>allow_url_fopen</kbd> in <kbd>php.ini</kbd> on your server.",
  // ini_get('allow_url_include')  => "Enable <kbd>allow_url_include</kbd> in <kbd>php.ini</kbd> on your server.",
];

if (in_array(false, array_keys($required_presets))) {
  // some of presets are not enabled.
  $errors = array_values(array_filter($required_presets, fn($key) => $key == false, ARRAY_FILTER_USE_KEY));
  errorView("<h4 style='margin: 15px 0'>{$errors[0]}</h4>");
}

//

function errorView($details, $title = "Application Error", $code = 422) {
  http_response_code($code);
  $basepath = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']
  . str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);

  die(<<<r
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel='stylesheet' href='{$basepath}assets/core/css/core.css' />
      <link rel="icon" href="{$basepath}assets/core/pics/ornata-ico.png" />
      <title>{$title} :: Ornata</title>
    </head>
    <body class="layout-core">
      <div class="layout-error">
        <div class="error-page container">
          <div class="title">{$code}</div>
          <div class="hint">{$title}</div>
          <div class="details">{$details}</div>
        </div>
      </div>
      <div class="rectangle" data-id="1"></div>
      <div class="rectangle" data-id="2"></div>
    </body>
    </html>
  r);
}

function loader($directory) {
  $ignoredFilenames = ['Blank.php', 'ignored', 'blank'];
  $files = [];

  $iterator = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($directory),
    RecursiveIteratorIterator::SELF_FIRST
  );

  foreach ($iterator as $file) {
    if ($file->isFile() && !in_array($file->getBasename(), $ignoredFilenames)) {
      $files[] = $file->getPathname();
    }
  }

  return $files;
}

//

if (version_compare(PHP_VERSION, '8.1.4', '<')) {
  errorView("<h4 style='margin: 15px 0'>Ornata works at least with PHP <kbd>8.1.4</kbd> while current is <kbd>" . PHP_VERSION . "</kbd>.</h4>");
}

if (in_array(false, $required_extensions)) {
  $r = array_keys(array_filter($required_extensions, fn($x) => $x === false));
  $r = join(' ', array_map(fn($r) => "<kbd>{$r}</kbd>", $r));
  errorView("<p><b>Ornata requires these extensions to function as well: </b></p>{$r}");
}

if (!is_dir('vendor') or !file_exists('vendor/autoload.php')) {
  errorView(<<<err
  <h4 style='margin: 25px 0 0'>Ornata depends on libraries that's installed by
    <a href='https://getcomposer.org/' target='_blank'>Composer</a>.
  </h4>
  <h4 style='margin: 5px 0 0'>Please install it and then run <kbd>composer install</kbd></h4>
  err);
}

// plugins
foreach ($plugins as $key => $plugin) {
  require_once $plugin;
}

// bootstrapping core
require_once 'vendor/autoload.php';
require_once 'Csrf.php';
require_once 'Cache.php';
require_once 'Helpers.php';
foreach (glob('helpers/**/**.php') as $key => $helper) { require_once $helper; }
require_once 'Database.php';
require_once 'Core.php'; new Core; // this is to init the Core
require_once 'Middleware.php';
require_once 'Schema.php';
require_once 'Model.php';
require_once 'Controller.php';
require_once 'ControllerExtended.php';
require_once 'Cronjobs.php';
require_once 'Cp.php';
require_once 'Api.php';
require_once 'Errors.php';

// load all $autoloaders
foreach ($autoloaders as $key => $autoload) {
  foreach (loader($autoload) as $key => $load) { require_once $load; }
}
