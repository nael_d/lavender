<?php

namespace CpController;

class CP extends \ControllerExtended {

  public $links = []; // this is used in cp aside links.
  public $cpa = [];
  public $cp_user = null;
  public $darkmode = false;

  public function __construct() {
    $this->views_root_path = 'cp';
    /**
     * `views_root_path` comes from Controller,
     * so constructing the parent (ControllerExtended) will not override the value
     */
    parent::__construct();

    if ($this->db_con == 'offline') {
      $this->error(details: "Database connection missing. Cannot use control panel offline.", force_error: true);
    }

    $this->load_model('CpModel\Auth');
    $this->cp_user = current((new \CpModel\Auth)->get(session('cp', 'id_cp_user')));
    $this->darkmode = $this->cp_user['cp_user_is_darkmode'] == 1;

    // $this->darkmode = (new \CpModel\Settings)->preference_get_value('dark-mode') == 1;

    $this->cpa = array_values(array_diff($this->current_path_array, ['cp']));
    $this->links = [
      [
        'title' => 'Slides',
        'icon'  => 'fas fa-images',
        'link'  => ('cp/slides'),
        'class' => 'slides ' . (in_array('slides', $this->cpa) ? 'active' : ''),
      ],
      [
        'title' => 'About us',
        'icon'  => 'fas fa-info',
        'link'  => ('cp/about-us'),
        'class' => 'about-us ' . (in_array('about-us', $this->cpa) ? 'active' : ''),
      ],
      [
        'title' => 'Contact us',
        'icon'  => 'fas fa-phone-alt',
        'link'  => ('cp/contact-us'),
        'class' => 'contact-us ' . (in_array('contact-us', $this->cpa) ? 'active' : ''),
      ],
      [
        'title' => 'Privacy policy',
        'icon'  => 'fas fa-lock',
        'link'  => ('cp/privacy-policy'),
        'class' => 'privacy-policy ' . (in_array('privacy-policy', $this->cpa) ? 'active' : ''),
      ],
      [
        'title' => 'Terms of service',
        'icon'  => 'fas fa-file-contract',
        'link'  => ('cp/terms-of-service'),
        'class' => 'terms-of-service ' . (in_array('terms-of-service', $this->cpa) ? 'active' : ''),
      ],
      "<hr class='border-secondary my-1' />",
      [
        'title' => 'Mail',
        'icon' => 'far fa-envelope',
        'link' => ('cp/mail'),
        'class' => 'mail ' . (in_array('mail', $this->cpa) ? 'active' : ''),
      ],
      [
        'title' => 'Mailing list',
        'icon' => 'fas fa-mail-bulk',
        'link' => ('cp/maillist'),
        'class' => 'maillist ' . (in_array('maillist', $this->cpa) ? 'active' : ''),
      ],
      [
        'title' => 'Settings',
        'icon' => 'fas fa-cog',
        'link' => ('cp/settings'),
        'class' => 'settings ' . (in_array('settings', $this->cpa) ? 'active' : ''),
      ],
    ];
  }

  public function bulk() {
    $table    = $_POST['table'];
    $column   = $_POST['column'];
    $list     = $_POST['list'];
    $list_arr = !is_array($list) ? explode(",", $list) : $list;
    $value    = $_POST['value'];
    $action   = $_POST['action'] ?? 'delete';

    if ($action == 'delete') {
      if ($table == 'mail') {
        /**
         * we're going to check that if the selected rows are in trash or not.
         * this means that we're gonig to set if the rows are deleted => mail_is_deleted = 1
         * otherwise, it's normal deletion => mail_is_active => 0
         */

        $this->load_model('CpModel\Mail');

        if ((new \CpModel\Mail)->get(where: ['id_mail' => $list_arr])[0]['mail_is_active'] == 1) {
          $update = update([
            'table' => $table,
            'data'  => ['mail_is_active' => 0],
            'where' => ['id_mail' => $list_arr],
          ]);
        }
        else {
          // the mails are in trash. So, set mail_is_deleted => 0 to delete them permanently logically.
          $update = update([
            'table' => $table,
            'data'  => ['mail_is_deleted' => 1],
            'where' => ['id_mail' => $list_arr],
          ]);
        }
      }
      else {
        $update = update([
          'table' => $table,
          'data'  => ["{$column}_is_deleted" => 1,],
          'where' => ["id_{$column}" => $list_arr],
        ]);
      }
    }
    else if ($action == 'activity') {
      $update = update([
        'table' => $table,
        'data'  => ["{$column}_is_active" => $value,],
        'where' => ["id_{$column}" => $list_arr],
      ]);
    }

    $run = \R::exec($update);
    echo json_encode(['status' => $run]);
  }

  public function remove() {
    $table        = $_POST['table'];
    $base_column  = $_POST['prefix'];
    $file         = $_POST['file'];
    $file_thumb   = pathinfo($file, PATHINFO_FILENAME) . "_thumb." . pathinfo($file, PATHINFO_EXTENSION);
    $id_item      = $_POST['id_item'];
    $column_name  = $_POST['column_name'];
    $folder       = $_POST['folder'];
    $column       = "{$base_column}_{$column_name}";

    $model = new \Model($table, $base_column, $table);
    $model->i18n = [];
    $model->neutral = [$column_name];
    $model->activeOnly = false;
    $item = $model->get($id_item);

    if (count($item) > 0) {
      $item = current($item);
      $list = (array) $item[$column];
      $list_popped = [...array_filter($list, fn($i) => $i !== $file)];

      sleep(2);
      $model->store([$column => $list_popped], id: $id_item);
      unlink("storage/{$folder}/{$file}");
      if (in_array(pathinfo($file, PATHINFO_EXTENSION), ['png', 'jpg', 'jpeg'])) {
        unlink("storage/{$folder}/{$file_thumb}"); // also delete its thumb image
      }

      echo 'ok';
    }
  }

  public function transmit() {
    $key = explode('|', array_key_first($_FILES));
    $column = $key[0];
    $table = $key[1];
    $prefix = $key[2];
    $suffix = $key[3];
    $id = $key[4];
    $file = current($_FILES);

    $model = new \Model($table, $prefix, $table);
    $model->i18n = [];
    $model->neutral = [$suffix];
    $model->activeOnly = false;
    $item = $model->get($id);

    if (count($item) > 0) {
      $item = current($item); // to get the very first item because it's only one
      $list = (is_array($item[$column]) or is_object($item[$column])) ? (array) $item[$column] : [];

      $name = $this->upload($file, "{$table}/{$suffix}", explode('/', $file['type'])[0]);
      if ($name) {
        // allowed file type and uploaded
        array_push($list, $name);
        // dpre($list);
        sleep(2);
        $model->store([$column => $list], id: $id);
        echo json_encode(['url' => storage("{$table}/{$suffix}/{$name}"), 'file' => $name]);
      }
      else {
        // else
        echo json_encode(['url' => '']);
      }
    }
  }

}
