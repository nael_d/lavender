<?php

namespace Schema;

use Exception; // Import Exception class

class Schema extends \Core {
  private $tableName;
  private $activity   = 1;
  private $columns    = [];
  private $prefix     = '';
  private $constraints = []; // To store constraints like primary keys, unique keys etc.

  public function __construct() {
    if ($this->activity == 0) {
      die("Schema builder is inactive to safeguard the database.");
    }

    parent::__construct();

    if (!($this instanceof SchemaBlueprint)) {
      throw new Exception("Error: `" . get_class($this) . "` schema must implement `SchemaBlueprint` interface.");
    }
  }

  public function table($tableName) {
    $this->tableName = $tableName;
    return $this;
  }

  public function primaryKey() {
    $columnCount = count($this->columns);
    if ($columnCount > 0) {
      $lastColumnName = array_key_last($this->columns);
      $this->columns[$lastColumnName]['primary_key'] = true;
      $this->constraints['primaryKey'][] = $lastColumnName; // Store primary key constraint
    }

    return $this;
  }

  public function string($columnName, $length = 255) {
    $this->addColumn($columnName, 'string', compact('length'));
    return $this;
  }

  public function integer($columnName) {
    $this->addColumn($columnName, 'integer');
    return $this;
  }

  public function longtext($columnName) {
    $this->addColumn($columnName, 'longtext');
    return $this;
  }

  public function datetime($columnName) {
    $this->addColumn($columnName, 'datetime');
    return $this;
  }

  public function json($columnName) {
    $this->addColumn($columnName, 'json');
    return $this;
  }

  public function tinyint($columnName) {
    $this->addColumn($columnName, 'tinyint');
    return $this;
  }

  public function mediumint($columnName) {
    $this->addColumn($columnName, 'mediumint');
    return $this;
  }

  public function bigint($columnName) {
    $this->addColumn($columnName, 'bigint');
    return $this;
  }

  public function autoIncrement($startValue = null) {
    $this->columns[array_key_last($this->columns)]['autoIncrement'] = true;
    if ($startValue !== null) {
      $this->columns[array_key_last($this->columns)]['autoIncrementStart'] = $startValue;
    }

    return $this;
  }

  public function defaultValue($value) {
    $this->columns[array_key_last($this->columns)]['defaultValue'] = $value;
    return $this;
  }

  public function allowNull() {
    $this->columns[array_key_last($this->columns)]['allowNull'] = true;
    return $this;
  }

  public function unsigned() {
    $this->columns[array_key_last($this->columns)]['unsigned'] = true;
    return $this;
  }

  public function comment($comment) {
    $this->columns[array_key_last($this->columns)]['comment'] = $comment;
    return $this;
  }

  public function prefix($prefix) {
    $this->prefix = $prefix;
    return $this;
  }

  private function addColumn($columnName, $type, $options = []) {
    $prefixedColumnName = $this->applyPrefix($columnName);

    // Store column definition with unquoted name, quoting will be done in build()
    $this->columns[$prefixedColumnName] = array_merge(
      ['columnName' => $prefixedColumnName, 'type' => $type],
      $options
    );
  }

  private function applyPrefix($columnName) {
    return str_replace('*', $this->prefix, $columnName);
  }

  private function getColumnTypeDefinition($column) {
    $dbType = $this->db_type;
    $type = $column['type'];

    switch ($type) {
      case 'string':
        $length = $column['length'] ?? 255;
        return $dbType === 'mysql' ? "VARCHAR({$length})" : 'TEXT';
      case 'integer':
        $baseType = $dbType === 'mysql' ? 'INT' : 'INTEGER';
        return $this->applyIntegerModifiers($baseType, $column, $dbType);
      case 'longtext':
        return $dbType === 'mysql' ? 'LONGTEXT' : 'TEXT';
      case 'datetime':
        return $dbType === 'mysql' ? 'DATETIME' : 'TEXT'; // Storing as TEXT in SQLite as per original logic.
      case 'json':
        return $dbType === 'mysql' ? 'JSON' : 'TEXT'; // Storing as TEXT in SQLite for simplicity.
      case 'tinyint':
        $baseType = $dbType === 'mysql' ? 'TINYINT(3)' : 'INTEGER';
        return $this->applyIntegerModifiers($baseType, $column, $dbType);
      case 'mediumint':
        $baseType = $dbType === 'mysql' ? 'MEDIUMINT' : 'INTEGER';
        return $this->applyIntegerModifiers($baseType, $column, $dbType);
      case 'bigint':
        $baseType = $dbType === 'mysql' ? 'BIGINT' : 'INTEGER';
        return $this->applyIntegerModifiers($baseType, $column, $dbType);
      default:
        return 'VARCHAR(255)'; // Default if type not recognized.
    }
  }

  private function applyIntegerModifiers($baseType, $column, $dbType) {
    $type = $baseType;
    if (isset($column['unsigned']) && $dbType === 'mysql') {
      $type .= ' UNSIGNED';
    }

    if (isset($column['autoIncrement']) && $dbType === 'mysql') {
      $type .= ' AUTO_INCREMENT';
    }

    return $type;
  }

  // Helper function to quote identifiers based on database type
  private function quoteIdentifier($identifier) {
    $dbType = $this->db_type;

    if ($dbType === 'mysql') {
      return "`" . $identifier . "`"; // Backticks for MySQL
    }
    else if ($dbType === 'sqlite3') {
      return "\"" . $identifier . "\""; // Double quotes for SQLite
    }
    else {
      return $identifier; // No quoting for other DB types (or handle differently if needed)
    }
  }

  public function build() {
    $quotedTableName = $this->quoteIdentifier($this->tableName); // Quote table name
    $sql = "CREATE TABLE IF NOT EXISTS $quotedTableName (\n";
    $columnDefinitions = [];
    $primaryKeyConstraints = [];

    foreach ($this->columns as $columnName => $column) {
      $quotedColumnName = $this->quoteIdentifier($columnName); // Quote column name here
      $definition = $this->getColumnTypeDefinition($column);
      $modifiers = [];

      if (!isset($column['allowNull']) || !$column['allowNull']) { // Default to NOT NULL if allowNull is not set to true
        $modifiers[] = 'NOT NULL';
      }

      if (isset($column['defaultValue'])) {
        $modifiers[] = "DEFAULT '" . $column['defaultValue'] . "'"; // Correctly quote default values
      }

      if (isset($column['comment']) && $this->db_type == 'mysql') {
        $modifiers[] = "COMMENT '" . $column['comment'] . "'";
      }

      $columnDefinitions[] = "  $quotedColumnName  " . $definition . " " . implode(' ', $modifiers); // Use quoted column name

      if (isset($column['primary_key']) && $column['primary_key'] && $this->db_type === 'sqlite3' && isset($column['autoIncrement'])) {
        $primaryKeyConstraints[] = $this->quoteIdentifier($columnName) . " AUTOINCREMENT"; // Quote and add AUTOINCREMENT for SQLite
      }
      else if (isset($column['primary_key']) && $column['primary_key']) {
        $primaryKeyConstraints[] = $this->quoteIdentifier($columnName); // Quote for primary key constraint
      }
    }

    $sql .= implode(",\n", $columnDefinitions);

    if (!empty($primaryKeyConstraints)) {
      $primaryKeySql = ",\n  PRIMARY KEY (" . implode(', ', $primaryKeyConstraints) . ")"; // Indent PRIMARY KEY clause
      $sql .= $primaryKeySql;
    }

    $sql .= "\n);\n"; // Add newline at the end for better formatting

    try {
      \R::exec($sql);

      // Apply ALTER TABLE AUTO_INCREMENT for MySQL if autoIncrementStart is set
      $autoIncrementStartColumns = array_filter($this->columns, fn($column) => isset($column['autoIncrementStart']));

      if ($this->db_type === 'mysql' && !empty($autoIncrementStartColumns)) {
        foreach ($autoIncrementStartColumns as $autoIncrementColumn) {
          $alterTableSQL = "ALTER TABLE $quotedTableName AUTO_INCREMENT = " . $autoIncrementColumn['autoIncrementStart'] . ";";
          \R::exec($alterTableSQL); // Execute ALTER TABLE statement
        }
      }

      return $this;
    }
    catch (Exception $e) {
      // Basic error handling - consider more robust logging or exception re-throwing in real app.
      throw new Exception("Error building schema for table '{$this->tableName}': " . $e->getMessage() . ". SQL: " . $sql, 0, $e);
    }
    finally {
      $this->columns = []; // Reset columns after build. Always reset, even on error, to allow for subsequent schema builds.
      $this->constraints = []; // Reset constraints as well
    }
  }

  public function insert($values) {
    if (empty($this->tableName)) {
      throw new Exception("Table name must be set before insert operation.");
    }

    if (empty($values)) {
      throw new Exception("Values array cannot be empty for insert operation.");
    }

    $prefixedValues = [];

    foreach ($values as $columnName => $value) {
      $prefixedValues[$this->applyPrefix($columnName)] = $value;
    }

    $quotedTableName = $this->quoteIdentifier($this->tableName); // Quote table name for insert
    $quotedColumns = array_map([$this, 'quoteIdentifier'], array_keys($prefixedValues)); // Quote column names for insert
    $columns = implode(', ', $quotedColumns);
    $valuePlaceholders = implode(', ', array_fill(0, count($prefixedValues), '?'));
    $sql = "INSERT INTO $quotedTableName ({$columns}) VALUES ({$valuePlaceholders});"; // Use quoted table and column names

    try {
      \R::exec($sql, array_values($prefixedValues)); // Use parameterized query
    }
    catch (Exception $e) {
      throw new Exception("Error inserting data into table '{$this->tableName}': " . $e->getMessage() . ". SQL: " . $sql . ". Values: " . json_encode($prefixedValues), 0, $e);
    }

    return $this;
  }

  public function __destruct() {
    $this->tableName = ''; // Reset table name after each operation? Or when appropriate in your workflow
    $this->prefix = ''; // Reset prefix too?
  }
}

interface SchemaBlueprint {
  public function install();
}
