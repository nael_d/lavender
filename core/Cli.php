<?php

namespace Cli;

function color(string $color, string $text) {
  $colors = [
    "black"         => "\033[0;30m",
    "red"           => "\033[0;31m",
    "light-red"     => "\033[1;31m",
    "green"         => "\033[0;32m",
    "light-green"   => "\033[1;32m",
    "brown"         => "\033[0;33m",
    "orange"        => "\033[0;33m",
    "blue"          => "\033[0;34m",
    "light-blue"    => "\033[1;34m",
    "purple"        => "\033[0;35m",
    "light-purple"  => "\033[1;35m",
    "cyan"          => "\033[0;36m",
    "light-cyan"    => "\033[1;36m",
    "light-gray"    => "\033[0;37m",
    "dark-gray"     => "\033[1;30m",
    "yellow"        => "\033[1;33m",
    "white"         => "\033[1;37m",
    "default"       => "\033[0m",
  ];

  if (!array_key_exists($color, $colors)) $color = $colors["default"];

  return $colors[$color] . "{$text}" . $colors['default'];
}

class Cli {

  private $excludedMethods = [
    'output', 'args', 'prompt', 'animation',
    'handle', 'review', 'check_console_width',
    'getTerminalWidth', 'zip', 'check_for_curl',
    'check_for_zip',
  ];

  public function __construct() {
    // requiring base classes
    require_once 'vendor/autoload.php';
    require_once 'progressbar/Progressbar.php';
    require_once 'helpers/EssentialHelpers.php';  // for [schema(), install(), zip(), ftp()]
    require_once "helpers/SessionHelpers.php";    // for [schema()]
    require_once "Cache.php";                     // for [wipe_cache()]
    require_once "Encrypia.php";                  // for [schema()]
    require_once "Database.php";                  // for [schema()]
    require_once "Core.php";  new \Core;          // for [schema()] with instance
    require_once "Schema.php";                    // for [schema()]
  }

  public function __destruct() {
    die;
  }

  private function getTerminalWidth() {
    return match (PHP_OS_FAMILY) {
      "Windows" => (int) exec('%SYSTEMROOT%\System32\WindowsPowerShell\v1.0\powershell.exe -Command "$Host.UI.RawUI.WindowSize.Width"'),
      'Darwin'  => (int) exec('/bin/stty/ size | cut -d" " -f2'),
      'Linux'   => (int) exec('/usr/bin/env tput cols'),
      default   => 0,
    };
  }

  private function check_console_width(int $width = 84) {
    $cols = $this->getTerminalWidth();

    if ($cols < $width) {
      $this->output("For optimal viewing of the output, Ornata CLI recommends using a console window with a width of at least ({$width}) characters (Current is {$cols}).");
      die();
    }
  }

  private function check_for_curl() {
    if (!extension_loaded('curl')) {
      $this->output(color('red', '[ERROR]') . " cURL extension is required to run this command.");
      die;
    }
  }

  private function check_for_zip() {
    if (!extension_loaded('zip')) {
      $this->output(color('red', '[ERROR]') . " Zip extension is required to run this command.");
      die;
    }
  }

  public function handle(array|null $args) {
    if (count($args) == 1) {
      // $this->howto();
      echo <<<ascii
          ____  _____  _   _       _______       
         / __ \|  __ \| \ | |   /\|__   __|/\    
        | |  | | |__) |  \| |  /  \  | |  /  \   
        | |  | |  _  /| . ` | / /\ \ | | / /\ \  
        | |__| | | \ \| |\  |/ ____ \| |/ ____ \ 
         \____/|_|  \_\_| \_/_/    \_\_/_/    \_\

                Welcome to Ornata CLI

        Ornata CLI boosts your productivity with its
        built-in tasks that help you do things efficiently.

        To dive in, run `php ornata review` to list all tasks.

      ascii;
      die(); // to prevent continue running this method that will fail in this case.
    }

    $command = $args[1] ?? '';

    if (method_exists($this, $command)) {
      $this->{$command}(...$this->args($args));
    }
    else {
      $this->output("`{$command}` task is not defined in Ornata CLI.", true, true);
    }
  }

  private function args(array $args) {
    $args = array_slice($args, offset: 2);
    $parsedArgs = [];
    $currentKey = null;

    foreach ($args as $arg) {
      // Check if the argument follows the --parameter value format
      if (strpos($arg, '--') === 0) {
        // Remove the leading --
        $currentKey = substr($arg, 2);
        // Check if the argument has a value assigned with space or equal sign
        if (strpos($currentKey, '=') !== false) {
          // Split parameter and value by equal sign
          [$currentKey, $value] = explode('=', $currentKey, 2);
          $parsedArgs[$currentKey] = $value !== "" ? $value : true;
        }
        else {
          $parsedArgs[$currentKey] = true; // Initialize as boolean (no value specified yet)
        }
      }
      else if ($currentKey !== null) {
        // If a key is set, assign the argument value
        $parsedArgs[$currentKey] = $arg;
        $currentKey = null; // Reset key after assigning value
      }
      else {
        // no flags are passed, so we list all params in an ordered zero-based array
        $parsedArgs[] = $arg;
      }
    }

    return $parsedArgs;
  }

  private function output(string $message = '', bool $eol_before = false, bool $eol_after = false, bool $eol_normal = true) {
    echo ($eol_before ? "\n" : '') . "{$message}" . ($eol_normal ? PHP_EOL : '') . ($eol_after ? "\n" : '');
    ob_flush();
  }

  private function animation(string $text, bool|null $status = null) {
    $symbols = ['\\', '|', '/', '-'];

    if ($status === null) {
      for ($i = 0; $i < 3; $i++) {
        foreach ($symbols as $symbol) {
          $this->output("\r{$text}\t\t\t{$symbol}", eol_normal: false);
          usleep(50000);
          ob_flush();
        }
      }
    }

    if ($status === false) {
      $this->output("\r{$text}\t\t[FAILED]", eol_after: true, eol_normal: false);
    }
    else if ($status === true) {
      $this->output("\r{$text}\t\t\t[OK]", eol_after: true, eol_normal: false);
    }
  }

  private function prompt(string $question, string|null $default = null) {
    $fullQuestion = $default ? "{$question}: ({$default})" : $question;
    $this->output("{$fullQuestion} >> ", eol_normal: false);
    ob_flush();
    $input = trim(fgets(STDIN));

    // pressing Enter with no parameters will return the default value for that prompt
    return ($input == '' && $default != null) ? $default : $input;
  }

  private function howto() {
    // Display help message or usage instructions
    $this->output(eol_before: true, eol_after: true, message: <<<output
      Usage: php ornata <command> (--flag?=?)[parameter]
    output);
  }

  private function review() {
    $reflection = new \ReflectionClass($this);
    $output = [];
    $i      = 1;

    foreach ($reflection->getMethods(\ReflectionMethod::IS_PRIVATE) as $method) {
      if (in_array($method->name, $this->excludedMethods)) continue;

      // Get method parameters
      $params = [];

      foreach ($method->getParameters() as $param) {
        $params[] = $param->name;
      }

      $output[] = [
        '#' => $i,
        'Command' => "{$method->name}" // . (count($params) > 0 ? "(".implode(', ', $params).")" : ""),
      ];

      $i++;
    }

    if (count($output) > 0) {
      $builder = new \AsciiTable\Builder();
      $builder->setTitle('Ornata CLI commands');
      $builder->addRows($output);
      $this->output($builder->renderTable(), true, true);
    }
    else {
      $this->output("No commands registered in `Ornata CLI` yet.", true, true);
    }
  }

  private function version() {
    $v = file_get_contents("ornata-version");
    $this->output("Ornata version is: `{$v}`", true, true);
  }

  private function install(string|null $link = null) {
    $this->check_for_curl();
    $this->check_for_zip();

    $frameworks = json_decode(file_get_contents('frameworks.json'), true);

    if (!$link) {
      $this->output("Choose one of the following items to install:");

      foreach ($frameworks as $index => $item) {
        $frameworks[$index]['#'] = $index + 1;
        $frameworks[$index]['Framework'] = $item['name'];
      }

      $builder = new \AsciiTable\Builder();
      $builder->setTitle("Ornata Framework Installer");
      $builder->addRows($frameworks);
      $builder->showColumns(['#', 'Framework']);
      $this->output($builder->renderTable(), true);

      $this->output(
        <<<message
        You may install your framework directly by passing
        the `https://**/*.zip` direct zipped framework link
        as a parameter of this command.
        It'll be installed and unzipped as well.
        message,
        true, true
      );
      $choice = $this->prompt("Enter the number of the item to install:");

      if (!is_numeric($choice) || $choice < 1 || $choice > count($frameworks)) {
        $this->output("Invalid choice. Please enter a valid number.", eol_after: true);
        return;
      }
    }
    else {
      if (strtolower(pathinfo($link, PATHINFO_EXTENSION)) != 'zip') {
        $this->output('Error: only `.zip` files is allowed to be installed.', true, true);
        die();
      }
    }

    $selectedItem     = !$link ? $frameworks[array_keys($frameworks)[$choice - 1]] : null;
    $selectedItemLink = $link ?? $selectedItem['link'];
    $selectedItemBase = pathinfo($selectedItemLink, PATHINFO_FILENAME);
    $selectedItemName = $link ? $selectedItemBase : $selectedItem['name'];
    $selectedItemFile = $selectedItemName . "." . pathinfo($selectedItemLink, PATHINFO_EXTENSION);
    $this->output("Installing: {$selectedItemName}");

    if (!is_dir("assets/web/libs")) { mkdir("assets/web/libs", 0755); }
    if (is_dir("assets/web/libs/{$selectedItemName}")) { deleteFolder("assets/web/libs/{$selectedItemName}"); }
    if (is_file("assets/web/libs/{$selectedItemFile}")) { unlink("assets/web/libs/{$selectedItemFile}"); }

    $file = fopen("assets/web/libs/{$selectedItemFile}", 'w');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $selectedItemLink);
    curl_setopt($ch, CURLOPT_NOPROGRESS, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_FILE, $file);
    curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, function ($resource, $downloadSize, $downloaded, $uploadSize, $uploaded) {
      // print_r([$resource, $downloadSize, $downloaded, $uploadSize, $uploaded]);
      if ($downloadSize == 0 and $downloaded == 0) {
        // do nothing because it's been started recently.
      }
      else if ($downloadSize > 0) {
        echo (new \ProgressBar($downloadSize))->drawCurrentProgress($downloaded);
      }
      else {
        static $startTime;
        $startTime ??= time();
        $elapsed = time() - $startTime;
        $d = $downloaded <= 1048576 ? round($downloaded / 1024, 2) . " KB/s" : round($downloaded / 1024 / 1024, 2) . " MB/s";;
        $s = '';
        $o = '';

        if ($elapsed > 0) {
          $speed = ($downloaded / $elapsed); // speed in bytes
          $s = "• Speed: (".
            ($speed <= 1048576
              ? (round($speed / 1024, 2) . " KB/s")
              : (round($speed / 1024 / 1024, 2) . " MB/s"))
          .")";
        }

        $o = "\rDownloading: {$d} {$s}";
        $this->output($o . str_repeat(' ', max(0, $this->getTerminalWidth() - strlen($o))), eol_normal: false);
      }

      ob_flush(); // necessary for cli instant refreshing
    });

    // Execute cURL session
    sleep(1);
    curl_exec($ch);

    // Close the cURL session
    curl_close($ch);

    // Close the file
    fclose($file);

    mkdir("assets/web/libs/{$selectedItemName}", 0755);
    $zipArchive = new \ZipArchive();
    $zipArchive->open("assets/web/libs/{$selectedItemFile}");
    $zipArchive->extractTo("assets/web/libs/{$selectedItemName}");
    $zipArchive->close();

    $this->output("Installation complete: {$selectedItemName}.", true);
    $this->output("Installation location: `assets/web/libs/{$selectedItemName}`", eol_after: true);
  }

  private function documentation(/*$server = 'localhost', */string|int $port = 3000) {
    $server = 'localhost'; // fix this value because Ornata uses this address depending on the localhost server address.
    // $server     = $server == 'localhost' ? $this->prompt("Enter the server address", "{$server}") : $server;
    $port       = $port == 3000 ? $this->prompt("Enter the port", "{$port}") : $port;
    $folderPath = "documentation";

    // Resolve to absolute path
    $folderPath = realpath($folderPath);

    if ($folderPath === false) {
      $this->output("Error: The specified folder `{$folderPath}` path is invalid.", true, true);
      return;
    }

    // Change directory to the specified folder
    if (!chdir($folderPath)) {
      $this->output("Error: Could not change to the specified folder.", true, true);
      return;
    }

    // Display the server address
    $this->output("Ornata documentation is ready at http://{$server}:{$port}/", true, true);

    // Start PHP built-in server
    shell_exec("php -S {$server}:{$port}");
  }

  private function updateDocumentation() {
    $this->check_for_curl();

    // GitLab raw file URL for the documentation
    $doc_url = 'https://gitlab.com/nael_d/ornata/-/raw/master/documentation/js/documentation.js?ref_type=heads&inline=false';

    // Get the new file name and the relative path
    $newFileName = 'documentation/js/documentation.js';
    $relativePath = pathinfo($newFileName, PATHINFO_DIRNAME);

    // Initialize cURL session
    $ch = curl_init();

    // Check if the existing documentation file exists
    if (file_exists($newFileName)) {
      // Get the content hash of the existing documentation file
      $existingFileHash = md5(file_get_contents($newFileName));

      // Fetch the content hash of the remote documentation
      $remoteFile = @file_get_contents($doc_url);

      if (!$remoteFile) {
        $this->output('Unable to retrieve remote documentation file.', true, true);
        die();
      }

      $remoteFileHash = md5($remoteFile);

      // Compare content hashes to check for a newer version
      if ($remoteFileHash && $remoteFileHash !== $existingFileHash) {
        // Remote file is different, proceed with update

        // Initialize the backup file name with a timestamp
        $timestamp = date('Y-m-d--H-i-s');
        $backupFileName = "{$relativePath}/documentation-old-{$timestamp}.js";

        // Create a backup with timestamp
        copy($newFileName, $backupFileName);

        // Output the message
        $this->output('Newer version available. Updating documentation...', eol_before: true);

        // Open the documentation file for writing
        $file = fopen($newFileName, 'w');
      }
      else {
        // Existing file is up-to-date or unable to get remote file information
        $this->output('Documentation is already up-to-date.', true, true);
        return; // Quit if the file is up-to-date
      }
    }
    else {
      // The existing documentation file does not exist, proceed with download
      $this->output('No existing documentation found. Downloading documentation...', eol_before: true);

      // Open the documentation file for writing
      $file = fopen($newFileName, 'w');
    }

    // Common cURL settings for both cases
    curl_setopt($ch, CURLOPT_URL, $doc_url);
    curl_setopt($ch, CURLOPT_NOPROGRESS, false);
    curl_setopt($ch, CURLOPT_FILE, $file);
    curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, function ($resource, $downloadSize, $downloaded, $uploadSize, $uploaded) {
      // print_r([$resource, $downloadSize, $downloaded, $uploadSize, $uploaded]);
      if ($downloadSize == 0 and $downloaded == 0) {
        // do nothing because it's been started recently.
      }
      else if ($downloadSize > 0) {
        echo (new \ProgressBar($downloadSize))->drawCurrentProgress($downloaded);
      }
      else {
        static $startTime;
        $startTime ??= time();
        $elapsed = time() - $startTime;
        $d = $downloaded <= 1048576 ? round($downloaded / 1024, 2) . " KB/s" : round($downloaded / 1024 / 1024, 2) . " MB/s";;
        $s = '';
        $o = '';

        if ($elapsed > 0) {
          $speed = ($downloaded / $elapsed); // speed in bytes
          $s = "• Speed: (".
            ($speed <= 1048576
              ? (round($speed / 1024, 2) . " KB/s")
              : (round($speed / 1024 / 1024, 2) . " MB/s"))
          .")";
        }

        $o = "\rDownloading: {$d} {$s}";
        $this->output($o . str_repeat(' ', max(0, $this->getTerminalWidth() - strlen($o))), eol_normal: false);
      }

      ob_flush(); // necessary for cli instant refreshing
    });

    // Execute cURL session
    curl_exec($ch);

    // Close the cURL session
    curl_close($ch);

    // Close the file
    fclose($file);

    // Move to a new line after progress completion
    echo PHP_EOL;

    // Output the final success message
    $this->output('Documentation has been updated successfully!');
  }

  private function env(string|null $key = null, string|null $value = null) {
    $this->check_console_width();
    $env = env();

    if (!$key and !$value) {
      $env = array_map(
        function ($v, $k) { return ['Key' => $k, 'Value' => trim($v)]; },
        $env, array_keys($env)
      );

      $builder = new \AsciiTable\Builder();
      $builder->setTitle('Ornata .env configs');
      $builder->addRows($env);
      $this->output($builder->renderTable(), true);
    }
    else if ($key) {
      if (!$value) {
        // just show the key's value
        $this->output("{$key} => " . env($key));
      }
      else {
        // update the key's value
        $env[$key] = $value;
        $env = array_map(
          function ($v, $k) { return "{$k}={$v}"; },
          $env, array_keys($env)
        );

        file_put_contents('.env', implode("\n", $env));
        $this->output(color('green', "`{$key}`") . " has been updated successfully.");
      }
    }
  }

  private function schema(array|string ...$schemas) {
    if (count($schemas) == 0) {
      $this->output(<<<schema
        Ornata Schema Installer

        To get started: `php ornata schema SchemaName SchemaName2 ...`
      schema, true, true);
      die;
    }
    else {
      $schemas_list = array_values(
        array_map(
          fn($s) => pathinfo(strtolower($s), PATHINFO_FILENAME),
          array_diff(scandir('schemas'), ['.', '..'])
        )
      );

      foreach ($schemas as $schema) {
        // $schema = strtolower($schema); // to uniform the names to lower case to avoid any typos.
        $this->animation("Installing `{$schema}`");

        if (!in_array(strtolower($schema), $schemas_list)) {
          $this->animation("Schema `{$schema}` is not found", false);
        }
        else {
          require_once "schemas/{$schema}.php";
          (new ("\Schema\\{$schema}"))->install();
          $this->animation("Installing `{$schema}`", true);
        }
      }
    }
  }

  private function wipeCache(string|null $name = null, string|null $tag = null, bool $all = false) {
    if (!empty($name)) {
      $name = explode(" ", $name);
      foreach ($name as $k => $v) {
        \Cache::clear($v);
        $this->output("Cache `{$v}` wiped.");
      }
    }
    else if (!empty($tag)) {
      $tag = explode(" ", $tag);
      foreach ($tag as $k => $v) {
        \Cache::clearByTag($v);
        $this->output("Cache tag `{$v}` wiped.");
      }
    }
    else if ($all) {
      \Cache::clearAll();
      $this->output("Cache cleared successfully.");
    }
    else {
      // no parameters passed, useful for hints and helping
      $this->output(<<<wipeCache
        Ornata Cache Wiper

        To get started, this command has these options:
        - name: Wipe a cache file by its name.
                Examples:
                  `ornata wipeCache custom-cache`
                  `ornata wipeCache --name custom-cache`
                  `ornata wipeCache --name=custom-cache`
                  `ornata wipeCache --name="custom-cache custom-cache-2"`

        - tag: Wipe cache files by its tagname.
                Examples:
                  `ornata wipeCache --tag custom-tag`
                  `ornata wipeCache --tag=custom-tag`
                  `ornata wipeCache --tag="custom-tag custom-tag-2"`

        - all: Wipe all cache files.
                Example: `ornata wipeCache --all`
      wipeCache, true, true);
      die;
    }
  }

  private function randText(int|string $length = 10) {
    if ($length < 4) {
      $this->output("Random text's length must be 4 chars minimum.");
    }
    else {
      $this->output("Here is your random text ({$length} letters): " . rand_str($length));
    }
  }

  private function database(string|bool|null $backup = null, string|bool|null $restore = null) {
    if ($backup) {
      $backup = (is_string($backup)) ? trim($backup) : null;
      $action = (new \Core)->backup($backup);

      switch ($action['status']) {
        case 'ok':
          $this->output("Database (backups/databases/{$action['name']}) has been successfully backed up.");
          break;
        case 'error':
          switch ($backup['reason']) {
            case 2:
              $this->output("Error #2: Command 'mysqldump' is not found. Ensure that it's installed or consult your system administrator.");
              break;
            case 126:
              $this->output("Error #126: Permission problem or command not executable. Check file permissions.");
              break;
            case 128:
              $this->output("Error #128: Invalid argument. Please review your input and try again.");
              break;
            case 130:
              $this->output("Error #130: Command terminated. Please avoid interruptions during execution.");
              break;
            case 255:
              $this->output("Error #128: Database access denied. Please check your credentials and ensure you have permission to export the data.");
              break;
            case 1:
            default:
              $this->output("Error #1: General failure. Please try again later or contact your developer.");
              break;
          }
        break;
      }
    }
    else if ($restore) {
      if ($restore) {
        if ((string) $restore === '1') {
          $this->output("Error: no file path passed to restore");
        }
        else {
          $file = file_get_contents($restore);

          if ($file) {
            // file exists, let's check it
            $types        = ['sqlite3', 'sql', 'sqlbackup'];
            $now          = date("Y-m-d--H-i-s");
            $restore_ext  = pathinfo($restore, PATHINFO_EXTENSION);

            if (in_array($restore_ext, $types)) {
              if ((new \Core)->db_type == 'sqlite3') {
                // sqlite3
                if (is_file(env('database'))) {
                  // yes, current database file exists. let's rename it.
                  $rename = copy(
                    env('database'),
                    "databases/" . pathinfo(env('database'), PATHINFO_FILENAME) . "-{$now}.sqlite3"
                  );

                  if ($rename) {
                    // renaming succeeded. let's restore the sqlite3 database file
                    if (in_array($restore_ext, ['sqlite3'])) {
                      // the backup file is `.sqlite3`
                      file_put_contents(env('database'), $file);
                    }
                    else {
                      // `.sql` and `.sqlbackup` on SQLite3: run queries on the now-created database.
                      // let's truncate the current database to execute the sql queries on it.
                      // don't worry, the current database been kept safe.
                      $file = preg_replace("/^--?.+/", '', $file);
                      $file = preg_replace('/\/\*.+?\*\/;?/s', '', $file);
                      $file = explode(";
", $file);    // this is to explode the queries into array to loop through it all.
                      file_put_contents(env('database'), "");
                      try {
                        foreach ($file as $key => $line) {
                          if (!empty($line)) \R::exec($line);
                        }
                      }
                      catch (\Exception $e) {
                        $this->output("". $e->getMessage());
                        die;
                      }
                    }

                    $this->output("Database backup has been restored successfully.");
                  }
                  else {
                    $this->output("Database backup failed. Check paths and close any open databases.");
                  }
                }
                else {
                  // no current database file.
                  // for now, this case is unreachable because an error will output triggered by Database class.
                  // so, you have to copy the database file manually into 'databases' folder.
                }
              }
              else {
                // mysql
                /**
                 * The ideal scenario is to backup the current database then restore the backup file into the server.
                 * But when we rename the current database in the server: the connection will fail to process (in Database class).
                 * 
                 * In this case, we will create a backup database and ensure that it matches with the current one,
                 * then we truncate the current database permanently then we restore the backup file into it.
                 * 
                 * This is the only-one safe way to do this scenario similar to that process in the SQLite3 case.
                 */

                if (in_array($restore_ext, ['sqlite3'])) {
                  $this->output("SQLite3 Backup to MySQL Restore Error: Restoring an SQLite3 backup file into a MySQL database is not supported due to database format differences.");
                  die;
                }
                else {
                  // everything looks good for now.
                  // let's backup the current database
                  $current_db = env('database');
                  $backup_db  = "{$current_db}_{$now}";
                  $tables = \R::inspect();
                  $response = ['status' => false, 'reason' => null];

                  try {
                    \R::exec("create database if not exists `{$backup_db}`;");
                    \R::exec("alter database `{$backup_db}` collate `utf8mb4_unicode_ci`;");
                    \R::exec("use `{$backup_db}`;"); // to use it as a backup
                    $response = ['status' => true, 'reason' => null];
                  }
                  catch (\Exception $e) {
                    $response = ['status' => false, 'reason' => $e->getMessage()];
                  }

                  if ($response['status']) {
                    // yes, creating backup database has been successfully done.
                    // let's move all tables into it.
                    foreach ($tables as $table) {
                      \R::exec("rename table `{$current_db}`.`{$table}` to `{$backup_db}`.`{$table}`;");
                    }

                    // now, all tables are backed up as well.
                    // let's restore the $file backup file into the current truncated empty database...
                    //\R::exec("use `{$current_db}`;");
                    $file = preg_replace("/^--?.+/", '', $file);
                    $file = preg_replace('/\/\*.+?\*\/;?/s', '', $file);
                    $file = explode(";
", $file);    // this is to explode the queries into array to loop through it all.

                    \R::exec("use `{$current_db}`;"); // to write on the current truncated backed-up database
                    foreach ($file as $key => $line) {
                      if (!empty($line)) \R::exec($line);
                    }

                    /**
                     * The only-one remaining thing is to change the database 
                     */

                    $this->output("Database backup has been restored successfully.");
                  }
                  else {
                    $this->output("Unable to create a backup for the existing database. Process is terminated.");
                  }
                }
              }
            }
            else {
              $this->output("Error: backup file's extension is not allowed.");
              die;
            }
          }
          else {
            // file doesn't exist.
            $this->output("Failed to load the database backup file.");
          }
        }
      }
    }
    else {
      $this->output(<<<database
        Ornata Database Recovery

        To get started, this command has these options:
        - backup: Creates a database backup. Passing a value will set it as file name.
                  Examples:
                    `php ornata database --backup`.
                    `php ornata database --backup my-db-file`.
                    `php ornata database --backup "My database backup"`.
                    `php ornata database --backup="My database backup"`.

        - restore: Restores a database backup from your file path.
                   Examples:
                     `php ornata database --restore "in-project/folder/path/to/file.sql"`.
                     `php ornata database --restore "in-project/folder/path/to/file.sqlite3"`.
                     `php ornata database --restore "in-project/folder/path/to/file.sqlbackup"`.
                     `php ornata database --restore="C:\\full\\path\\to\\file.sqlite3"`.

        Important notes:
        - When restoring an SQLite3 database file, the current database will be preserved
          by auto-renaming it as a backup file to avoid any unexpected loss.
        - When restoring a MySQL database file, the current online database will be preserved
          by auto-renaming it as a backup database to avoid any unexpected loss.
        - If the auto-renaming of the database fails for any reason, the restoration process
          will not proceed to ensure the preservation of the current database.
      database, true, true);
      die;
    }
  }

  private function testing(bool|string|array|null ...$all) {
    if (count($all) > 0) {
      require_once "Testing.php";
      $this->check_console_width(145);

      array_map(fn($t) =>  require_once $t, [...glob("tests/*.php"), ...glob("tests/**/*.php")]);
      $tests    = $all['all'] // this means that the user calls `ornata testing --all`
        ? array_map(fn($t) => \Testing\Testing::run($t), array_keys(\Testing\Testing::$tests))
        : array_map(fn($t) => \Testing\Testing::run($t), $all);

      $builder  = new \AsciiTable\Builder();
      $builder->setTitle("Testing Results");
      $builder->showColumns(['ID', 'Name', 'Function', 'Parameters', 'Expected', 'Actual', 'Status', 'Period (μs)']);
      $builder->addRows($tests);
      $this->output($builder->renderTable(), true, true);
    }
    else {
      $this->output(<<<testing
        Ornata Testing Unit

        To get started, this command has these options:
        - Single testing: Run a single testing case.
            Example: `php ornata testing test_1`

        - Multiple testing: Run multiple testing cases.
            Example: `php ornata testing test_1 test_2 test_3 test_4 ...`

        - All testing: Run all registered testing cases at once.
            Example: `php ornata testing --all`

        Locate your testing files in the `tests` directory.
        All testing file will be loaded automatically in the CLI.
      testing, true, true);
    }
  }

  private function zip(string $source, string $destination, array $excluded = []) {
    $this->check_for_zip();

    if (!file_exists($source)) {
      return "Source folder not found: {$source}";
    }

    $zip = new \ZipArchive();
    if (!$zip->open($destination, \ZIPARCHIVE::CREATE | \ZIPARCHIVE::OVERWRITE)) {
      return "Failed to create zip file: {$destination}";
    }

    // Get the absolute path for the source folder
    $source = realpath($source);

    $iterator = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator($source),
      \RecursiveIteratorIterator::SELF_FIRST
    );

    // Convert exclusion list to lowercase for case-insensitive matching
    $excluded = count($excluded) > 0 ? array_map('strtolower', $excluded) : $excluded;
    $content = [];
    $i = 0;

    foreach ($iterator as $file) {
      $filePath = $file->getRealPath();
      $relativePath = substr($filePath, strlen($source) + 1);
      $fileName = strtolower($file->getFilename()); // Get filename in lowercase for case-insensitive matching

      if (in_array($fileName, $excluded, true) || count(array_intersect(explode(DIRECTORY_SEPARATOR, $relativePath), $excluded)) > 0) {
        continue; // Skip excluded files and folders
      }

      // Check for complete subfolder path match (case-insensitive)
      $isExcludedSubfolder = false;
      foreach ($excluded as $excludedPath) {
        if (strpos($relativePath, $excludedPath) === 0) {
          $isExcludedSubfolder = true;
          break;
        }
      }

      if ($isExcludedSubfolder) {
        continue; // Skip excluded files and folders
      }

      $content[] = $file;
    }

    foreach ($content as $file) {
      $i++;
      $filePath = $file->getRealPath();
      $relativePath = substr($filePath, strlen($source) + 1);
      if ($file->isDir()) {
        // Add empty directory
        $zip->addEmptyDir($relativePath);
      }
      else {
        // Add file with relative path
        $zip->addFile($filePath, $relativePath);
      }

      // we've disabled outputting the progressbar due to supreme lazy process.
      // echo (new \ProgressBar(count($content)))->drawCurrentProgress($i);
      // ob_flush();
    }

    $zip->close();
    return true;
  }

  private function ftp() {
    $this->check_for_zip();

    $host = $this->prompt(color("purple", "[PROMPT]  ") . "Enter FTP address:");

    if (!$host) {
      $this->output(color("red", "[ERROR]   ") . 'Error in FTP address: no address is provided');
      die;
    }
    else if (stripos($host, '://') > 0) {
      $this->output(color("red", "[ERROR]   ") . "Error in FTP address: no `://` prefix should be.");
      die;
    }
    else if (filter_var($host, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) and !preg_match('/^(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/', $host)) {
      // it's possibly be IP
      $this->output(color("red", "[ERROR]   ") . "Error in FTP address: address IP is not valid.");
      die;
    }
    else if (!filter_var($host, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) and !preg_match('/^(?!^([0-9]{1,3}\.){3}[0-9]{1,3}$)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i', $host)) {
      $this->output(color("red", "[ERROR]   ") . "Error in FTP address: address is not valid.");
      die;
    }

    $port = (int) $this->prompt(color("purple", "[PROMPT]  ") . 'Enter FTP port', 21);

    if ($port <= 0) {
      $this->output(color("red", "[ERROR]   ") . 'Error in FTP port: no port is provided');
      die;
    }

    $user = $this->prompt(color("purple", "[PROMPT]  ") . 'Enter FTP username');

    if (!$user) {
      $this->output(color("red", "[ERROR]   ") . 'Error in FTP username: no username is provided');
      die;
    }

    $pass = $this->prompt(color("purple", "[PROMPT]  ") . 'Enter FTP password');

    if (!$pass) {
      $this->output(color("red", "[ERROR]   ") . 'Error in FTP password: no password is provided');
      die;
    }

    $this->output(color('yellow', "[INFO]    ") . "Due to terminal environment, creating a zip file for a whole project may take much several minutes.");
    $this->output(color('yellow', "[INFO]    ") . "Shortening the time, you may pass your zipped file path to upload it immediately.");
    $this->output(color('yellow', "[INFO]    ") . "However, Ornata may help you to compress your necessary project's file and upload it to your webhosting.");
    $zipped_file = $this->prompt(color("purple", "[PROMPT]  ") . 'Do you have a pre-zipped file to upload? or Do you want Ornata to do all stuff?'
    . PHP_EOL .
    '          If a pre-zipped file exists, type the relative path of your zipped file.'
    . PHP_EOL .
    '          However, pressing Enter will create a zipped file automatically', "Compress");

    if ($zipped_file == 'Compress') {
      // $zipped_file = 'Compress';
    }
    else if (!in_array(pathinfo($zipped_file, PATHINFO_EXTENSION), ['zip'])) {
      $this->output(color('red', '[ERROR]   ') . "Invalid file `" . color('yellow', $zipped_file) . "`. Only `" . color('yellow', '.zip') . "` files are allowed");
      die;
    }
    else if (!is_file($zipped_file)) {
      $this->output(color('red', '[ERROR]   ') . "Zipped file `" . color('yellow', $zipped_file) . "` is not found.");
      die;
    }
    else if (!is_readable($zipped_file)) {
      $this->output(color('red', '[ERROR]   ') . "Couldn't read `" . color('yellow', $zipped_file) . "` file. Check for its permissions.");
      die;
    }
    else {
      if (!extension_loaded('zip')) {
        $this->output(color('red', "[ERROR]") . " Zip extension is required to run this command.");
        die;
      }

      $zip = new \ZipArchive();

      if ($zip->open($zipped_file, \ZipArchive::CHECKCONS) !== true) {
        $this->output(color("red", "[ERROR]   ") . "Invalid corrupted file `" . color('yellow', $zipped_file) . "`.");
        die;
      }
    }

    // all data has been provided, let's try to connect.

    $this->output(color("light-cyan", "[STATUS]  ") . "Connecting to FTP server ...");
    $ftp = ftp_connect($host, $port);

    if (!$ftp) {
      $this->output(color("red", "[ERROR]   ") . "Error: failed to connect to the FTP server.");
      die;
    }
    else {
      $this->output(color("green", "[SUCCESS] ") . "Connected to FTP server: " . color("green", "{$host}:{$port}"));
      $this->output(color("light-cyan", "[STATUS]  ") . "Logging in ...");

      if (!ftp_login($ftp, $user, $pass)) {
        $this->output(color("red", "[ERROR]   ") . "Error in login: credentials mismatching.");
        die;
      }
      else {
        $this->output(color("green", "[SUCCESS] ") . "Logged in: " . color("green", $user));

        if ($zipped_file == 'Compress') {
          /** Developer didn't create a zip folder for the project.
           * We'll create it whatever it takes a time. We've warned him anyway.
           */
          $this->output(color("light-cyan", "[STATUS]  ") . "Compressing your project ...");

          $time = time();
          $excluded = [
            '.git', '.sass-cache', '.idea', '.vscode', '.backups', '.cache', '.logs', 'node_modules', 'gulpfile.js',
            'assets/core/js/src', 'documentation/src',
          ];

          $zipped_file = ".cache/ornata-{$time}.zip";
          $this->zip('.', $zipped_file, $excluded);
          $this->output(color('green', '[SUCCESS] ') . "Successfully compressed your project `" . color('green', $zipped_file) . "`.");
        }
        else {
          /**
           * Developer has zipped his project to shorten the time.
           * Thanks for Allah because we won't stuck with the compressing the zip file time.
           */
          $this->output(color('light-cyan', '[STATUS]  ') . "Retrieving your zip file: " . color('green', $zipped_file));
        }

        // Enabling passive mode
        ftp_pasv($ftp, true);

        // Now, we're going to upload the zipped file into the webhosting FTP server.
        $handle = fopen($zipped_file, "r");

        if (!$handle) {
          $this->output(color("red", "[ERROR]   ") . "Failed to load `" . color('green', $zipped_file) . "`.");
          die;
        }
        else {
          $remote_dir = $this->prompt(color('purple', '[PROMPT]  ') . "Enter destination uploading folder", '/');

          if (!$remote_dir) {
            $this->output(color("red", "[ERROR]   ") . "Destination uploading folder is mandatory.");
          }
          else {
            $this->output(color('light-cyan', '[STATUS]  ') . "Preparing to upload file ...");
            $this->output(color('light-cyan', '[STATUS]  ') . "Uploading ...");

            $zipped_file_size = filesize($zipped_file);
            $remote_path      = "{$remote_dir}/" . basename($zipped_file);

            if (extension_loaded('curl')) {
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, "ftp://{$host}:{$port}/{$remote_path}");
              curl_setopt($ch, CURLOPT_USERPWD, "{$user}:{$pass}");
              curl_setopt($ch, CURLOPT_UPLOAD, 1);
              curl_setopt($ch, CURLOPT_INFILE, $handle);
              curl_setopt($ch, CURLOPT_INFILESIZE, $zipped_file_size);
              curl_setopt($ch, CURLOPT_FTP_CREATE_MISSING_DIRS, true); // Create missing directories
              // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return the transfer as a string
              curl_setopt($ch, CURLOPT_NOPROGRESS, false); // Enable progress tracking
              curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, function ($resource, $downloadSize, $downloaded, $uploadSize, $uploaded) use($zipped_file_size) {
                if ($uploadSize == 0 and $uploaded == 0) {
                  // do nothing because it's been started recently.
                }
                else if ($uploadSize > 0) {
                  echo (new \ProgressBar($zipped_file_size))->drawCurrentProgress($uploaded);
                }
                else {
                  static $startTime;
                  $startTime ??= time();
                  $elapsed = time() - $startTime;
                  $d = $uploaded <= 1048576 ? round($uploaded / 1024, 2) . " KB/s" : round($uploaded / 1024 / 1024, 2) . " MB/s";;
                  $s = '';
                  $o = '';

                  if ($elapsed > 0) {
                    $speed = ($uploaded / $elapsed); // speed in bytes
                    $s = "• Speed: (".
                      ($speed <= 1048576
                        ? (round($speed / 1024, 2) . " KB/s")
                        : (round($speed / 1024 / 1024, 2) . " MB/s"))
                    .")";
                  }

                  $o = "\r          {$d} {$s}";
                  $this->output($o . str_repeat(' ', max(0, $this->getTerminalWidth() - strlen($o))), eol_normal: false);
                }

                ob_flush(); // necessary for cli instant refreshing
              });

              curl_exec($ch);
              curl_close($ch);
            }
            else {
              $this->output(color('yellow', '[INFO]    ') . "cURL extension is not enabled. Switching to FTP mode.");
              $this->output(color('light-cyan', '[STATUS]  ') . "Uploading ...");

              // Reset the file pointer to the beginning of the file
              rewind($handle);

              // Upload the entire file in one operation
              if (!ftp_fput($ftp, $remote_path, $handle, FTP_BINARY)) {
                $this->output(color('red', '[STATUS]  ') . "Error uploading file.");
              }
            }

            $this->output(color('green', '[SUCCESS] ') . "Successfully uploaded your zipped file `" . color('green', $zipped_file) . "`.");
          }
        }

        $this->output(color('light-cyan', "[STATUS]  ") . "Closing FTP connection ...");

        // Close FTP connection
        ftp_close($ftp);
        fclose($handle);

        $this->output(color('light-cyan', "[STATUS]  ") . "FTP connection closed.");
      }
    }
  }

  //

}

$cli = new \Cli\Cli();
$cli->handle($argv);
