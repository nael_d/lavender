<?php

class ControllerExtended extends Controller {

  public $settings;

  public function __construct() {
    parent::__construct();
    $this->load_model(["CpModel\Settings"]);
    $this->settings = new CpModel\Settings;
    $this->settings->activeOnly = true;

    if ($this->platform == "web" and $this->db_con == "connected") {
      // for website activation, it's done in cronjobs.
      // here is only to check the status and show the coming soon page
      $website_status = $this->settings->preference_get("website-status");

      if (count((array) $website_status['setting_value']) > 0) {
        $activity = ['base_path' => $this->base_path, ...(array) $website_status['setting_value']];

        if ($website_status['setting_image']) {
          $activity['image'] = storage("website-status/{$website_status['setting_image']}");
        }

        if ($activity['status'] == 0) {
          self::View::setDefaultPath('views'); // because 'pages' folder is in the very root of 'views', not 'cp' nor 'web'.
          self::View::render('pages/comingsoon', $activity);
          die(exit);
        }
      }
    }

    $this->extra_params = [
      //
    ];
  }

}