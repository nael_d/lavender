<?php

/**
 * CP base helpers. All functions are needed to run as well.
 * You can customize any options that you want here.
 * The best practise to avoid any conflicts with your functions names is
 *   to prefix them with `cp_` and use `function_exists()`.
 */

