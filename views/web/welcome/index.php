<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="UTF-8" />
  <base href="{{ $base_path }}" />
  <script>
    if (`${location.origin}${location.pathname}` !== document.querySelector('base').getAttribute('href')) {
      location.href = document.querySelector('base').getAttribute('href');
    }
  </script>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  {!! _link('icon', asset('core/pics/ornata-ico.png')) !!}
  @php
    $css = [
      'cp/AdminLTE-3.2.0/libs/fontawesome-free/css/all.min.css',
      'core/libs/bootstrap-5.3.0-alpha1-dist/css/bootstrap.min.css',
      'core/libs/Selectr-master/dist/selectr.min.css',
      'core/libs/tippyjs/tippy.css',
      'core/css/core.css',
    ];
    $js = [
      'cp/AdminLTE-3.2.0/libs/jquery-v3.6.0/jquery.js',
      'core/libs/popperjs@1.16.1/popper.min.js',
      'core/libs/tippyjs/tippy.js',
    ];
  @endphp
  @foreach ($css as $key => $value) {!! css($value) !!} @endforeach
  <title>Welcome :: Ornata</title>
</head>
<body class='welcome-page'>
  <header>
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-auto d-flex align-items-center">
          {!! a('', img(asset('core/pics/ornata-text-1.png')), ['header-logo'], attrs: ['data-faster-link' => '/']) !!}
        </div>
        <div class="col-auto">
          {!! a_icon('//', 'fas fa-minus', class: ['color-8-text', 'header-link', 'header-view-changer'], attrs: ['data-toggle' => 'tippyjs', 'data-tippy-content' => 'Change header appearance']) !!}
          {!! a_icon('https://gitlab.com/nael_d/ornata', 'fab fa-gitlab', open_new_tab: true, class: ['color-1-text', 'header-link'], attrs: ['data-toggle' => 'tippyjs', 'data-tippy-content' => 'Ornata on Gitlab']) !!}
          {!! a_icon('https://discord.gg/U7qv7SMJ8H', 'fab fa-discord', open_new_tab: true, class: ['color-8-text', 'header-link'], attrs: ['data-toggle' => 'tippyjs', 'data-tippy-content' => 'Ornata on Discord']) !!}
        </div>
      </div>
    </div>
  </header>

  <main>
    <div class="container py-5">
      <div class="row justify-content-center align-items-center" style="min-height: calc(100vh - 275px);">
        <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 col-xxl-3">
          <img
            class="d-block w-100 mx-auto rounded-4 shadow mb-3"
            src='assets/core/pics/welcome-1.jpg'
            style="max-width: 250px;"
          />
        </div>
        <div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9 col-xxl-9">
          <div class="text-center">
            <h1 class='h1 mb-3'>The journey has begun</h1>
            <p class="lead">We believe development should be empowering, not limiting.</p>
            <h6 class="h6">Whether you're a seasoned developer tackling complex projects or a passionate beginner bringing your vision to life, Ornata empowers you to code with confidence and build anything you can imagine.</h6>
            <h6 class="h6">As a reminder, run <kbd>php ornata documentation</kbd> command in your project's console to dive in-depth.</h6>
          </div>
        </div>
      </div>
    </div>
  </main>

  <footer class="py-3">
    <div class="container">
      <div class='text-muted text-center'>
        {!! a("https://www.ornata.dev", "Ornata", open_new_tab: true) !!} website is powered by OrnataJS, a beta sub-project of Ornata.
      </div>
      <div class='text-muted text-center mt-1'>
        <small>Developed with <i class="fas fa-heart"></i> by {!! a('http://naeldahman.me', 'Mohammad Nael Dahman', ['color-5-text'], true) !!} for a better web.</small>
        <br />
        {{ file_get_contents("ornata-version") }} ● &copy; 2018-{{ date("Y") }}
      </div>
    </div>
  </footer>

  @foreach ($js as $key => $value) {!! js($value) !!} @endforeach
  <script>
    tippy("[data-toggle='tippyjs']");

    $('.header-view-changer').on({
      click: function () {
        $('header').toggleClass('view-2');
      },
    });
  </script>
</body>
</html>