<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="UTF-8" />
  <base href="{{ $base_path }}" />
  <script>
    if (`${location.origin}${location.pathname}` !== document.querySelector('base').getAttribute('href')) {
      location.href = document.querySelector('base').getAttribute('href');
    }
  </script>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  {!! _link('icon', asset('core/pics/ornata-ico.png')) !!}
  @php
    $css = [
      'cp/AdminLTE-3.2.0/libs/fontawesome-free/css/all.min.css',
      'core/libs/bootstrap-5.3.0-alpha1-dist/css/bootstrap.min.css',
      'core/libs/Selectr-master/dist/selectr.min.css',
      'core/libs/tippyjs/tippy.css',
      'core/css/core.css',
    ];
    $js = [
      'core/js/ornata.new.legacy.min.js',
      'cp/AdminLTE-3.2.0/libs/jquery-v3.6.0/jquery.js',
      'core/libs/bootstrap-5.3.0-alpha1-dist/js/bootstrap.bundle.min.js',
      'core/libs/Selectr-master/dist/selectr.min.js',
      'core/libs/popperjs@1.16.1/popper.min.js',
      'core/libs/tippyjs/tippy.js',
      'core/js/setup.js',
    ];
  @endphp
  @foreach ($css as $key => $value) {!! css($value) !!} @endforeach
  <title>Welcome :: Ornata</title>
</head>
<body class='welcome-page'>
  <header>
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-auto d-flex align-items-center">
          {!! a('', img(asset('core/pics/ornata-text-1.png')), ['header-logo'], attrs: ['data-faster-link' => '/']) !!}
        </div>
        <div class="col-auto">
          {!! a_icon('//', 'fas fa-minus', class: ['color-8-text', 'header-link', 'header-view-changer'], attrs: ['data-toggle' => 'tippyjs', 'data-tippy-content' => 'Change header appearance']) !!}
          {!! a_icon('https://gitlab.com/nael_d/ornata', 'fab fa-gitlab', open_new_tab: true, class: ['color-1-text', 'header-link'], attrs: ['data-toggle' => 'tippyjs', 'data-tippy-content' => 'Ornata on Gitlab']) !!}
          {!! a_icon('https://discord.gg/U7qv7SMJ8H', 'fab fa-discord', open_new_tab: true, class: ['color-8-text', 'header-link'], attrs: ['data-toggle' => 'tippyjs', 'data-tippy-content' => 'Ornata on Discord']) !!}
        </div>
      </div>
    </div>
  </header>

  <main data-faster-app></main>

  <div
    data-faster-loading
    style="display: flex; flex-direction: column; justify-content: center; align-items: center; gap: 20px;
      z-index: 3; position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: white;">
    <div class="logos-row d-flex" style="gap: 16px;">
      <img src="assets/core/pics/ornata-logo-2.png" alt="Ornata logo" style="max-height: 85px;" />
      <img src="assets/core/pics/ornata-text-1.png" alt="Ornata word" style="max-height: 85px;" />
    </div>
    <div class="ornata-loader"></div>
  </div>

  <footer class="py-3">
    <div class="container">
      <div class='text-muted text-center'>
        {!! a("https://www.ornata.dev", "Ornata", open_new_tab: true) !!} website is powered by OrnataJS, a beta sub-project of Ornata.
      </div>
      <div class='text-muted text-center mt-1'>
        <small>Developed with <i class="fas fa-heart"></i> by {!! a('http://naeldahman.me', 'Nael Dahman', ['color-5-text'], true) !!} for a better web.</small>
        <br />
        {{ file_get_contents("ornata-version") }} ● &copy; 2018-{{ date("Y") }}
      </div>
    </div>
  </footer>

  @foreach ($js as $key => $value) {!! js($value) !!} @endforeach
</body>
</html>