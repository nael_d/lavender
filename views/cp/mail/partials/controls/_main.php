<div class="mailbox-controls">
  <button type="button" class="btn btn-default btn-sm checkbox-toggle"
    data-toggle='tippyjs' data-tippy-content='Select all' data-tippy-appendTo='parent'>
    <i class="far fa-square"></i>
  </button>
  <button type="button" class="btn btn-default btn-sm bulk-delete"
    data-table="mail" data-column="mail"
    data-toggle='tippyjs' data-tippy-content='Delete' data-tippy-appendTo='parent'>
    <i class="far fa-trash-alt"></i>
  </button>
  <button type="button" class="btn btn-default btn-sm bulk-export-selected"
    data-table="mail" data-module="Mail"
    data-toggle='tippyjs' data-tippy-content='Export to CSV' data-tippy-appendTo='parent'>
    <i class="fas fa-file-excel"></i>
  </button>
  <h6 class="float-right mb-0 mt-1"><b>({{ count($mails) . " mail" . (count($mails) > 1 ? 's' : '') }})</b></h6>
</div>
