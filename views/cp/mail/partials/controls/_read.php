<div class="px-2">
  <div class="btn-group" style='width: 80px;'>
    @if ($mail['mail_is_active'] == 1)
      <a href="{{ path("cp/mail/{$mail['id_mail']}/delete") }}"
        data-toggle="tippyjs" data-tippy-placement="top" title="Delete" data-tippy-appendTo='parent'
        class="btn btn-default btn-sm" data-tippy-content="Delete">
        <i class="far fa-trash-alt"></i>
      </a>
    @else
      <a href="{{ path("cp/mail/{$mail['id_mail']}/restore") }}"
        data-toggle="tippyjs" data-tippy-placement="top" title="Restore"
        data-tippy-appendTo='parent'
        class="btn btn-default btn-sm" data-tippy-content="Restore">
        <i class="fas fa-trash-restore-alt"></i>
      </a>
    @endif
  
    <button type="button" class="mail-print btn btn-default btn-sm"
      data-toggle="tippyjs" data-tippy-placement="top" data-tippy-content="Print"
      data-tippy-appendTo='parent'>
      <i class="fas fa-print"></i>
    </button>
  </div>
</div>
