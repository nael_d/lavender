<div class="card">
  <div class="card-header">
    <h3 class="card-title">Folders</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse">
        <i class="fas fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="card-body p-0">
    <ul class="nav nav-pills flex-column">
      <li class="nav-item active">
        <a href="{{ path('cp/mail') }}" class="nav-link">
          <i class="fas fa-inbox"></i> Inbox
          <span class="badge bg-warning float-right">{{ self::$viewbag['counters']['inbox'] }}</span>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ path('cp/mail/unread') }}" class="nav-link">
          <i class="fas fa-envelope-open-text"></i> Unread
          <span class="badge bg-info float-right">{{ self::$viewbag['counters']['unread'] }}</span>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ path('cp/mail/trash') }}" class="nav-link">
          <i class="far fa-trash-alt"></i> Trash
          <span class="badge bg-danger float-right">{{ self::$viewbag['counters']['trash'] }}</span>
        </a>
      </li>
    </ul>
  </div>
</div>