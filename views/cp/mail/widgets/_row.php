@php $mail = $attributes['mail']; @endphp

<tr>
  <td class="mailbox-icheck">
    <div class="icheck-primary">
      <input data-items-list type="checkbox" id="check{{ $mail['id_mail'] }}" value="{{ $mail['id_mail'] }}" />
      <label for="check{{ $mail['id_mail'] }}"></label>
    </div>
  </td>
  <td class="mailbox-name">{!! a("cp/mail/{$mail['id_mail']}", $mail['mail_sender']) !!}</td>
  <td class="mailbox-subject">{{ $mail['mail_title'] }}</td>
  <td class="mailbox-date">{{ format_date($mail['mail_date_created']) }}</td>
</tr>
