@extends('mail/layout')

@section('mail-content')
  @if (count($mails) > 0)
    <div class="card card-primary card-outline">
      <div class="card-header">
        <div class="card-title">{{ $title }}</div>
      </div>

      <div class="card-body p-0">
        @include('mail/partials/controls/_main')

        <div class="mailbox-messages">
          <table class="table table-hover table-striped table-responsive">
            <tbody>
              @foreach ($mails as $key => $mail)
                <x-mailrow :mail="$mail" />
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

      <div class="card-footer p-0">
        @include('mail/partials/controls/_main')
      </div>
    </div>
  @else
    <h3 class="text-center py-5">No mails yet</h3>
  @endif
@endsection

@section('scripts')
  <script>
    let itemsList = [];

    // Enable check and uncheck all functionality
    $('.checkbox-toggle').on({
      click: function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
          // Uncheck all checkboxes
          $(".mailbox-messages input[type='checkbox']").prop('checked', false).trigger('change');
          $('.checkbox-toggle .far.fa-check-square').removeClass('fa-check-square').addClass('fa-square');
          itemsList = [];
        }
        else {
          // Check all checkboxes
          itemsList = [];
          $(".mailbox-messages input[type='checkbox']").prop('checked', true).trigger('change');
          $('.checkbox-toggle .far.fa-square').removeClass('fa-square').addClass('fa-check-square');
        }
        $(this).data('clicks', !clicks);
      },
    });

    // Select one row checkbox
    $("input[type='checkbox'][data-items-list]").on({
      change: function () {
        if ($(this).is(':checked')) {
          itemsList.push($(this).val());
        }
        else {
          itemsList.splice(itemsList.indexOf($(this).val()), 1);
        }
      },
    });

    // Delete selected rows action
    $('.bulk-delete').on({
      click: function () {
        let $this = $(this), table = $this.data('table'), column = $this.data('column');

        if (itemsList.length === 0) {
          swal('error', 'No items are selected to be deleted.');
        }
        else {
          Swal.fire({
            icon: 'question',
            title: 'Are you sure that you want to do this action: Delete?',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !Swal.isLoading(),
            preConfirm: () => {
              let formData = new FormData();
              formData.append('table', table);
              formData.append('column', column);
              formData.append('list', itemsList);

              return fetch(`${cpPath}/bulk`, {
                method: 'POST',
                body: formData,
              }).then(response => {
                return response.json(); // --------------------------------------- <This>
                // { isConfirmed: true, isDenied: false, isDismissed: false, value: {…} }
              }).catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                );
              });
            },
          }).then(result => {
            let serverResponse = result.value;

            if (serverResponse !== undefined && serverResponse.status >= 1) {
              let selector = $('.icheck-primary');
              itemsList.forEach(v => {
                selector.find(`input[value="${v}"]`).parents('tr').remove();
              });
            }
          });
        }
      },
    });

    // Export selected rows to excel
    $('.bulk-export-selected').on({
      click: function () {
        let $this = $(this), table = $this.data('table');

        if (itemsList.length === 0) {
          swal('error', 'No table are selected to be exported.');
        }
        else {
          $.ajax({
            type: "POST",
            url: `${cpPath}/settings/transfer/export`,
            data: { table: table, ids: itemsList },
            beforeSend: function () {
              swal('info', "Exporting ...");
            },
            success: function (response) {
              if (!response.includes('.cache/')) {
                swal('error', "Error in exporting!");
              }
              else {
                swal('success', "Exported!");
                location.href = `${basePath}/${response}`;
              }
            },
            error: function () {
              swal('error', "Error in exporting");
            },
            complete: function () {
              //
            },
          });
        }
      },
    });

    // pagination
    $('table').DataTable({
      ordering: false,
      columns: [{width: '60px'}, {width: '210px'}, {width: '370px'}, {width: '190px'}],
    });
  </script>
  <style>
    table thead, table tfoot { display: none; }
  </style>
@endsection
