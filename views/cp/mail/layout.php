@extends('layout/main')

@section('content')
  <section class="content">
    <div class="row">
      <div class="col-12 col-md-3">
        @include('mail/partials/_folders')
      </div>
      <div class="col-12 col-md-9">
        @yield('mail-content')
      </div>
    </div>
  </section>
@endsection
