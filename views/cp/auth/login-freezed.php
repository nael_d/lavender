@extends('layout/auth')

@section('content')
  <p class="px-0 pb-1 login-box-msg">We noticed several incorrect login attempts.</p>
  <p class="px-0 pb-1 login-box-msg">As a security precaution, logging-in has been temporarily suspended.</p>
  <p class="p-0 login-box-msg">Please try again in {{ humanize_time(env('cp_login_lockout_duration')) }}.</p>
@endsection
