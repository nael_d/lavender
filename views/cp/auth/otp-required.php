@extends('layout/auth')

@section('content')
  <div class="login-box-msg text-center">
    <small>Enter your OTP code to verify your login.</small>
  </div>
  <form method="post" action="{{ self::$global->current_path_full }}" class="mb-0">
    <div class="input-group mb-3">
      <input type="tel" class="form-control" placeholder="OTP" name="otp_code" pattern="[0-9]{6}" maxlength="6" required autofocus />

      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-key"></span>
        </div>
      </div>
    </div>

    <div class="text-center">
      {!! \Csrf\Csrf::embed() !!}
      <button type="submit" class="btn btn-primary btn-block">Verify OTP</button>
    </div>

    @if(session('cp', 'login_status') != '')
      @php
        $error = session('cp', 'login_error');
      @endphp

      @if($error)
        <div class="alert alert-danger text-center mt-3" role="alert">
          <small>{{ $error }}</small>
        </div>
      @endif
    @endif

    {!! a_icon('cp/login/use', ['fas fa-chevron-left', "Go back"], 'btn btn-sm btn-block text-red mt-3') !!}
  </form>
@endsection
