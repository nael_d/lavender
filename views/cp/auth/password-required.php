@extends('layout/auth')

@section('content')
  <p class="login-box-msg">Enter your password to continue.</p>
  <form method="post" action="{{ self::$global->current_path_full }}" class="mb-0">
    <div class="input-group mb-3 justify-content-center">
      {{ session('cp', 'login_username') }}
    </div>

    <div class="input-group mb-3">
      <input type="password" class="form-control" placeholder="Password" name="cp_user_password" required autofocus />
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-lock"></span>
        </div>
      </div>
    </div>

    <div class="text-center">
      {!! \Csrf\Csrf::embed() !!}
      <button type="submit" class="btn btn-primary btn-block">Login</button>

      @if(session('cp', 'login_status') != '')
        @php
          $error = session('cp', 'login_error');
        @endphp

        @if($error)
          <div class="alert alert-danger text-center mt-3" role="alert">
            <small>{{ $error }}</small>
          </div>
        @endif
      @endif

      {!! a_icon('cp/login/use', ['fas fa-chevron-left', "Go back"], 'btn btn-sm btn-block text-red mt-3') !!}
    </div>

  </form>
@endsection
