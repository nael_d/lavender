@extends('layout/auth')

@section('content')
  <div class="login-box-msg text-center">
    <small>Choose how you'd like to authenticate your login.</small>
  </div>

  <form action="use" method="post">
    {!! button('submit', icon('fas fa-fingerprint mx-2') . tag('span', 'Use Fingerprint'), name: 'option', value: 'webauthn', class: 'btn btn-block btn-secondary') !!}
    {!! button('submit', icon('fab fa-google mx-2') . tag('span', 'Use Google Authenticator'), name: 'option', value: 'gauth', class: 'btn btn-block btn-info', if: self::$global->get_gauth()) !!}
    {!! button('submit', icon('fas fa-key mx-2') . tag('span', 'Use Password'), name: 'option', value: 'password', class: 'btn btn-block btn-success') !!}
  </form>

  {!! a_icon('cp/logout', ['fas fa-sign-out-alt', "Logout"], 'btn btn-sm btn-block text-red mt-3') !!}
@endsection('content')
