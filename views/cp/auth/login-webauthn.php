@extends('layout/auth')

@section('content')
  @php
    $error = session('cp', 'login_error');
  @endphp

  @if($error)
    <div class="alert alert-danger text-center mt-3" role="alert">
      <small>{{ $error }}</small>
    </div>
    {!! a_icon('cp/login/use', ['fas fa-chevron-left', "Go back"], 'btn btn-sm btn-block text-red mt-3') !!}
  @else
    <div class="d-flex justify-content-center align-items-center flex-column mx-auto py-4" style="width: fit-content; gap: 20px; max-width: 660px;">
      <div class="ornata-loader"></div>
      <div class="alert m-0 d-none text-center"></div>
    </div>

    {!! js('cp/js/webauthn.js') !!}
  @endif
@endsection

@section('script')
  <script>
    let creds = {!! session('cp', 'webauthn') !!};
    let backLink = `{!! a_icon('cp/login/use', ['fas fa-chevron-left', "Go back"], 'btn btn-sm btn-block text-red') !!}`;

  creds.challenge = base64ToArrayBuffer(creds.challenge);

  if (creds.allowCredentials.length > 0) {
    creds.allowCredentials = creds.allowCredentials.map(c => {
      return {
        ...c,
        id: base64ToArrayBuffer(c.id),
      };
    });
  }

  if (!navigator.credentials) {
    $('.ornata-loader').remove();
    $('.alert').addClass('alert-danger').removeClass('d-none').text(`WebAuthn is not supported in this browser`)
      .after(backLink)
      .parent().removeClass('py-4');
  }
  else {
    let success = false;

    navigator.credentials.get({ publicKey: creds })
    .then(response => {
      fetch(`${basePath}/cp/login/use/webauthn`, {
        body: JSON.stringify({
          type: 'login-webauthn',
          data: response, // Convert the JavaScript object to a JSON string.
        }),
        method: 'POST',
        cache:'no-cache',
        headers: {
          'Content-Type': 'application/json'
        },
      })
      .then(response => response.json())
      .then(result => {
        success = result.success;

        $('.alert').addClass(result.success ? 'alert-success' : 'alert-danger').text(result.details);
        if (result.success) {
          setTimeout(() => { location.href = `${basePath}/cp`; }, 1500);
        }
      })
      .catch(error => {
        $('.alert').addClass('alert-danger').text(`An error occurred. Try again later or contact administrator.`);
      })
      .finally(() => {
        $('.ornata-loader').remove();
        $('.alert').removeClass('d-none')
          .after(!success ? backLink : ``)
          .parent().removeClass('py-4');
      });
    })
    .catch(error => {
      $('.ornata-loader').remove();
      $('.alert').addClass('alert-danger').removeClass('d-none')
        .after(!success ? backLink : ``)
        .parent().removeClass('py-4');

      switch (error.name) {
        case 'NotAllowedError':
          $('.alert').text('You denied the WebAuthn request or the operation was blocked.');
          break;
        case 'InvalidStateError':
          $('.alert').text('This device has been already registered.');
          break;
        case 'AbortError':
          $('.alert').text('The WebAuthn operation was aborted.');
          break;
        case 'SecurityError':
          $('.alert').text('WebAuthn requires a secure connection (HTTPS). Please ensure you are using a secure connection.');
          break;
        default:
          $('.alert').text('An unknown error. Please try again later or contact system admin.');
          break;
      }
    })
    .finally(() => {
    });
  }
  </script>
@endsection
