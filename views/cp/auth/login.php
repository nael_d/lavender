@extends('layout/auth')

@section('content')
  <p class="login-box-msg">Hello! Sign in to start your session.</p>
  <form method="post" action="{{ self::$global->current_path_full }}">
    <div class="input-group mb-3">
      <input type="text" class="form-control" placeholder="Username" name="cp_user_username" required autofocus />
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-id-badge"></span>
        </div>
      </div>
    </div>

    <div class="text-center">
      {!! \Csrf\Csrf::embed() !!}
      <button type="submit" class="btn btn-primary btn-block">Login</button>
    </div>

    @if(session('cp', 'login_error'))
      <div class="alert alert-danger text-center mt-3" role="alert">
        <small>{{ session('cp', 'login_error') }}</small>
      </div>
    @endif

  </form>
@endsection
