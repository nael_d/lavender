<div class="{{ $attributes['class'] }}">
  <div class="info-box">
    <span class="info-box-icon bg-info"><i class="{{ $attributes['icon'] ?? '' }}"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">{{ $attributes['title'] ?? '' }}</span>
      <span class="info-box-number">{!! ($attributes['content'] ? htmlentities($attributes['content']) : null) ?? $contents ?? '' !!}</span>
    </div>
  </div>
</div>
