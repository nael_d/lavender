<div class="card card-outline card-primary">
  <div class="card-header">
    <h3 class="card-title">
      <i class="{{ $attributes['icon'] ?? '' }} mr-2"></i>
      {{ $attributes['title'] ?? '' }}
    </h3>
  </div>
  <div class="card-body">
    {!! $contents !!}
  </div>
</div>
