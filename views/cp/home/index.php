@extends('layout/main')

@section('content')
  <main @class(['dashboard-view'])>
    <div class="row">
      <div class="col-12">
        <div class="card card-outline card-primary">
          <div class="card-body pt-3">
            <p class="lead mb-1">Good to see you again, {{ self::$global->cp_user['cp_user_username'] }}!</p>
          </div>
        </div>
      </div>
    </div>

    @if(self::$global->cp_user['id_cp_user'] == 1)
      <div class="row">
        <h6 class='col-12'>Server insights <small>(Loading this page may experiences a delay of about 15 seconds due to server analyzing).</small></h6>
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Server uptime" icon="fas fa-clock" :content="$stats['server_uptime']" />
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Server IP" icon="fas fa-ethernet" :content="$stats['ip_info']['ip']" />
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Server Location" icon="fas fa-map-marker-alt">
          {!! a("https://www.google.com/maps/@{$stats['ip_info']['loc']},15z", "{$stats['ip_info']['city']}, {$stats['ip_info']['region']}, {$stats['ip_info']['country']}", open_new_tab: true) !!}
        </x-stat>
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Server timezone" icon="fas fa-globe-europe" :content="$stats['timezone']" />
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="CPU usage" icon="fas fa-server">{{ $stats['server_cpuload'] }}%</x-stat>
        @php $os_link = 'fab fa-' . strtolower(PHP_OS_FAMILY); @endphp
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Operating system" :icon="$os_link">
          {{ PHP_OS_FAMILY . ' ' . php_uname('m') }}
        </x-stat>
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="PHP Version" icon="fab fa-php" :content="PHP_VERSION" />
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Memory usage" icon="fas fa-memory" :content="$stats['server_ramusage']" />
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Disk usage" icon="fas fa-hdd" :content="$stats['disk_usage']" />
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Database usage" icon="fas fa-database" :content="$stats['database_usage']" />
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Scheduled maillist mails" icon="fas fa-mail-bulk">{{ $stats['scheduled_mails'] }}</x-stat>
        <x-stat class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3" title="Website status" icon="fas fa-power-off">
          {!! a('cp/settings/website-status', $stats['website_status'] ? 'Active' : 'Inactive') !!}
        </x-stat>
      </div>
    @endif

    <div class="row">
      <div class="order-1 order-sm-2 order-md-3 order-lg-1 order-xl-1 col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3"></div>
      <div class="order-2 order-sm-1 order-md-2 order-lg-2 order-xl-2 col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
        <x-todos :todos="$todos" />
      </div>
      <div class="order-3 order-sm-3 order-md-4 order-lg-3 order-xl-3 col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
        <x-weather :data="$weather" />
        <x-clockwall />
      </div>
    </div>
  </main>
@endsection
