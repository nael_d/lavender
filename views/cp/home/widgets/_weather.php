<x-widget title="Weather" icon="fas fa-cloud-sun">
  @php $data = $attributes['data']; @endphp
  @if(count(get_object_vars($data)) > 0)
    <section class='weather text-center'>
      <div class="weather-last-update text-right text-muted mb-2">{{ format_date($data->current->last_updated_epoch) }}</div>
      <div class="weather-temp">
        <img src='{{ $data->current->condition->icon }}' />
        <span class='d-inline-block'>{{ $data->current->temp_c }}°C</span>
      </div>
      <div class="weather-status text-muted">{{ $data?->current->condition->text }}</div>
      <div class="weather-location text-muted">{{ $data?->location->name }}, {{ $data?->location->country }}</div>
    </section>
  @else
    <h6 class="text-center mb-0">Unable to retrieve weather.</h6>
  @endif
</x-widget>
