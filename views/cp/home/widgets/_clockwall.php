<x-widget title="Clockwall" icon="fas fa-clock">
  <section class="clockwall">
    <div data-date class="text-center text-sm mb-3">Processing ...</div>
    <div data-clockwall-container>
      <div data-hour></div>
      <div data-minute></div>
      <div data-second></div>
    </div>
  </section>

  <script>
    setInterval(() => {
      let
        date = new Date(),
        h = date.getHours(),
        m = date.getMinutes(),
        s = date.getSeconds(),
        d = date.toLocaleDateString('en-US', {
          weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',
        }),
        hrotation = 30 * h + m / 2,
        mrotation = 6 * m,
        srotation = 6 * s === 0 ? 360 : 6 * s;

      document.querySelector("[data-clockwall-container] [data-hour]").style.transform   = `rotate(${hrotation}deg)`;
      document.querySelector("[data-clockwall-container] [data-minute]").style.transform = `rotate(${mrotation}deg)`;
      document.querySelector("[data-clockwall-container] [data-second]").style.transform = `rotate(${srotation}deg)`;

      if (d !== document.querySelector(".clockwall [data-date]").innerText) {
        document.querySelector(".clockwall [data-date]").innerText = d;
      }
    }, 1000);
  </script>
</x-widget>
