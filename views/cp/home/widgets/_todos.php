<x-widget title="Todos" icon="fas fa-list-ul">
  <section class="todos">
    @php $todos = $attributes['todos']; @endphp

    <ul class="list-unstyled">
      @foreach($todos as $key => $todo)
        <li @class(['todo', 'checked' => $todo['cp_todo_is_active'] == 1])>
          <div class="todo-activity">
            <div class="icheck-primary">
              <input
                data-items-list
                type="checkbox"
                value="{{ $todo['id_cp_todo'] }}"
                id="check{{ $todo['id_cp_todo'] }}"
                @checked($todo['cp_todo_is_active'] == 1)
              />
              <label for="check{{ $todo['id_cp_todo'] }}"></label>
            </div>
          </div>
          <div class="todo-title">{{ $todo['cp_todo_title'] }}</div>
          <div class="todo-tools">
            <button type="button" class="btn btn-secondary btn-xs">
              <i class="fas fa-times fa-xs fa-fw"></i>
            </button>
          </div>
        </li>
      @endforeach
    </ul>

    <hr />

    <form action="{{ path('cp/todos/store') }}">
      <div class="input-group mb-3">
        <input type="text" name='todo' class="form-control" placeholder="What do you have to do? ..." required />
        <div class="input-group-append">
          <button class="btn btn-secondary" type="submit" id="todo">
            <i class="fas fa-save fa-fw"></i>
            <span>Save</span>
          </button>
        </div>
      </div>
    </form>
  </section>
</x-widget>

@section('scripts')
  <script>
    $('section.todos form').on({
      submit: function (e) {
        e.preventDefault();
        let $this = $(this),
          controls = $this.find('input, button'),
          todo = $this.find('input');

        if (todo !== '') {
          $.ajax({
            url: `${cpPath}/home/todos/store`,
            type: 'POST',
            data: { todo: todo.val(), },
            dataType: 'json',
            beforeSend: function () {
              controls.prop('disabled', true);
            },
            success: function (data) {
              todo.val('');
              $this.parents('section.todos').find('.list-unstyled').prepend(`<li>${cp_todo(data)}</li>`);
              $('section.todos ul').scrollTop(0);
            },
            error: function () { swal('error', "Todo hasn't been saved. Please try later."); },
            complete: function () {
              controls.prop('disabled', false);
            },
          });
        }
      },
    });

    $('body').on('change', 'section.todos input:checkbox', function () {
      let $this = $(this),
        id_cp_todo = $this.attr('value'),
        current_activity = $this.is(':checked');

      $('section.todos').parent().scrollTop(0);

      $.ajax({
        url: `${cpPath}/home/todos/activity`,
        type: 'POST',
        data: { id_cp_todo: id_cp_todo, },
        beforeSend: function () {
          $this.parents('.todo').toggleClass('checked');
        },
        success: function (data) {
          if (data !== 'ok') {
            $this.prop('checked', !current_activity).parents('.todo').toggleClass('checked');
          }
        },
        error: function () { swal('error', "Todo hasn't been affected. Please try later."); },
        complete: function () {
        },
      });
    });

    $('body').on('click', 'section.todos .todo button', function () {
      let $this = $(this),
        id_cp_todo = $this.parents('.todo').find('input').attr('value');

        $.ajax({
        url: `${cpPath}/home/todos/delete`,
        type: 'POST',
        data: { id_cp_todo: id_cp_todo, },
        dataType: 'json',
        beforeSend: function () {
          $this.parents('.todo').toggleClass('deleting');
        },
        success: function (data) {
          if (data.status === 'ok') {
            $this.parents('.todo').delay(750).slideUp(250, function () {
              $(this).parents('li').remove();
            });
          }
        },
        error: function () {
          swal('error', "Todo hasn't been affected. Please try later.");
          $this.parents('.todo').toggleClass('deleting');
        },
        complete: function () {
        },
      });
    });

    function cp_todo(todo) {
      $id = todo.id_cp_todo;
      $title = todo.cp_todo_title;
      $is_checked = todo.cp_todo_is_active === 1 ? 'checked' : '';

      return `
      <div class="todo ${$is_checked}">
        <div class="todo-activity">
          <div class="icheck-primary">
            <input data-items-list type="checkbox" value="${$id}" id="check${$id}" ${$is_checked} />
            <label for="check${$id}"></label>
          </div>
        </div>
        <div class="todo-title">${$title}</div>
        <div class="todo-tools"><button type="button" class="btn btn-secondary btn-xs"><i class="fas fa-times fa-xs fa-fw"></i></button></div>
      </div>
      `;
    }
  </script>
@endsection
