@extends('layout/main')

@section('content')
  @php
    $item   ??= [];
    $url    = 'privacy-policy';
    $prefix = 'setting';
    $href   = "{$url}";
    $path   = path("cp/{$href}/store");
    $view   = count($item) == 0 ? 'create' : 'update';
  @endphp

  <section class="content">
    <form enctype="multipart/form-data" method="post" data-view="{{ $view }}" action="{{ $path }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8" style="z-index: 2;">
          <x-polyglot>
            @php $i = 0; @endphp
            @foreach (self::$global->langs as $language => $lang)
              @php $i++; @endphp
              <div role="tabpanel" id="vert-tabs-{{ $lang }}" aria-labelledby="vert-tabs-{{ $lang }}-tab" @class(['tab-pane', 'text-left', 'fade', 'active show' => $i == 1])>
                <x-textarea title="Text" :prefix="$prefix" column="value" sub_column="text" :lang="$lang" :item="$item" :is_wysiwyg="true" />
              </div>
            @endforeach
          </x-polyglot>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4"></div>

        <div class="col-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fas fa-save mr-1"></i>
              <span>Save</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection
