@extends('layout/main')

@section('content')
  @php
    $item   ??= [];
    $url    = '';
    $prefix = '';
    $href   = count($item) == 0 ? $url : "{$url}/{$item["id_{$prefix}"]}";
    $path   = path("cp/{$href}/store");
    $view   = count($item) == 0 ? 'create' : 'update';
  @endphp

  <section class="content">
    <form enctype="multipart/form-data" method="post" data-view="{{ $view }}" action="{{ $path }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8" style="z-index: 2;">
          <x-polyglot>
            @php $i = 0; @endphp
            @foreach (self::$global->langs as $language => $lang)
              @php $i++; @endphp
              <div role="tabpanel" id="vert-tabs-{{ $lang }}" aria-labelledby="vert-tabs-{{ $lang }}-tab" @class(['tab-pane', 'text-left', 'fade', 'active show' => $i == 1])>
              </div>
            @endforeach
          </x-polyglot>

          <x-neutral></x-neutral>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
          <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
              <x-image title="Main Image" :prefix="$prefix" column="image" storage="slides" :item="$item" :is_required="true" />
            </div>

            <div class="col-12 col-md-6 col-lg-12"></div>
          </div>
        </div>

        <div class="col-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fas fa-save mr-1"></i>
              <span>Save</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection

{{-- <x-input title="" prefix="" column="" sub_column="" lang="" type="" hint="" placeholder="" :item="" :is_rtl="" :is_required="" :is_disabled="" :is_readonly="" /> --}}

{{-- <x-textarea title="" prefix="" column="" sub_column="" lang="" hint="" placeholder="" :item="" :is_rtl="" :is_required="" :is_disabled="" :is_readonly="" :is_wysiwyg="" /> --}}

{{-- <x-select title="" prefix="" column="" sub_column="" :item="" list_showinglang="" :list="[]" list_primarykey="" list_titlekey="" :is_multiple="false" :is_required="" :is_disabled="false" :preset_list="[]" :preset_activity="true" /> --}}

{{-- <x-image title="" prefix="" column="" storage="" :item="" :is_required="" :is_disabled="" /> --}}

{{-- <x-images title="" prefix="" column="" storage="" table="" :item="" :is_required="" :is_disabled="" /> --}}

{{-- <x-files title="" prefix="" column="" storage="" table="" :item="" /> --}}

{{-- <x-hint icon="" title=""></x-hint> --}}
