@extends('layout/main')

@section('content')
  @php $url = '';  @endphp
  <x-section
    :items="$items"
    :url="$url"
    title=""
    name=""
    column=""
    table=""
    limit="10"
    :actions=""
    :action_create=""
    :action_delete=""
    :action_export=""
  />
@endsection
