@extends('layout/main')

@section('content')
  @php
    $item   ??= [];
    $url    = 'contact-us';
    $prefix = 'setting';
    $href   = "{$url}";
    $path   = path("cp/{$href}/store");
    $view   = count($item) == 0 ? 'create' : 'update';
  @endphp

  <section class="content">
    <form enctype="multipart/form-data" method="post" data-view="{{ $view }}" action="{{ $path }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8" style="z-index: 2;">
          <x-polyglot>
            @php $i = 0; @endphp
            @foreach (self::$global->langs as $language => $lang)
              @php $i++; @endphp
              <div role="tabpanel" id="vert-tabs-{{ $lang }}" aria-labelledby="vert-tabs-{{ $lang }}-tab" @class(['tab-pane', 'text-left', 'fade', 'active show' => $i == 1])>
                <x-input title="Address location" :prefix="$prefix" column="value" sub_column="address_location" :lang="$lang" :item="$item" />
                <x-input title="Opening hours" :prefix="$prefix" column="value" sub_column="opening_hours" :lang="$lang" :item="$item" />
              </div>
            @endforeach
          </x-polyglot>

          <x-neutral>
            <x-input title="Email" :prefix="$prefix" column="value" sub_column="email" type="email" :item="$item" :is_required="false" />
            <x-input title="Facebook link" :prefix="$prefix" column="value" sub_column="facebook" type="url" :item="$item" :is_required="false" />
            <x-input title="Twitter link" :prefix="$prefix" column="value" sub_column="twitter" type="url" :item="$item" :is_required="false" />
            <x-input title="Instagram link" :prefix="$prefix" column="value" sub_column="instagram" type="url" :item="$item" :is_required="false" />
            <x-input title="Threads link" :prefix="$prefix" column="value" sub_column="threads" type="url" :item="$item" :is_required="false" />
            <x-input title="TikTok link" :prefix="$prefix" column="value" sub_column="tiktok" type="url" :item="$item" :is_required="false" />
            <x-input title="YouTube link" :prefix="$prefix" column="value" sub_column="youtube" type="url" :item="$item" :is_required="false" />
            <x-input title="LinkedIn link" :prefix="$prefix" column="value" sub_column="linkedin" type="url" :item="$item" :is_required="false" />
            <x-input title="Mobile number" :prefix="$prefix" column="value" sub_column="tel" type="tel" :item="$item" :is_required="false" />
            <x-input title="WhatsApp number" :prefix="$prefix" column="value" sub_column="whatsapp" type="tel" :item="$item" :is_required="false" />
            <x-input title="Viber number" :prefix="$prefix" column="value" sub_column="viber" type="tel" :item="$item" :is_required="false" />
            <x-input title="Telegram number" :prefix="$prefix" column="value" sub_column="telegram" type="tel" :item="$item" :is_required="false" />
            <x-input title="Google maps link" :prefix="$prefix" column="value" sub_column="google_map_location" type="text" :item="$item" :is_required="false" />
            <x-select title="Activity" :prefix="$prefix" column="is_active" :item="$item" :preset_activity="true" hint="This will control the ability of posting messages to the CP Mail." />
          </x-neutral>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
          <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
              <x-image title="Main Image" :prefix="$prefix" column="image" storage="contact-us" :item="$item" :is_required="true" />
            </div>
          </div>
        </div>

        <div class="col-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fas fa-save mr-1"></i>
              <span>Save</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection
