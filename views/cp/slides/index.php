@extends('layout/main')

@section('content')
  <x-section :items="$items" title="Slide title" name="title" column="slide" url="slides" table="slides" limit="10" />
@endsection
