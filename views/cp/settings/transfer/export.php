@extends('layout/main')

@section('content')
  <form method="post" id="exportForm" class="container-narrow">
    <h5 class='h5 text-center'>Select a table to export its contents quickly.</h5>

    <select class='select2bs4' data-placeholder="Select table to export ..." required>
      <option value="" disabled selected></option>
      @foreach ($tables as $table) {!! option($table, ucfirst($table)) !!} @endforeach
    </select>

    <div class="text-center">
      <button class='btn btn-success btn-sm mt-2 p-2 px-3'>
        <i class='fas fa-file-export mr-2'></i> Export
      </button>
      <button type="button" class='generate-blank-excel btn btn-info btn-sm mt-2 p-2 px-3'>
        <i class='fas fa-file-export mr-2'></i> Generate blank file
      </button>
    </div>
  </form>
@endsection

@section('scripts')
  <script>
    $('#exportForm').on({
      submit: function (e) {
        let
          $this = $(this),
          table = $this.find('select').val();

        e.preventDefault();
        $.ajax({
          type: "POST",
          url: `${cpPath}/settings/transfer/export`,
          data: { table: table },
          beforeSend: function () {
            $this.prop('disabled', true);
            swal('info', "Exporting ...");
          },
          success: function (response) {
            // console.log(response);
            if (!response.includes('.cache/')) {
              swal('error', "Error in exporting");
            }
            else {
              swal('success', "Exported!");
              location.href = `${basePath}/${response}`;
            }
          },
          error: function () {
            swal('error', "Error in exporting");
          },
          complete: function () {
            $this.prop('disabled', false);
          },
        });
      },
    });

    $('.generate-blank-excel').on({
      click: function () {
        let table = $('#exportForm select').val();

        if (table === null) {
          swal('error', 'No table are selected to be exported.');
        }
        else {
          $.ajax({
            type: 'POST',
            url: `${cpPath}/settings/transfer/export/generate`,
            data: { table: table },
            beforeSend: function () {
              swal('info', "Generating ...");
            },
            success: function (response) {
              // console.log(response);
              if (!response.includes('.cache/')) {
                swal('error', "Error in generating");
              }
              else {
                swal('success', "Generated!");
                location.href = `${basePath}/${response}`;
              }
            },
            error: function () {
              swal('error', "Error in generating");
            },
          });
        }
      }
    });
  </script>
@endsection
