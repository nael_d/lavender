@extends('layout/main')

@section('content')
  <div class='container-narrow'>
    <p class='text-center'>Backup and restore your data quickly and easily.</p>

    <div class="row">
      <div class="col-6">
        <div class="card text-center">
          <div class="card-body pb-3">
            <div class="card-title mb-3 float-none">Export</div>
            <p class="card-text">Backup your data to Excel file.</p>
            {!! a('cp/settings/transfer/export', "<i class='fas fa-file-export mr-2'></i> Export", ['btn', 'btn-secondary']) !!}
          </div>
        </div>
      </div>

      <div class="col-6">
        <div class="card text-center">
          <div class="card-body pb-3">
            <div class="card-title mb-3 float-none">Import</div>
            <p class="card-text">Restore your data from an Excel file.</p>
            {!! a('cp/settings/transfer/import', "<i class='fas fa-file-import mr-2'></i>Import", ['btn', 'btn-secondary']) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
