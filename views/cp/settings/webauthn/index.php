@extends('layout/main')

@section('content')
  @php $url = 'settings/webauthn';  @endphp
  <x-section
    :items="$items"
    :url="$url"
    title="device_name"
    name="device_name"
    column="cred"
    table="cp_users_creds"
    limit="10"
    :actions="true"
    :action_create="true"
    :action_delete="true"
    :action_export="false"
  />
@endsection
