@extends('layout/main')

@section('content')
  @php
    $item   ??= [];
    $url    = 'settings/webauthn';
    $prefix = 'cred';
    $href   = count($item) == 0 ? $url : "{$url}/{$item["id_{$prefix}"]}";
    $path   = path("cp/{$href}/store");
    $view   = count($item) == 0 ? 'create' : 'update';
  @endphp

  <section class="content">
    <form enctype="multipart/form-data" method="post" data-view="{{ $view }}" action="{{ $path }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8" style="z-index: 2;">
          <x-neutral>
            <x-input
              title="Device Name"
              :prefix="$prefix"
              column="device_name"
              type="text"
              placeholder="Give this device a unique name ..."
              :item="$item"
              :is_rtl="false"
              :is_required="true"
              :is_disabled="false"
              :is_readonly="false"
            />

            <x-select title="Activity" :prefix="$prefix" column="is_active" :item="$item" :preset_activity="true" />
          </x-neutral>
        </div>

        <div class="col-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fas fa-save mr-1"></i>
              <span>Save</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection
