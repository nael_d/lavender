@extends('layout/main')

@section('content')
  <div class="d-flex justify-content-center align-items-center flex-column mx-auto py-4" style="width: fit-content; gap: 20px; max-width: 660px;">
    <div class="ornata-loader"></div>
    <div class="alert m-0"></div>
  </div>

  {!! js('cp/js/webauthn.js') !!}
@endsection

@section('scripts')
  <script>
    let webauthnOptions = {!! $creds !!};

    // Convert the challenge (a random string) from base64 to an ArrayBuffer.
    webauthnOptions.challenge = base64ToArrayBuffer(webauthnOptions.challenge);

    // Convert the user ID (which is also a base64 string) to an ArrayBuffer.
    webauthnOptions.user.id = base64ToArrayBuffer(webauthnOptions.user.id);

    // If there are any credentials to exclude (already registered), convert each credential's id.
    if (webauthnOptions.excludeCredentials && webauthnOptions.excludeCredentials.length > 0) {
      webauthnOptions.excludeCredentials = webauthnOptions.excludeCredentials.map(function(cred) {
        return {
          id: base64ToArrayBuffer(cred.id), // Convert the credential id to an ArrayBuffer.
          type: cred.type,
          transports: ['internal'],
        };
      });
    }

    // ----------------------------------------------------------------------------------------- //

    if (!navigator.credentials) {
      $('.ornata-loader').remove();
      $('.alert').addClass('alert-danger').removeClass('d-none').text(`WebAuthn is not supported in this browser`);
    }
    else {
      navigator.credentials.create({ publicKey: webauthnOptions })
      .then(function(credential) {
        fetch(`${cpPath}/settings/webauthn/create`, {
          body: JSON.stringify(credential), // Convert the JavaScript object to a JSON string.
          method: 'POST',
          cache:'no-cache',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(response => response.json())
        .then(result => {
          // Handle the server response.
          $('.alert').addClass(result.success ? 'alert-success' : 'alert-danger').text(result.details);

          if (result.success) {
            setTimeout(() => { location.href = `${cpPath}/settings/webauthn/${result.id_cred}`; }, 2000);
          }
        })
        .catch(error => {
          // Handle any errors that occur while sending data.
          $('.alert').addClass('alert-danger');
          $('.alert').text(error.details);
        });
      })
      .catch(function(error) {
        // This callback handles errors during the credential creation process.
        $('.alert').addClass('alert-danger');

        switch (error.name) {
          case 'NotAllowedError':
            $('.alert').text('You denied the WebAuthn request or the operation was blocked.');
            break;
          case 'InvalidStateError':
            $('.alert').text('This device has been already registered.');
            break;
          case 'AbortError':
            $('.alert').text('The WebAuthn operation was aborted.');
            break;
          case 'SecurityError':
            $('.alert').text('WebAuthn requires a secure connection (HTTPS). Please ensure you are using a secure connection.');
            break;
          default:
            $('.alert').text('An unknown error. Please try again later or contact system admin.');
            break;
        }
      })
      .finally(() => {
        $('.ornata-loader').remove();
      });
    }
  </script>
@endsection
