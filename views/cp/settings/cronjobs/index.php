@extends('layout/main')

@section('content')
  <div class="content container-narrow">
    @if (count($items) > 0)
      <div class="card">
        <div class="card-body p-0">
          <table class='table table-striped'>
            <thead>
                <tr>
                  <th class="text-center" style="width: 50px">#</th>
                  <th style="width: 80px">Cronjob</th>
                  <th>Description</th>
                  <th class="text-center" style="width: 60px">Tools</th>
                </tr>
              </thead>
              <tbody>
                @php $i = 0; @endphp
                @foreach ($items as $cronjob => $details)
                  @php $i++; @endphp
                  <x-cronjobrow :i="$i" :item="$cronjob" :description="$details" />
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
    @else
      <h2 class="text-center">No data yet</h2>
    @endif
  </div>
@endsection
