@extends('layout/main')

@section('content')
  <section class="content container-narrow">
    @if (count($items) > 0)
      <div class="card">
        <div class="card-body p-0">
          <table class='table table-striped'>
            <thead>
                <tr>
                  <th class="text-center" style="width: 50px">#</th>
                  <th>Task</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($items as $i => $method)
                  <x-cronjobmethod :i="$i" :method="$method" />
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
    @else
      <h2 class="text-center">No data yet</h2>
    @endif
  </section>
@endsection
