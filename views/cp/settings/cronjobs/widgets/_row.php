<tr>
  <td class="text-center">{{ $attributes['i'] }}</td>
  <td>{{ $attributes['item'] }}</td>
  <td>{{ $attributes['description']['description'] }}</td>
  <td class="text-center">
    <a
      data-toggle="tippyjs"
      data-tippy-placement="top"
      data-tippy-appendTo='parent'
      data-tippy-content="View"
      href="{{ path("cp/settings/cronjobs/{$attributes['item']}") }}/view"
      class="btn btn-sm btn-success">
      <i class="fas fa-folder-open"></i>
    </a>
  </td>
</tr>
