@extends('layout/main')

@section('content')
  @php $url = 'settings/users';  @endphp
  <x-section :items="$items" title="Username" name="username" column="cp_user" :url="$url" table="cp_users" limit="10" />
@endsection
