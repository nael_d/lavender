@extends('layout/main')

@section('content')
  @php
    $item   ??= [];
    $url    = 'settings/users';
    $prefix = 'cp_user';
    $href   = count($item) == 0 ? $url : "{$url}/{$item["id_{$prefix}"]}";
    $path   = path("cp/{$href}/store");
    $view   = count($item) == 0 ? 'create' : 'update';
  @endphp

  <section class="content">
    <form enctype="multipart/form-data" method="post" data-view="{{ $view }}" action="{{ $path }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
          <x-neutral>
            <x-input title="Username" :prefix="$prefix" column="username" :item="$item" />
            <x-input title="Password" :prefix="$prefix" column="password" :item="$item" />
            <x-input title="Mobile Number" :prefix="$prefix" column="mobile_number" :item="$item" :is_required="false" />
            <x-input title="Telegram Number" :prefix="$prefix" column="telegram_number" :item="$item" :is_required="false" />
            @if(count($item) > 0)
              @php $preset_list = [['value' => 1, 'title' => 'Activated'], ['value' => '', 'title' => 'Reset OTP']]; @endphp
              <x-select title="OTP Status" :prefix="$prefix" column="gauth_secret" :item="$item" :preset_list="$preset_list"
                hint="If you choose 'Reset OTP', user will need to remove its OTP in the Authenticator application and recreate it." />
            @else
              <x-input type="hidden" :prefix="$prefix" column="gauth_secret" value="generate_otp" />
            @endif
            <x-select title="Activity" :prefix="$prefix" column="is_active" :item="$item" :preset_activity="true" />
          </x-neutral>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4"></div>

        <div class="col-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fas fa-save mr-1"></i>
              <span>Save</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection
