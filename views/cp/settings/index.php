@extends('layout/main')

@section('content')
  @php
    // #17a2b8, #0c687a
    $main_color = '#17a2b8';
    $cards = [
      [
        'icon'  => 'fas fa-fingerprint',
        'color' => $main_color,
        'path'  => path('cp/settings/webauthn'),
        'text'  => "Passkeys",
      ],
      [
        'icon'  => $darkmode ? 'fas fa-moon' : 'fas fa-sun',
        'color' => $main_color,
        'path'  => path('cp/settings/dark-mode'),
        'text'  => "Dark mode: " . ($darkmode ? 'Enabled' : 'Disabled'),
      ],
    ];

    if (session('cp', 'id_cp_user') == '1') {
      $cards = [
        [
          'icon'  => 'fas fa-users',
          'color' => $main_color,
          'path'  => path('cp/settings/users'),
          'text'  => "CP accounts",
        ],
        [
          'icon'  => 'fab fa-google',
          'color' => $main_color,
          'path'  => path('cp/settings/gauth'),
          'text'  => "Google Authenticator: " . ($gauth ? 'Enabled' : 'Disabled'),
        ],
        [
          'icon'  => 'fas fa-key',
          'color' => $main_color,
          'path'  => path('cp/settings/apis'),
          'text'  => "API manager",
        ],
        [
          'icon'  => 'fas fa-exchange-alt',
          'color' => $main_color,
          'path'  => path('cp/settings/transfer'),
          'text'  => "Transfer",
        ],
        [
          'icon'  => 'fas fa-robot',
          'color' => $main_color,
          'path'  => path('cp/settings/cronjobs'),
          'text'  => "Cronjobs",
        ],
        [
          'icon'  => 'fas fa-download',
          'color' => $main_color,
          'path'  => path('cp/settings/backups'),
          'text'  => "Backups",
        ],
        [
          'icon'  => 'fas fa-wifi',
          'color' => $main_color,
          'path'  => path('cp/settings/website-status'),
          'text'  => "Website Status",
        ],
        [
          'icon'  => 'fas fa-undo-alt',
          'color' => $main_color,
          'path'  => path('cp/settings/reset-database'),
          'text'  => "Reset database",
        ],
        ...$cards,
      ];
    }
  @endphp

  <div class="settings-view">
    <div class="row">
      @foreach ($cards as $key => $card)
        <div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2 mb-3">
          <x-settingcard :card="$card" />
        </div>
      @endforeach
    </div>
  </div>
@endsection
