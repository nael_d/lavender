@extends('layout/main')

@section('content')
  @php
    $item   ??= [];
    $url    = 'settings/website-status';
    $prefix = 'setting';
    $href   = "{$url}";
    $path   = path("cp/{$href}/store");
    $view   = count($item) == 0 ? 'create' : 'update';
  @endphp

  <section class="content">
    <form enctype="multipart/form-data" method="post" data-view="{{ $view }}" action="{{ $path }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
          <x-polyglot>
            @php $i = 0; @endphp
            @foreach (self::$global->langs as $language => $lang)
              @php $i++; @endphp
              <div role="tabpanel" id="vert-tabs-{{ $lang }}" aria-labelledby="vert-tabs-{{ $lang }}-tab" @class(['tab-pane', 'text-left', 'fade', 'active show' => $i == 1])>
                <x-input title="Message" :prefix="$prefix" column="value" sub_column="message" :lang="$lang" :item="$item" />
              </div>
            @endforeach
          </x-polyglot>

          <x-neutral>
            <x-select title="Website activity" :prefix="$prefix" column="value" sub_column="status" :item="$item" :preset_activity="true" />
            @php $attrs = ['class' => ['datetimepicker-input'], 'data-toggle' => 'datetimepicker', 'data-target' => '#setting_value-activation_date']; @endphp
            <x-input
              title="Activation date"
              :prefix="$prefix"
              column="value"
              sub_column="activation_date"
              hint="Schedule website activation date while you're away."
              :item="$item"
              :is_required="false"
              :attrs="$attrs"
            />
          </x-neutral>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
          <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
              <x-image title="Background Image" :prefix="$prefix" column="image" storage="website-status" :item="$item" />
            </div>
          </div>
        </div>

        <div class="col-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fas fa-save mr-1"></i>
              <span>Save</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection

@section('private-scripts')
  <script>
    $('#setting_value-activation_date').datetimepicker({
      icons: { time: 'far fa-clock' },
      format: 'DD/MM/YYYY HH:mm',
      minDate: moment().startOf('minute'),
    });
  </script>
@endsection
