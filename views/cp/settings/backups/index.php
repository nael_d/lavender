@extends('layout/main')

@section('content')
  <section class="content container-narrow">
    <div class="backups-controls mb-3">
      <span>Actions:</span>
      <a href="{{ path("cp/settings/backups/create") }}" class="btn btn-primary btn-sm mx-1">
        <i class="fas fa-plus"></i>
        <span>Create</span>
      </a>

      @if (count($items) > 0)
        <a href="{{ path("cp/settings/backups/wipe") }}" class="btn btn-danger btn-sm mx-1">
          <i class="fas fa-trash-alt"></i>
          <span>Delete All</span>
        </a>
      @endif
    </div>

    <div class="backups-container">
      @if (count($items) > 0)
        <div class="card">
          <div class="card-body p-0">
            <table class='table table-striped'>
              <thead>
                  <tr>
                    <th class="text-center" style="width: 40px">#</th>
                    <th>Title</th>
                    <th style="width: 180px">Created at</th>
                    <th style="width: 85px">Size</th>
                    <th class="text-center" style="width: 106px">Tools</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($items as $i => $item)
                    <x-backuprow :item="$item" :i="++$i" />
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      @else
        <h2 class="text-center">No data yet</h2>
      @endif
    </div>
  </section>
@endsection
