@php
  $item       = $attributes['item'];
  $i          = $attributes['i'];
  $path       = path("cp/settings/backups/{$item['filename']}");
  $created_at = format_date($item['created_at']);
  $size       = _filesize($item['size']);
@endphp

<tr>
  <td class="text-center">{{ $i }}</td>
  <td style='word-break: break-word;'>{{ $item['filename'] }}</td>
  <td>{{ $created_at }}</td>
  <td>{{ $size }}</td>
  <td class="text-center">
    <a
      data-toggle="tippyjs"
      data-tippy-placement="top"
      data-tippy-appendTo='parent'
      data-tippy-content="Download"
      href="{{ $path }}/download" download
      class="btn btn-sm btn-success">
      <i class="fas fa-download"></i>
    </a>
    <a
      data-toggle="tippyjs"
      data-tippy-placement="top"
      data-tippy-appendTo='parent'
      data-tippy-content="Delete"
      href="{{ $path }}/delete"
      class="btn btn-sm btn-danger">
      <i class="fas fa-trash-alt"></i>
    </a>
  </td>
</tr>
