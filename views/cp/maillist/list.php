@extends('maillist/layout')

@section('maillist-content')
  @php
    $url      = 'maillist';
    $activity = $ptype == 'scheduled';
  @endphp
  <x-section
    :items="$items"
    :url="$url"
    title="Maillist"
    name="title"
    column="maillist_mail"
    table="maillist_mails"
    limit="10"
    :actions="false"
    :action_create="false"
    :action_delete="false"
    :action_export="false"
    :activity="$activity"
  />
@endsection
