@extends('maillist/layout')

@section('maillist-content')
  @php
    $mail = [
      'action'  => ($item ? "{$item['id_maillist_mail']}/" : '') . 'store',
      'title'   => $item['maillist_mail_title'] ?? '',
      'mail'    => $item['maillist_mail_message'] ?? '',
      'date'    => $item['maillist_mail_date_scheduled'] ? date('d/m/Y H:i', (int) $item['maillist_mail_date_scheduled']) : '',
      'is_sent' => $item['maillist_mail_is_sent'] ?? null
    ];
  @endphp

  <div class="card card-outline card-info">
    <div class="card-header">
      <div class="card-title">{{ $title }}</div>
    </div>

    <div class="card-body">
      @if($mail['is_sent'] != 1)
        <form method="post" action="{{ path("cp/maillist/{$mail['action']}") }}">
      @endif

      <p>All mails are sent from `no-reply@{{ $_SERVER['HTTP_HOST'] }}` address.</p>
      <p>To maintain server stability, scheduled emails will be sent on time, while immediate emails will have a mandatory 10-minute delay.</p>
      <hr />

      <div class="mb-3">
        <label for="maillist_mail_from" class="form-label">Mail from</label>
        <p>no-reply@{{ $_SERVER['HTTP_HOST'] }}</p>
      </div>

      <div class="mb-3">
        <label for="maillist_mail_title" class="form-label">Mail title</label>
        <input type="text" name="maillist_mail_title" id="maillist_mail_title" class="form-control form-control-border"
          placeholder="Mail title" aria-describedby="maillist_mail_title" value="{{ $mail['title'] }}" required />
      </div>

      <div class="mb-3">
        <label for="maillist_mail_message" class="form-label">Mail message</label>
        <textarea id="maillist_mail_message" name="maillist_mail_message" required>{!! $mail['mail'] !!}</textarea>
      </div>

      <div class="mb-3">
        <label for="maillist_mail_date_scheduled" class="form-label">Mail schedule date</label>
        @if ($mail['is_sent'] != 1)
          <input type="text" class="form-control datetimepicker-input form-control-border"
            name="maillist_mail_date_scheduled" id="maillist_mail_date_scheduled"
            data-toggle="datetimepicker" data-target="#maillist_mail_date_scheduled" value="{{ $mail['date'] }}" required />
        @else
          <input type="text" class="form-control form-control-border" name="maillist_mail_date_scheduled" value="{{ $mail['date'] }}" />
        @endif
      </div>

      @if($mail['is_sent'] != 1)
          <div class="mb-3">
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-lg">
                <i class="fas fa-save mr-1"></i>
                <span>Save</span>
              </button>
            </div>
          </div>
        </form>
      @endif
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $('#maillist_mail_date_scheduled').datetimepicker({
      icons: { time: 'far fa-clock' },
      format: 'DD/MM/YYYY HH:mm',
      minDate: moment().startOf('minute'),
    });
  </script>
@endsection
