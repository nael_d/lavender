@extends('layout/main')

@section('content')
  <section class="content">
    <div class="row">
      <div class="col-12 col-sm-3 col-md-3 col-lg-2">
        @include('maillist/partials/_links')
      </div>
      <div class="col-12 col-sm-9 col-md-9 col-lg-10">
        @yield('maillist-content')
      </div>
    </div>
  </section>
@endsection
