@extends('maillist/layout')

@section('maillist-content')
  <div class="row">
    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">
      <x-infobox color="success" icon="fas fa-users" title="Subscribers" :amount="$subs_count" />
    </div>
    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">
      <x-infobox color="info" icon="fas fa-paper-plane" title="Sent" :amount="$sent_mails_count" />
    </div>
    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">
      <x-infobox color="secondary" icon="fas fa-clock" title="Scheduled" :amount="$scheduled_mails_count" />
    </div>
  </div>
@endsection
