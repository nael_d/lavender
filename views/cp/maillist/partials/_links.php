<div class="card card-widget widget-user-2">
  <ul class="nav flex-column">
    <li class="nav-item">{!! a('cp/maillist', 'Overview', ['nav-link']) !!}</li>
    <li class="nav-item">{!! a('cp/maillist/create', 'Create mail', ['nav-link']) !!}</li>
    <li class="nav-item">{!! a('cp/maillist/scheduled', 'Scheduled', ['nav-link']) !!}</li>
    <li class="nav-item">{!! a('cp/maillist/sent', 'Sent', ['nav-link']) !!}</li>
  </ul>
</div>
