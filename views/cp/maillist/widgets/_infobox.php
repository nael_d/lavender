<div class="info-box bg-gradient-{{ $attributes['color'] ?? '' }}">
  <span class="info-box-icon"><i class="{{ $attributes['icon'] ?? '' }}"></i></span>
  <div class="info-box-content">
    <span class="info-box-text">{{ $attributes['title'] ?? '' }}</span>
    <span class="info-box-number">{{ $attributes['amount'] ?? '' }}</span>
  </div>
</div>
