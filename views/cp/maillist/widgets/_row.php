@php
  $row  = $attributes['row'];
  $id   = $row["id_maillist_mail"];
  $path = path("cp/maillist/{$id}");
@endphp

<tr>
  <td class="text-center">{{ $row['id_maillist_mail'] }}</td>
  <td>{{ $row['maillist_mail_title'] }}</td>
  <td class="text-center">
    <a
      data-toggle="tippyjs" data-tippy-placement="top" data-tippy-appendTo='parent' data-tippy-content="View"
      href="{{ $path }}" class="btn btn-sm btn-success">
      <i class="fas fa-eye"></i>
    </a>
  </td>
</tr>
