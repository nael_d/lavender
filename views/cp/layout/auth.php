@php
  $css = [
    'cp/AdminLTE-3.2.0/libs/fontawesome-free/css/all.min.css',
    'cp/AdminLTE-3.2.0/libs/bootstrap-v4.6.1/css/bootstrap.css',
    'cp/AdminLTE-3.2.0/css/adminlte.css',
    'cp/AdminLTE-3.2.0/css/adminlte.fixes.css',
  ];
  $js = [
    'cp/AdminLTE-3.2.0/libs/jquery-v3.6.0/jquery.js',
    'cp/AdminLTE-3.2.0/libs/bootstrap-v4.6.1/js/bootstrap.bundle.min.js',
    'cp/AdminLTE-3.2.0/js/adminlte.js',
  ];
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" type="image/jpg" href="{{ asset('cp/pics/logo.png') }}" />
  @foreach ($css as $key => $value){!! css($value) !!}  @endforeach
  <style>
    input[type=number] {
      /* Chrome, Safari, Edge, Opera */
      /* Firefox */
      -moz-appearance: textfield;
      appearance: textfield;
    }
    input[type=number]::-webkit-outer-spin-button, input[type=number]::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
  </style>
  <title>{{ self::$global->base_title }} CP :: Login</title>
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      {!! a(path(), "<b>" . self::$global->base_title . "</b> CP") !!}
    </div>

    <div class="card">
      <div class="card-body login-card-body">
        @yield('content')
      </div>
    </div>

    <hr />

    <h6 class="text-center">{!! copyright(self::$global->base_title) !!}</h6>
    <div class="text-center text-muted">
      <small>
        {!! powered_by(self::$global->base_title) !!} » {{ file_get_contents("ornata-version") }}
      </small>
    </div>
  </div>

  @foreach ($js as $key => $value){!! js($value) !!}  @endforeach
  <script>
    let
      pathname  = location.pathname.split('/').splice(1),
      basePath  = location.origin + (
                    (location.hostname === 'localhost' || location.hostname.split('.').length === 4)
                    ? `/${([...pathname].slice(0, pathname.indexOf("cp")).join('/'))}` // project folder name in localhost
                    : ''
                  ),
      cpPath    = `${basePath}/cp`;
  </script>
  @yield('script')
</body>
</html>