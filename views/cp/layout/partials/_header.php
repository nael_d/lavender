<nav class="main-header navbar navbar-expand navbar-light {{ self::$global->darkmode == 1 ? 'navbar-dark' : 'navbar-white' }}">
  <div class="container">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="javascript://" role="button">
          <i class="fas fa-bars"></i>
        </a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        {!! a('cp', 'Dashboard', ['nav-link'], match_url: false, match_url_exact: false) !!}
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        {!! a('cp/logout', '<i class="fas fa-sign-out-alt"></i>', ['nav-link'], match_url: false, attrs: ['data-toggle' => 'tippyjs', 'data-tippy-content' => 'Logout', 'data-tippy-appendTo' => 'parent']) !!}
      </li>
    </ul>
  </div>
</nav>

<aside class="main-sidebar main-sidebar-custom- sidebar-dark-primary elevation-4">
  <a href="{{ path('cp') }}" class="brand-link">
    <img
      src="{{ asset('cp/AdminLTE-3.2.0/img/AdminLTELogo.png') }}"
      alt="AdminLTE"
      class="brand-image img-circle elevation-3"
      style="opacity: .8"
    />
    <span class="brand-text font-weight-light">{{ env('base_title') }} CP</span>
  </a>

  <div class="sidebar">
    <ul
      class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-compact"
      data-widget="treeview" role="menu"
      data-accordion="false">
      <div class="user-panel pb-4- mb-3 d-flex">
        <ul class="nav nav-pills nav-sidebar flex-column mt-2">
          @foreach (self::$global->links as $key => $cpath)
            <li @class(['nav-item'])>
              @if (is_array($cpath))
                {!! a( link: ($cpath['link']), text: "<i class='nav-icon {$cpath['icon']}'></i><p>{$cpath['title']}</p>", class: ['nav-link', $cpath['class']], match_url: false, match_url_exact: false) !!}
              @else
                {!! $cpath !!}
              @endif
            </li>
          @endforeach
        </ul>
      </div>
    </ul>
  </div>
</aside>
