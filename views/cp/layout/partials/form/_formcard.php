<div class="card card-primary card-outline">
  <div class="card-header">
    <h3 class="card-title">
      @yield('card-header')
    </h3>

    {{-- <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
    </div> --}}
  </div>

  <div class="card-body">
    @yield('card-content')
  </div>
</div>
