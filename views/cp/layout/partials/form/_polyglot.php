@extends('layout/partials/form/_formcard')

@section('card-header')
  <i class="fas fa-language mr-2"></i><span>Polyglot</span>
@endsection

@section('card-content')
  <div class="row">
    <div class="col-3 col-sm-3 col-md-2 col-lg-2 col-xl-2">
      <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
        @php $i = 0; @endphp
        @foreach (self::$global->langs as $language => $lang)
          @php
            $i++;
            $link = a('//', ucfirst($language), ['nav-link', $i == 1 ? 'active' : ''], attrs: [
              'id'            => "vert-tabs-{$lang}-tab",
              'href'          => "#vert-tabs-{$lang}",
              'data-target'   => "#vert-tabs-{$lang}",
              'aria-controls' => "vert-tabs-{$lang}",
              'aria-selected' => $i == 1 ? 'true' : 'false',
              'data-toggle'   => 'pill',
              'role'          => 'tab',
            ]);
          @endphp
          {!! $link !!}
        @endforeach
      </div>
    </div>
    <div class="col-9 col-sm-9 col-md-10 col-lg-10 col-xl-10">
      <div class="tab-content" id="vert-tabs-tabContent">
        {!! $contents !!}
      </div>
    </div>
  </div>
@endsection
