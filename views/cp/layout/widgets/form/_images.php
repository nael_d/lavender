@extends('layout/partials/form/_formcard')

@section('card-header')
  <i class="fas fa-image mr-2"></i><span>{{ $attributes['title'] ?? '' }}</span>
@endsection

@section('card-content')
  @php
    $item         = $attributes['item'] ?? [];
    $title        = $attributes['title'] ?? '';
    $prefix       = $attributes['prefix'] ?? '';
    $column       = $attributes['column'] ?? '';
    $storage      = $attributes['storage'] ?? '';
    $is_required  = $attributes['is_required'] ?? true;
    $is_disabled  = $attributes['is_disabled'] ?? false;
    $title       .= !$is_required ? ' (Optional)' : '';
    $table        = $attributes['table'] ?? '';
    $name         = "{$prefix}_{$column}";
    $id           = $item["id_{$prefix}"];
    $list         = [];

    foreach ($item[$name] as $key => $value) {
      $exploded = explode('.', $value);
      $thumb = storage("{$storage}/{$exploded[0]}_thumb.{$exploded[1]}");
      $list[] = <<<thumb
        <img class='img-thumbnail cursor-pointer rounded-sm my-2 multiple-images' alt='Image #{$key}' style='max-height: 45px'
          id="{$value}" data-file="{$value}" data-prefix="{$prefix}" data-table="{$table}"
          data-id-item="{$id}" data-column-name="{$column}" data-folder="{$storage}" src="{$thumb}"
        />
      thumb;
    }
  @endphp

  <label for="{{ $name }}">{{ $attributes['title'] ?? '' }}</label>
  <div class="input-group">
    <div class="custom-file">
      <input type="file" multiple accept='.png, .jpg, .jpeg' style="display: none;" class='image-input' id="{{ $name }}" name="{{ $name }}[]"
        @required($is_required) @disabled($is_disabled)
      />
      <label for="{{ $name }}" data-label="{{ $name }}" class='custom-file-label'>Choose files{{ !$is_required ? ' (Optional)' : '' }}</label>
    </div>

    <div class='input-group-append'>
      <button type='button' class='btn btn-muted btn-remove-image-input' data-remove-image-btn='{{ $name }}' disabled>
        <i class='fas fa-times'></i>
      </button>
    </div>
  </div>
  
  <div class="input-images mt-2">{!! join("", $list) !!}</div>
@endsection
