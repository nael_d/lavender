@extends('layout/partials/form/_formcard')

@section('card-header')
  <i @class(['mr-2', $attributes['icon'] ?? ''])></i><span>{{ $attributes['title'] ?? '' }}</span>
@endsection

@section('card-content')
  {!! $contents !!}
@endsection
