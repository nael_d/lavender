@php
  $item         = $attributes['item'] ?? [];
  $title        = $attributes['title'] ?? '';
  $prefix       = $attributes['prefix'] ?? '';
  $column       = $attributes['column'] ?? '';
  $sub_column   = $attributes['sub_column'] ?? '';
  $lang         = $attributes['lang'] ?? null;
  $is_rtl       = $attributes['is_rtl'] ?? false;
  $is_readonly  = $attributes['is_readonly'] ?? false;
  $is_required  = $attributes['is_required'] ?? true;
  $is_disabled  = $attributes['is_disabled'] ?? false;
  $title       .= !$is_required ? ' (Optional)' : '';
  $placeholder  = $attributes['placeholder'] ?? $title;
  $type         = $attributes['type'] ?? 'text';
  $hint         = $attributes['hint'] ?? null;
  $attrs        = $attributes['attrs'] ?? [];
  $classes      = ['form-control', 'form-control-border', 'text-rtl' => $is_rtl];

  if (isset($attrs['class'])) {
    $classes = [...$classes, ...$attrs['class']];
    unset($attrs['class']); // to remove it from $attrs.
  }

  if (count($attrs) > 0) {
    $a = implode(' ', array_map(fn($key, $value) => "{$key}={$value}", array_keys($attrs), $attrs));
  }

  $name   = "{$prefix}_{$column}";
  $value  = $attributes['value'] ?? $item[$name] ?? '';

  if ($sub_column != '') {
    $name .= "-{$sub_column}";
    $value = is_object($value) ? ($value->$sub_column ?? '') : '';
  }

  if ($lang != '') {
    $name .= "_{$lang}";
    $value = is_object($value) ? value_json($value, $lang) : '';
  }

  if (!$is_rtl) {
    $is_rtl = preg_match('/[\x{0590}-\x{083F}]|[\x{08A0}-\x{08FF}]|[\x{FB1D}-\x{FDFF}]|[\x{FE70}-\x{FEFF}]/u', $value) != 0;
  }
@endphp
<div class="form-group">
  <label for='{{ $name }}'>{{ $title }}</label>
  <input type="{{ $type }}" name="{{ $name }}" id="{{ $name }}" value="{{ $value }}" placeholder="{{ $placeholder }}" {{ $a }}
    @class($classes) @readonly($is_readonly) @disabled($is_disabled) @required($is_required)
  />
  @if($hint) <p class="mb-0">{{ $hint }}</p> @endif
</div>
