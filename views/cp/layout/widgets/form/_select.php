@php
  $item             = $attributes['item'] ?? [];
  $title            = $attributes['title'] ?? '';
  $prefix           = $attributes['prefix'] ?? '';
  $column           = $attributes['column'] ?? '';
  $sub_column       = $attributes['sub_column'] ?? '';
  $is_disabled      = $attributes['is_disabled'] ?? false;
  $is_required      = $attributes['is_required'] ?? true;
  $is_multiple      = (isset($attributes['is_multiple']) and $attributes['is_multiple']) ? '[]' : '';
  $preset_list      = $attributes['preset_list'] ?? [];
  $preset_activity  = (isset($attributes['preset_activity']) and $attributes['preset_activity']) ? [['value' => 1, 'title' => 'Activated'], ['value' => 0, 'title' => 'Deactivated']] : [];
  $list_arr         = []; // handles all options, processed `$preset_list` and user-passed `$list`.
  $list             = $attributes['list'] ?? [];
  $list_type        = count($list) === count(array_filter($list, 'is_object')) ? 'object' : 'array'; // object|array
  $list_pk          = $attributes['list_primarykey'] ?? null;
  $list_tk          = $attributes['list_titlekey'] ?? null;
  $list_lang        = $attributes['list_showinglang'] ?? self::$global->default_lang_cp;
  $hint             = $attributes['hint'] ?? null;

  $title  .= !$is_required ? ' (Optional)' : '';
  $name   = "{$prefix}_{$column}";
  $value  = $attributes['value'] ?? $item[$name];

  if ($sub_column != '') {
    $name .= "-{$sub_column}";
    $value = $value ? value_json($value, $sub_column) : null;
  }

  // handling preset list first
  $preset_list = [...$preset_activity, ...$preset_list];
  foreach ($preset_list as $key => $pvalue) {
    $list_arr[] = option(
      $pvalue['value'],
      $pvalue['title'],
      !is_array($value) ? ($value == $pvalue['value']) : (in_array($pvalue['value'], $value))
    );
  }

  // then handling user-passed list
  if (count($list) > 0) {
    if ($list_type != 'object') {
      $value = (array) $value;

      foreach ($list as $key => $vlist) {
        $list_arr[] = option(
          $vlist,
          $vlist,
          in_array($vlist, $value)
        );
      }
    }
    else {
      // object: using $list_pk, $list_tk and $list_lang
      $value_tmp = $value ? json_decode($value) : '';
      if (is_array($value_tmp)) { $value = $value_tmp; }

      foreach ($list as $key => $vlist) {
        $list_arr[] = option(
          $vlist[$list_pk],
          lang_json($vlist[$list_tk], $list_lang),
          $is_multiple == '[]' and is_array($value) ? in_array($vlist[$list_pk], $value) : $value == $vlist[$list_pk]
        );
      }
    }
  }
@endphp

<div class="form-group">
  <label for="{{ $name }}">{{ $title ?? '' }}</label>
  <select name="{{ $name }}{{ $is_multiple }}" id="{{ $name }}" class="form-control select2bs4" @disabled($is_disabled) @required($is_required) {{ $is_multiple == '[]' ? 'multiple' : '' }}>
    <option value="" disabled @selected(!isset($item))>Select ...</option>
    {!! join('', $list_arr) !!}
  </select>
  @if($hint) <p class="mb-0">{{ $hint }}</p> @endif
</div>
