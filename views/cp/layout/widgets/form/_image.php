@extends('layout/partials/form/_formcard')

@section('card-header')
  <i class="fas fa-image mr-2"></i><span>{{ $attributes['title'] ?? '' }}</span>
@endsection

@section('card-content')
  @php
    $blank_img    = 'https://via.placeholder.com/850x250/97a9b9/333?text=Upload+image';
    $item         = $attributes['item'] ?? [];
    $prefix       = $attributes['prefix'] ?? '';
    $column       = $attributes['column'] ?? '';
    $storage      = $attributes['storage'] ?? '';
    $is_required  = $attributes['is_required'] ?? true;
    $is_disabled  = $attributes['is_disabled'] ?? false;
    $name         = "{$prefix}_{$column}";
    $img_src      = (count($item) > 0 and $item[$name]) ? storage("{$storage}/{$item[$name]}") : $blank_img;
  @endphp

  <label for="{{ $name }}">{{ $attributes['title'] ?? '' }}</label>
  <div class="input-group">
    <div class="custom-file">
      <input type="file" accept='image/*' style="display: none;" @required($is_required) @disabled($is_disabled)
        id="{{ $name }}" name="{{ $name }}" data-input="{{ $name }}" class='image-input'
      />
      <label for="{{ $name }}" data-label="{{ $name }}" class='custom-file-label'>Choose file{{ !$is_required ? ' (Optional)' : '' }}</label>
    </div>

    <div class='input-group-append'>
      <button type='button' class='btn btn-muted btn-remove-image-input' data-remove-image-btn='{{ $name }}' disabled>
        <i class='fas fa-times'></i>
      </button>
    </div>
  </div>

  <img data-image="{{ $name }}" data-current-src='' src="{{ $img_src }}" class='d-block img-fluid mt-2 img-thumbnail' />
@endsection
