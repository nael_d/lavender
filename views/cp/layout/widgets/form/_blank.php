@extends('layout/partials/form/_formcard')

@section('card-header')
  <i class="fas fas fa-flag mr-2"></i><span>Blank</span>
@endsection

@section('card-content')
  {!! $contents !!}
@endsection
