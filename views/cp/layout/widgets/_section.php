@php
  $items    = is_array($attributes['items']) ? $attributes['items'] : [];
  $name     = $attributes['name'] ?? '';
  $title    = $attributes['title'] ?? '';
  $column   = $attributes['column'] ?? '';
  $url      = $attributes['url'] ?? '';
  $table    = $attributes['table'] ?? '';
  $limit    = $attributes['limit'] ?? 10;
  $filter   = $attributes['filter'] ?? false;
  $a_create = $attributes['action_create'] ?? true;
  $a_delete = $attributes['action_delete'] ?? true;
  $a_export = $attributes['action_export'] ?? true;
  $actions  = $attributes['actions'] ?? ($a_create or $a_delete or $a_export) ?? true;
  $activity = $attributes['activity'] ?? true;
@endphp

<div class="container-narrow">
  @if($actions)
    <section class="content mb-3">
      <span>Actions:</span>
      @if($a_create)
        <a href="{{ path("cp/{$url}/create") }}" class="btn btn-primary btn-sm mx-1">
          <i class="fas fa-plus"></i>
          <span>Create</span>
        </a>
      @endif

      @if(count($items) > 0)
        @if($a_delete)
          <button class="btn btn-danger btn-sm mx-1 bulk-delete"
            data-table="{{ $table }}" data-column="{{ $column }}">
            <i class="fas fa-trash mx-1"></i>
            <span>Delete</span>
          </button>
        @endif

        @if($a_export)
          <button class="btn btn-success btn-sm mx-1 bulk-export-selected"
            data-table="{{ $table }}" data-column="{{ $column }}">
            <i class="fas fa-file-excel mx-1"></i>
            <span>Export</span>
          </button>
        @endif
      @endif
    </section>
  @endif

  <section class='content'>
    @if(count($items) > 0)
      <div class="card">
        <div class="card-body p-0">
          <div class="pagination-container"></div>
          <table class='table table-striped' data-page-length="{{ $limit }}">
            <thead>
                <tr>
                  <th class="text-center" style="width: 60px">
                    <button
                      data-toggle="tippyjs"
                      data-tippy-placement="top"
                      data-tippy-appendTo='parent'
                      data-tippy-content="Toggle select all"
                      class="btn btn-secondary btn-sm toggle-select-all">
                      <i class="fas fa-check fa-sm"></i>
                    </button>
                  </th>
                  <th class="text-center" style="width: 50px">#</th>
                  <th>{{ $title }}</th>
                  @if($activity)
                    <th class="text-center" style="width: 72px">Activity</th>
                    @else
                    <th class="text-center" style="width: 72px"></th>
                  @endif
                  <th class="text-center" style="width: 60px">Tools</th>
                </tr>
              </thead>
              <tbody>
                @foreach($items as $key => $item)
                  @php
                    $id_item  = $item["id_{$column}"];
                    $title    = lang_json($item["{$column}_{$name}"], self::$global->default_lang_cp) ?? $item["{$column}_{$name}"];
                    $title    = $filter ? a(path("cp/{$url}/filter/{$id_item}"), $title) : $title;
                  @endphp
                  <tr>
                    <td class="text-center">
                      <div class="icheck-primary">
                        <input data-items-list type="checkbox" value="{{ $id_item }}" id="check{{ $id_item }}">
                        <label for="check{{ $id_item }}"></label>
                      </div>
                    </td>
                    <td class="text-center">{{ $id_item }}</td>
                    <td>{{ $title }}</td>
                    @if($activity)
                      <td class="text-center">
                        <input
                          type="checkbox"
                          value="{{ $item["{$column}_is_active"] }}"
                          data-table="{{ $table }}"
                          data-column="{{ $column }}"
                          data-id="{{ $id_item }}"
                          @checked($item["{$column}_is_active"])
                          data-activity-key
                          data-bootstrap-switch
                          data-size="mini"
                          data-off-color="danger"
                          data-on-color="success"
                        />
                      </td>
                    @else
                      <td class="text-center"></td>
                    @endif
                    <td class="text-center">
                      <a data-toggle="tippyjs"
                        data-tippy-placement="top"
                        data-tippy-appendTo='parent'
                        data-tippy-content="View"
                        href="{{ path("cp/{$url}/{$id_item}") }}"
                        class="btn btn-sm btn-success">
                        <i class="fas fa-eye"></i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
      </div>
    @else
      @if($contents)
        {!! $contents !!}
      @else
        <h2 class="text-center">No data yet</h2>
      @endif
    @endif
  </section>
</div>

@section('private-scripts')
  <script>
    let itemsList = [];

    // Select all checkbox
    $('.toggle-select-all').on({
      click: function () {
        let $this = $(this);

        if (!$this.hasClass('toggled')) {
          $this.addClass('toggled');
          $('input[type="checkbox"][data-items-list]').prop('checked', true);
        }
        else {
          $this.removeClass('toggled');
          $('input[type="checkbox"][data-items-list]').prop('checked', false);
        }

        $('input[type="checkbox"][data-items-list]').trigger('change');
      },
    });

    // Select one row checkbox
    $("input[type='checkbox'][data-items-list]").on({
      change: function () {
        if ($(this).is(':checked')) {
          itemsList.push($(this).val());
        }
        else {
          itemsList.splice(itemsList.indexOf($(this).val()), 1);
        }
      },
    });

    // Activity switcher
    $("input[type='checkbox'][data-activity-key]").on({
      'switchChange.bootstrapSwitch': function (event, state) {
        let $this = $(this), table = $this.data('table'), column = $this.data('column'), value = state ? 1 : 0, id = [$this.data('id')];

        $.ajax({
          url: `${cpPath}/bulk`,
          type: 'POST',
          dataType: 'json',
          data: { action: 'activity', table: table, column: column, value: value, list: id, },
          beforeSend: function () {
            swal('info', "Switching activity ...");
          },
          success: function (data) {
            if (data.status === 1) {
              swal('success', 'Updated successfully.');
            }
            else {
              swal('error', 'Not updated.');
            }
          },
          error: function () {
            swal('error', 'Error in updating activity.');
          },
          complete: function () {
            //
          },
        });
      },
    });

    // Delete selected rows action
    $('.bulk-delete').on({
      click: function () {
        let $this = $(this), table = $this.data('table'), column = $this.data('column');

        if (itemsList.length === 0) {
          swal('error', 'No items are selected to be deleted.');
        }
        else {
          Swal.fire({
            icon: 'question',
            title: '<h5>Are you sure that you want to do this action: Delete?</h5>',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !Swal.isLoading(),
            preConfirm: () => {
              let formData = new FormData();
              formData.append('table', table);
              formData.append('column', column);
              formData.append('list', itemsList);

              return fetch(`${cpPath}/bulk`, {
                method: 'POST',
                body: formData,
              }).then(response => {
                return response.json(); // --------------------------------------- <This>
                // { isConfirmed: true, isDenied: false, isDismissed: false, value: {…} }
              }).catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                );
              });
            },
          }).then(result => {
            let serverResponse = result.value;

            if (serverResponse !== undefined && serverResponse.status >= 1) {
              let selector = $('.icheck-primary');
              itemsList.forEach(v => {
                selector.find(`input[value="${v}"]`).parents('tr').remove();
              });
            }
          });
        }
      },
    });

    // Export selected rows to excel
    $('.bulk-export-selected').on({
      click: function () {
        let $this = $(this), table = $this.data('table');

        if (itemsList.length === 0) {
          swal('error', 'No items are selected to be exported.');
        }
        else {
          $.ajax({
            type: "POST",
            url: `${cpPath}/settings/transfer/export`,
            data: { table: table, ids: itemsList },
            beforeSend: function () {
              swal('info', "Exporting ...");
            },
            success: function (response) {
              if (!response.includes('.cache/')) {
                swal('error', "Error in exporting!");
              }
              else {
                swal('success', "Exported!");
                location.href = `${basePath}/${response}`;
              }
            },
            error: function () {
              swal('error', "Error in exporting");
            },
            complete: function () {
              //
            },
          });
        }
      },
    });

    // pagination
    $('table').DataTable({
      ordering: false,
      columns: [{width: '60px'}, {width: '60px'}, null, {width: '60px'}, {width: '60px'}]
    });
  </script>
@endsection
