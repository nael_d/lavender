function online() {
  // console.log('online');

  if ($('.brand-link').hasClass('action-prevented')) {
    // it was offline before. So, show online toast
    onlineAction();
  }

  $('a, form, button').removeClass('action-prevented');
}

function localhost() {
  // console.log('localhost');

  // $('.brand-link, a, form, button').addClass('action-prevented');
  $('.brand-link' /* 'a, form, button' */).addClass('action-prevented');
  localhostAction();
}

function offline() {
  // console.log('offline');

  $('a, form, button').addClass('action-prevented');
  offlineAction();
}

function onlineAction() {
  swal('success', 'You are connected.');
}

function localhostAction() {
  swal('info', 'No internet but localhost.');
}

function offlineAction() {
  swal('error', 'Internet connection is lost.');
}

function connectivityChecker() {
  if (navigator.onLine) {
    // this means that the connection is online
    online();
  }
  else {
    // this means that the connection is offline. let's check that if we are in localhost or not
    if (location.host === 'localhost') {
      localhost();
    }
    else {
      offline();
    }
  }
}

function readURL(input, img) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      img.attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function swal(type, msg) {
  Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    // allowOutsideClick: true,
    timer: 5000
  }).fire({
    icon: type,
    title: msg,
  });
}

function filedrop_file(file) {
  let name = file.name;
  let icon = name.split('.').pop();
  let icons = {
    'jpg':  '<i class="fas  fa-2x fa-fw  fa-image"></i>',
    'jpeg': '<i class="fas  fa-2x fa-fw  fa-image"></i>',
    'png':  '<i class="fas  fa-2x fa-fw  fa-image"></i>',
    'gif':  '<i class="fas  fa-2x fa-fw  fa-image"></i>',
    'mp3':  '<i class="fas  fa-2x fa-fw  fa-music"></i>',
    'wav':  '<i class="fas  fa-2x fa-fw  fa-music"></i>',
    'mp4':  '<i class="fas  fa-2x fa-fw  fa-film"></i>',
    'avi':  '<i class="fas  fa-2x fa-fw  fa-film"></i>',
    'pdf':  '<i class="fas  fa-2x fa-fw  fa-file-pdf"></i>',
    'doc':  '<i class="fas  fa-2x fa-fw  fa-file-word"></i>',
    'docx': '<i class="fas  fa-2x fa-fw  fa-file-word"></i>',
    'rar':  '<i class="fas  fa-2x fa-fw  fa-file-archive"></i>',
    '*':    '<i class="fas  fa-2x fa-fw  fa-file"></i>',
  };

  return `
    <div class="filedrop-file" data-id="${name}">
      <div class="filedrop-file-icon">${icons[icon] ?? icons['*']}</div>
      <div class="filedrop-file-name">
        <small>${name}</small>
        <div class="filedrop-tools d-flex w-100 align-items-center" style="gap: 5px">
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-label="Filename uploading percentage"
              style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0%</div>
          </div>
          <div class="filedrop-file-tools d-flex" style="gap: 5px">
            <button type="button" class="filedrop-remove btn btn-danger btn-xs disabled" disabled
              data-toggle="tippyjs" data-tippy-content="Remove" data-tippy-appendto="parent">
              <i class="fas fa-trash fa-sm fa-fw"></i>
            </button>
            <a href="" class="filedrop-download btn btn-info btn-xs disabled" disabled target="_blank"
              data-toggle="tippyjs" data-tippy-content="Download" data-tippy-appendto="parent">
              <i class="fas fa-eye fa-sm fa-fw"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  `;
}

let
  pathname  = location.pathname.split('/').splice(1),
  basePath  = location.origin + (
                (location.hostname === 'localhost' || location.hostname.split('.').length === 4)
                ? `/${([...pathname].slice(0, pathname.indexOf("cp")).join('/'))}` // project folder name in localhost
                : ''),
  cpPath    = `${basePath}/cp`;

tippy("[data-toggle='tippyjs']");
$("[data-bootstrap-switch]").bootstrapSwitch();
$('form[data-view="update"]').find("input[type='file'][required]").removeAttr('required');
['load', 'online', 'offline'].forEach((v, i) => {
  window.addEventListener(v, connectivityChecker);
});

$('.select2bs4').select2({
  theme: 'bootstrap4',
});

$('textarea:not([data-no-wysiwyg])').summernote({
  height: 250,
  dialogsInBody: true,
  toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['style', 'fontname', 'fontsize', 'fontsizeunit']],
    ['font-2', ['strikethrough', 'superscript', 'subscript', 'color']],
    ['para', ['ul', 'ol', 'paragraph', 'ltr','rtl']],
    ['insert', ['link','picture', 'video', 'table', 'hr', 'codeview']],
    // ['view', [/*'fullscreen'*//*, 'help'*/]],
  ],
});

$('body').on('click submit', '.action-prevented', function (e) {
  e.preventDefault();

  if (location.hostname === 'localhost') {
    localhostAction();
  }
  else {
    offlineAction();
  }
});

$('.image-input').on({
  change: function (e) {
    let
      $this = $(this),
      inputId = $this.data('input'),
      label = $('label').filter(`[data-label='${inputId}']`),
      btnRemove = $('.btn-remove-image-input').filter(`[data-remove-image-btn=${$this.attr('id')}]`),
      img = $(`img[data-image='${inputId}']`);

    btnRemove.prop('disabled', false);
    img.attr({
      'data-current-src': img.attr('src'),
    });
    readURL(this, img);
    label.attr({
      'data-old-text': label.text(),
    }).text("File chosen");
    // console.log(this, $this);
  },
});

$('.btn-remove-image-input').on({
  click: function (e) {
    // e.preventDefault();

    let
      $this = $(this),
      btnId = $this.data('remove-image-btn'),
      label = $('label').filter(`[data-label='${btnId}']`),
      img   = $(`img[data-image='${btnId}']`),
      input = $(`input[data-input='${btnId}']`);

    img.attr({
      src: img.attr('data-current-src'),
      'data-current-src': '',
    });
    $this.prop('disabled', true);
    input.val('');
    label.text(label.attr('data-old-text')).attr({'data-old-text': ''});
    // console.log(input);
  },
});

$('.multiple-images').on({
  click: function () {
    let
      $this       = $(this),
      table       = $this.data('table'),
      prefix      = $this.data('prefix'),
      file        = $this.data('file'),
      id_item     = $this.data('id-item'),
      column_name = $this.data('column-name'),
      folder      = $this.data('folder');

    $.ajax({
      url: `${cpPath}/bulk/remove`,
      type: 'POST',
      data: {
        table: table, prefix: prefix, file: file, id_item: id_item, column_name: column_name, folder: folder,
      },
      beforeSend: function () {
        $this.css({opacity: .4});
      },
      success: function (data) {
        if (data === 'ok') {
          $this.remove();
        }
      },
      error: function () { $this.css({opacity: 1}); },
      complete: function () {},
    });
  },
});

let el = $(), maxfilesize = 15;
$('.filedrop').filedrop({
  fallback_dropzoneClick: false,
  url: `${cpPath}/bulk/transmit`,
  data: {},
  allowedfiletypes: [], // Empty array means no restrictions.
  allowedfileextensions: ['.pdf', '.rar', '.zip', '.xlsx', '.ppt', '.doc', '.docx', '.mp3', '.mp4'], // Empty array means no restrictions.
  maxfiles: 15,
  maxfilesize: maxfilesize, // max file size in MBs
  paramname: function (e) {
    let
      prefix = el.data('prefix'),
      column = el.data('column'),
      table  = el.data('table'),
      id     = el.data('id');
    return `${prefix}_${column}|${table}|${prefix}|${column}|${id}`;
  },

  error: function (err, file) {
    switch(err) {
      case 'BrowserNotSupported':
        swal('error', 'browser does not support HTML5 drag and drop');
        break;
      case 'TooManyFiles':
        // user uploaded more than 'maxfiles'
        swal('error', 'Maximum queue length is 15 files only.');
        break;
      case 'FileTooLarge':
        // program encountered a file whose size is greater than 'maxfilesize'
        // FileTooLarge also has access to the file which was too large
        // use file.name to reference the filename of the culprit file
        swal('error', `${file.name} file si too large. Maximum file size is 15MB only.`);
        break;
      case 'FileTypeNotAllowed':
        // The file type is not in the specified list 'allowedfiletypes'
        swal('error', `File type is not allowed.`);
        break;
      case 'FileExtensionNotAllowed':
        // The file extension is not in the specified list 'allowedfileextensions'
        swal('error', `File extension is not allowed.`);
        break;
      default:
        swal('error', `An error occurred. Please contact the developer.`);
        break;
    };
  },

  dragOver: function () {
    // user dragging files over #dropzone
    $(this).addClass('hovering');
  },
  dragLeave: function () {
    // user dragging files out of #dropzone
    $(this).removeClass('hovering');
  },
  docOver: function () {
    // user dragging files anywhere inside the browser document window
  },
  docLeave: function () {
    // user dragging files out of the browser document window
  },
  drop: function (e) {
    // user drops file
    el = $(e.target);
    $(this).removeClass('hovering');
  },

  uploadStarted: function (i, file, len) {
    // a file began uploading
    // i = index => 0, 1, 2, 3, 4 etc
    // file is the actual file of the index
    // len = total files user dropped
    // console.log(i, file, len);
  },
  uploadFinished: function (i, file, response, time) {
    // response is the data you got back from server in JSON format.
    let url = response.url;
    let f = $(`.filedrop-file[data-id="${file.name}"]`);

    if (url !== '') {
      f.attr('data-id', response.file);
      f.find('.filedrop-file-tools').find('button, a')
        .removeAttr('disabled').removeClass('disabled')
        .filter('a').attr({href: url});
      f.find('.progress').remove();
    }
    else {
      f.addClass('rejected').find('.filedrop-file-icon').html(`<i class="fas fa-times fa-2x fa-fw"></i>`);
      f.delay(6000).slideUp(500, function () {
        f.remove();
      });
    }
  },

  progressUpdated: function (i, file, progress) {
    // this function is used for large files and updates intermittently
    // progress is the integer value of file being uploaded percentage to completion
    // console.log(i, file, progress);
    let f = $(`.filedrop-file[data-id="${file.name}"]`);
    f.find('.progress .progress-bar').attr({'aria-valuenow': progress}).css({width: `${progress}%`}).text(`${progress}%`);
  },
  globalProgressUpdated: function (progress) {
    // progress for all the files uploaded on the current instance (percentage)
    // ex: $('#progress div').width(progress+"%");
  },

  speedUpdated: function (i, file, speed) {
    // speed in kb/s
    // console.log(i, file, speed);
    el.parents('.card').find('.card-title small').text(`(${Math.round(speed)} kb/s)`);
  },
  beforeEach: function (file) {
    // file is a file object
    // return false to cancel upload
    if (maxfilesize * 1024 * 1024 >= file.size) {
      el.append(filedrop_file(file));
    }
  },
  beforeSend: function (file, i, done) {
    // file is a file object
    // i is the file index
    // call done() to start the upload
    done();
  },
  afterAll: function () {
    // runs after all files have been uploaded or otherwise dealt with
    el.parents('.card').find('.card-title small').text('');
  }
});

$('body').on('click', '.filedrop-remove', function () {
  let
    $this     = $(this),
    f         = $this.parents('.filedrop-file'),
    filedrop  = $this.parents('.filedrop'),
    file      = f.data('id'),
    table     = filedrop.attr('data-table'),
    column    = filedrop.attr('data-column'),
    id_item   = filedrop.data('id');

  $.ajax({
    url: `${cpPath}/bulk/remove`,
    type: 'POST',
    data: {
      table: table,
      prefix: filedrop.data('prefix'),
      column_name: column,
      file: file,
      id_item: id_item,
      folder: `${table}/${column}`,
    },
    beforeSend: function () {
      f.css({opacity: .4});
    },
    success: function (data) {
      if (data === 'ok') {
        f.slideUp(500, function () {
          f.remove();
        });
      }
    },
    error: function () { f.css({opacity: 1}); },
    complete: function () {},
  });
});


