// -------------------------------------------------------------------
// Helper function: Converts a base64-encoded string to an ArrayBuffer
// -------------------------------------------------------------------
// which is required for the WebAuthn API.
// Convert base64 strings to ArrayBuffer (required by WebAuthn)

function base64ToArrayBuffer(base64) {
  const binaryString = window.atob(base64); // Decode the base64 string to a binary string.
  const len = binaryString.length; // Get the length of the binary string.
  const bytes = new Uint8Array(len); // Create a typed array of bytes.

  for (let i = 0; i < len; i++) {
    bytes[i] = binaryString.charCodeAt(i); // Convert each character to its char code.
  }

  return bytes.buffer; // Return the underlying ArrayBuffer.
}

// ----------------------------------------------
// Helper Function: Convert ArrayBuffer to Base64
// ----------------------------------------------
// This function converts an ArrayBuffer (binary data) into a Base64 string.
// Why? Because many fields in the credential object (like rawId, clientDataJSON,
// and attestationObject) are ArrayBuffers, and we need to send them as strings.
function arrayBufferToBase64(buffer) {
  let binary = '';
  const bytes = new Uint8Array(buffer);

  for (let i = 0; i < bytes.byteLength; i++) {
    binary += String.fromCharCode(bytes[i]);
  }

  return window.btoa(binary);
}

