<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebModel|CpModel|ApiModel

class Blank Extends Model {

  public function __construct() {
    parent::__construct('table_name', 'column_prefix', 'storage_folder_name');
    $this->activeOnly = false;
    $this->i18n       = [];
    $this->neutral    = [];
  }

}