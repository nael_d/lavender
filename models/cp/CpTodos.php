<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpModel;

class CpTodos Extends \Model {

  public function __construct() {
    parent::__construct('cp_todos', 'cp_todo', '');
    $this->activeOnly = false;
    $this->i18n       = [];
    $this->neutral    = ['id_user', 'title', 'is_active'];
  }

}