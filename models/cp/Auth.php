<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebModel|CpModel|ApiModel
namespace CpModel;

class Auth Extends \Model {

  public function __construct() {
    parent::__construct('cp_users', 'cp_user');
    $this->i18n     = [];
    $this->neutral  = ['username', 'password', 'is_darkmode', 'is_active', 'gauth_secret', 'mobile_number', 'telegram_number'];
  }

  public function store(array $inputs = [], array $files = [], int|null $id = null) {
    if ($this->col == 'cp_user') {
      $inputs['cp_user_is_darkmode'] ??= 0; // when "creating" new user, no <select> value is passed.
  
      if ($inputs['cp_user_gauth_secret'] == 'generate_otp') {
        unset($inputs['cp_user_gauth_secret'], $this->neutral[array_search('gauth_secret', $this->neutral)]);
        // we removed the 'secret' key because it'll be generated later when logging in
      }
      else if ($inputs['cp_user_gauth_secret'] == 1) {
        unset($this->neutral[array_search('secret', $this->neutral)]); // passing 1 means keep the current gauth key
      }
      else if ($inputs['cp_user_gauth_secret'] == 0) {
        $inputs['cp_user_gauth_secret'] = null; // this means gauth has been reset once he logins again.
      }
    }

    return parent::store(inputs: $inputs, id: $id);
  }

  public function set_webauthn() {
    $this->activeOnly = false;
    $this->neutral = ['id_user', 'credential_id', 'public_key', 'device_name', 'sign_count'];
    return $this->set('cp_users_creds', 'cred', '');
  }

  public function webauthn_increment_count(int $count, int $id_user) {
    $this->set_webauthn();
    $this->neutral = ['sign_count'];
    return $this->store(['cred_sign_count' => $count], id: $id_user);
  }

}