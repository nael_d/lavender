<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpModel;

class Slides Extends \Model {

  public function __construct() {
    parent::__construct('slides', 'slide', 'slides');
    $this->activeOnly = false;
    $this->i18n       = ['title'];
    $this->neutral    = ['is_active'];
  }

}