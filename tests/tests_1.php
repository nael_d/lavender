<?php

function add($n1, $n2) {
  return $n1 + $n2;
}

\Testing\Testing::declare(
  'add', // spaces in the ID is not allowed
  'Summing two numbers',
  'add', // custom function
  [1, 2],
  3,
);

\Testing\Testing::declare(
  'subtract',
  'Subtraction test',
  function($a, $b) { // anonymous function
    return $a - $b;
  },
  [5, 3],
  2,
);

\Testing\Testing::declare(
  'multiply',
  'Multiplication test',
  fn($a, $b) => $a * $b, // shorthand function
  [2, 3],
  6,
);

$divide = function ($a, $b) {
  return $a / $b;
};

\Testing\Testing::declare(
  'divide',
  'Division test',
  $divide, // callable closure function
  [10, 2],
  5,
);

\Testing\Testing::declare(
  'string_length',
  'String length test',
  'strlen', // built-in function
  ['Hello, World!'],
  13,
);

\Testing\Testing::declare(
  'array_sum',
  'Array sum test',
  'array_sum', // built-in function
  [[1, 2, 3, 4, 5]],
  15,
);

$stdClass = new \stdClass();
$stdClass->name = 'John';
$stdClass->age = 30;
$stdClass->city = 'New York';

\Testing\Testing::declare(
  'json_decode',
  'JSON decode test',
  'json_decode', // built-in function
  ['{"name":"John","age":30,"city":"New York"}'],
  $stdClass,
);

\Testing\Testing::declare(
  'data_manipulation',
  'Data manipulation test',
  function ($data) { // anonymous function
    $data['name'] = 'Jane';
    unset($data['age']);
    return $data;
  },
  [['name' => 'John', 'age' => 30, 'city' => 'New York']],
  ['name' => 'Jane', 'city' => 'New York']
);

\Testing\Testing::declare(
  'sort_array',
  'Testing array sorting',
  function ($arr) { // anonymous function
    sort($arr);
    return $arr;
  },
  [[5, 3, 1, 4, 2]],
  [1, 2, 3, 4, 5]
);

\Testing\Testing::declare(
  'average',
  'Average calculation test',
  function ($numbers) { // anonymous function
    if (count($numbers) === 0) {
      return null; // Return null for an empty array
    }
    return array_sum($numbers) / count($numbers);
  },
  [[1, 2, 3, 4, 5]],
  3, // Expected average of [1, 2, 3, 4, 5] is 3
);

\Testing\Testing::declare(
  'reverse_string',
  'String reversal test',
  fn($str) => strrev($str), // shorthand function
  ['hello'],
  'olleh', // Expected reversed string
);

