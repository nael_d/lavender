<?php

namespace ApiController;

class Home extends Api {
  public function __construct() {
    parent::__construct();
  }

  public function get() {
    echo $this->make(["key" => "value"]);
  }
}
