<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebController|CpController|ApiController

class Blank Extends ControllerExtended {

  private $model        = '';
  private $title        = "";
  private $title_single = "";
  private $folder       = "";
  private $redirectTo   = '';
  private $title_column = '';

  public function __construct() {
    parent::__construct();
  }

}