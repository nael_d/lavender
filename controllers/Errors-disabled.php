<?php

/**
 * If you want to setup your own errors, rename this file to `Errors.php`.
 * To disable it, just rename it to `Errors-disabled.php`.
 * 
 * Feel free to create your own error views for each platform using `$this->view()`.
 * For a better structuring views, use `views/errors` folder and create a file for each platform.
 * For example: create `views/errors/website.php` file and call it as `$this->view("errors/website");`.
 * 
 * This is how to take the errors views away from the main website layout.
 * However, if you still want to use the main website layout for your errors views, do the following:
 * 
 * - Create `errors` folder inside each platform folder that you want.
 * - Create a file inside it named as platform folder.
 *   For example: you'll have: [`views/web/errors/website.php`, `views/cp/errors/cp.php`, `views/api/errors/api.php`].
 * - To use `website()` method, you have to set `$this->views_root_path = 'web';` before your code.
 * - To use `api()` method, you have to set `$this->views_root_path = 'api';` before your code.
 * - To use `cp()` method, you have to set `$this->views_root_path = 'cp';` before your code.
 * - After all, you could use `$this->layout();` as well.
 */

namespace Web;

class Errors Extends \ControllerExtended {

  public function __construct() {
    parent::__construct();
  }

  public function __destruct() {
    die();
  }

  public function website($code, $details, $force_error = false) {
    # code...
  }

  public function api($code, $details) {
    # code...
  }

  /*public function cp($code, $details, $force_error = false) {
    # code...
  }*/
  
}