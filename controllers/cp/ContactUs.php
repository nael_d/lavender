<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpController;

class ContactUs Extends Cp {

  private $model;
  private $redirectTo   = 'cp/contact-us';
  private $title        = "Contact us";

  public function __construct() {
    parent::__construct();
    $this->load_model('CpModel\Settings');
    $this->model = new \CpModel\Settings;
    $this->model->folder = 'contact-us';
  }

  public function index() {
    self::View::render('contact-us/view', [
      'title' => $this->title,
      'item'  => $this->model->preference_get('contact-us'),
    ]);
  }

  public function store() {
    $this->model->store(inputs: $_POST, files: $_FILES, id: 'contact-us');
    redirect($this->redirectTo);
  }

}