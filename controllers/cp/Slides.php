<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpController;

class Slides Extends Cp {

  private $model;
  private $title        = "Slides";
  private $title_single = "slide";
  private $folder       = "slides";
  private $redirectTo   = 'cp/slides';
  private $title_column = 'slide_title';

  public function __construct() {
    parent::__construct();

    $this->load_model("CpModel\Slides");
    $this->model = new \CpModel\Slides;
  }

  public function get(int|null $id = null) {
    if ($id > 0) {
      $item = $this->model->get($id);
      if (count($item) == 0) redirect($this->redirectTo);
      $item = current($item);

      self::View::render("{$this->folder}/view", [
        'title' => $item[$this->title_column]->{$this->default_lang_cp},
        'item'  => $item,
      ]);
    }
    else {
      self::View::render("{$this->folder}/index", [
        'title' => $this->title,
        'items' => $this->model->get(),
      ]);
    }
  }

  public function create() {
    self::View::render("{$this->folder}/view", [
      'title'   => "Create {$this->title_single}",
    ]);
  }

  public function store(int|null $id = null) {
    $this->model->store(inputs: $_POST, files: $_FILES, id: $id);
    redirect("{$this->redirectTo}/{$id}");
  }

}