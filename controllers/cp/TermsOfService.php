<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpController;

class TermsOfService Extends Cp {

  private $model;
  private $redirectTo   = 'cp/terms-of-service';
  private $title        = "Terms of service";

  public function __construct() {
    parent::__construct();
    $this->load_model('CpModel\Settings');
    $this->model = new \CpModel\Settings;
  }

  public function index() {
    self::View::render('terms-of-service/view', [
      'title' => $this->title,
      'item'  => $this->model->preference_get('terms-of-service'),
    ]);
  }

  public function store() {
    $this->model->store(inputs: $_POST, id: 'terms-of-service');
    redirect($this->redirectTo);
  }

}