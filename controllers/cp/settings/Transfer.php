<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebController|CpController|ApiController
namespace CpController;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

class Transfer Extends CP {

  private $model                    = '';
  private $redirectTo               = 'cp/settings/transfer';
  private $title                    = "Transfer";
  private $title_single             = "";
  private $title_column             = '';
  private $tables_blueprint         = [];
  private $tables_blueprint_parsed  = [];

  public function __construct() {
    parent::__construct();

    foreach ($this->db_tables as $key => $table) {
      $this->tables_blueprint[$table] = \R::getColumns($table);
    }
  }

  public function index() {
    self::View::render('settings/transfer/index', [
      'title' => $this->title,
    ]);
  }

  private function columns(string $table) {
    $this->load_model(["CpModel\\$table"]);
    $this->model  = new("CpModel\\$table");
    $get          = $this->model->get(null, ['limit' => 1], []);
    $i18n         = $this->model->i18n;
    $neutral      = $this->model->neutral;
    $neutral_obj  = [];

    if (count($get) > 0) {
      // in this way, i want to check that if any neutral column contains an key-value object to parse it
      $get = current($get);
      foreach ($neutral as $k => $n) {
        if (is_object($get["{$this->model->col}_{$n}"])) {
          foreach ($get["{$this->model->col}_{$n}"] as $n_key => $n_val) {
            $neutral_obj[$n][] = "{$this->model->col}_{$n}-{$n_key}";
          }
        }
      }
    }
    else {
      // otherwise, let's generate the file in the default way.
      // just do nothing here ...
    }

    foreach (array_keys($this->tables_blueprint[$table]) as $i => $column) {
      if (explode('_', $column)[0] == 'id') { $this->tables_blueprint_parsed[] = $column; }
      else {
        $column_unsuffixed = str_replace("{$this->model->col}_", '', $column);
        if (in_array($column_unsuffixed, $i18n)) {
          foreach ($this->langs as $language => $lang) {
            $this->tables_blueprint_parsed[] = "{$column}_{$lang}";
          }
        }
        else if (array_key_exists($column_unsuffixed, $neutral_obj)) {
          // a neutral key-value column
          foreach ($neutral_obj[$column_unsuffixed] as $nk => $nv) {
            $this->tables_blueprint_parsed[] = "{$nv}";
          }
        }
        else {
          $this->tables_blueprint_parsed[] = $column;
        }
      }
    }

    return $this->tables_blueprint_parsed;
  }

  private function excel_writer(array $list, string $table) {
    $excel_file = ".cache/{$table}_" . rand_str() . ".xlsx";
    $writer = WriterEntityFactory::createXLSXWriter();
    $writer->openToFile($excel_file);
    $writer->addRow(WriterEntityFactory::createRowFromArray($this->columns($table)));
  
    if (count($list) > 0) {
      foreach ($list as $key => $item) {
        $writer->addRow(WriterEntityFactory::createRowFromArray($item));
      }
    }
  
    $writer->close();
    return $excel_file; // the path to temporarily generated excel file
  }

  public function export() {
    if (__REQUEST__ == 'get') {
      self::View::render('settings/transfer/export', [
        'title'   => "Export data to Excel file",
        'tables'  => $this->db_tables,
      ]);
    }
    else {
      $table      = sanitizer($_POST['table']);
      $ids_list   = sanitizer($_POST['ids'] ?? []);
      $data_dist  = [];

      if (!in_array($table, $this->db_tables)) {
        die("Error");
      }

      $this->load_model(["CpModel\\$table"]);
      $this->model  = new("CpModel\\$table");
      $data         = $this->model->get(id: $ids_list);

      foreach ($data as $key => $item) {
        foreach ($item as $k => $v) {
          if (is_numeric($v) or is_string($v)) {
            $data_dist[$key][] = $v;
          }
          else if (is_object($v)) {
            $v = (array) $v;
            if (is_numeric(array_keys($v)[0])) {
              // this is an array of listed items
              $data_dist[$key][] = json_encode($v, JSON_UNESCAPED_UNICODE);
            }
            else if (array_intersect(array_keys($v), $this->langs)) {
              // this column is multi languages array
              foreach ($this->langs as $language => $lang) {
                $data_dist[$key][] = $v[$lang];
              }
            }
            else {
              // this is a object in neutral column
              // let's check if it's an array or object
              foreach (array_keys($v) as $k2 => $v2) {
                if (!is_numeric($v2)) {
                  // this is a neutral object
                  $data_dist[$key][] = $v[$v2];
                }
              }
            }
          }
        }
      }

      echo $this->excel_writer($data_dist, $table);
    }
  }

  public function generate() {
    $table = sanitizer($_POST['table']);
    echo $this->excel_writer([], $table);
  }

  public function import() {
    if (__REQUEST__ == 'get') {
      self::View::render('settings/transfer/import', [
        'title'   => "Import from Excel",
        'tables'  => $this->db_tables,
      ]);

      session_remove('temp', ['i', 'errors']);
    }
    else {
      /**
       * $i used to count the processed items in the `import` action.
       * Any {update|delete} items that doesn't have `id` column will be ignored.
       */
      $i                  = 0;
      $table              = sanitizer($_POST['table']);
      $action             = sanitizer($_POST['action']);
      $file               = $_FILES['excel_file'];
      $isEmpty            = true;
      $header             = null;
      $datalist           = [];
      $errors             = [];
      $i18n               = [];
      $neutral            = [];
      $suffix             = null;
      $datalist_compined  = [];

      if (count($_POST) == 0) { $errors[] = "No data passed."; }
      if (!sanitizer($table)) { $errors[] = "No table passed."; }
      if (!sanitizer($action)) { $errors[] = "No action passed."; }

      if (count($_FILES) == 0 or !$_FILES['excel_file'] or !$file['name']) {
        $errors[] = "No file uploaded.";
      }
      else if ($file['size'] == 0 or $file['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
        $errors[] = "Invalid .xlsx file or it's corrupted.";
      }

      // file looks good for now. keep going ...
      try {
        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($file['tmp_name']);

        foreach ($reader->getSheetIterator() as $key => $sheet) {
          foreach ($sheet->getRowIterator() as $key2 => $row) {
            $isEmpty = false;
            if (!$header) { $header = $row->toArray(); continue; }
            $datalist[] = $row->toArray();
          }
        }
      }
      catch (\Throwable $th) {
        $pattern = '/([a-zA-Z]:\\\\[^\s]+\\\\[^\s]+\.[a-zA-Z0-9]{1,4})/';

        if (preg_match($pattern, $th->getMessage(), $matches)) {
          $err = preg_replace($pattern, "'{$file['name']}'", $th->getMessage());
          $err = preg_replace("/ \(.+\)/", '', $err);

          $errors[] = $err;
        }
      }

      if ($isEmpty) { $errors[] = "Empty .xlsx file."; }

      $column_checking = [];
      foreach ($header as $key => $head) {
        $head_exploded = explode('_', $head);
        if ($head_exploded[0] == 'id') {
          // this is the first column
          array_shift($head_exploded);
          $column_checking[] = implode('_', $head_exploded);
        }
        else {
          /**
           * the suffix must be a singular phrase derivated from the table name.
           * like: `section_categories` table :: `section_category` column.
           * so in this case, we have to subtract the column full name.
           */
          $table_exploded = explode('_', $table);
          $cs = implode('_', array_slice($head_exploded, 0, count($table_exploded)));
          if (!in_array($cs, $column_checking)) {
            $column_checking[] = implode('_', array_slice($head_exploded, 0, count($table_exploded)));
          }
        }
      }

      // dpre($column_checking);

      if (count($column_checking) > 1 or !preg_match("/{$column_checking[0]}/i", $table)) {
        // error: it must only contain prefixed columns derivated from the name of the table
        $errors[] = "Invalid .xlsx file: mismatching table columns criteria, columns don't match with the table.";
      }
      else {
        $column_checking = $column_checking[0];
      }

      foreach ($header as $key => $head) {
        $head_exploded = explode('_', $head);

        if ($head_exploded[0] == 'id') {
          array_shift($head_exploded);
          $suffix = implode('_', $head_exploded);
          continue;
        }

        $column = str_replace("{$suffix}_", '', $head);

        if (
          count($head_exploded) > 1 and
          in_array(end($head_exploded), $this->langs)
        ) {
          // this column is multi languages
          $column = explode('_', $column);
          array_pop($column);
          $column = implode('_', $column);
          if (!in_array($column, $i18n)) {
            $i18n[] = $column;
          }
        }
        else {
          // this column is neutral
          if (str_contains($column, '-')) {
            $column = explode('-', $column)[0];
            if (!in_array($column, $neutral)) {
              $neutral[] = $column;
            }
          }
          else {
            $neutral[] = $column;
          }
        }
      }

      // checking that `id_{$suffix}` column exists at position [0] explicitly, otherwise throw error
      if ($this->columns($table)[0] != $header[0]) {
        $errors[] = "Invalid .xlsx file: primary-key column name doesn't match with the column in the table.";
      }

      if (count($errors) == 0) {
        // all good. keep going ...
        $this->load_model("CpModel\\$table");
        $this->model = new("CpModel\\$table");
        // pre($i18n);
        // pre($neutral);
        // pre($datalist);
        // pre($header);

        // Check if $i18n contains at least one or all values from $this->model->i18n, but no other values.
        if (count(array_diff($i18n, $this->model->i18n)) > 0) {
          // the i18n columns in the excel file doesn't match with i18n defined in the model.
          $errors[] = "Invalid .xlsx file: mismatching i18n columns within the excel file and the table.";
        }

        // Check if $neutral contains at least one or all values from $this->model->neutral, but no other values.
        if (count(array_diff($neutral, $this->model->neutral)) > 0) {
          // the neutral columns in the excel file doesn't match with neutral defined in the model.
          $errors[] = "Invalid .xlsx file: mismatching neutral columns within the excel file and the table.";
        }

        /**
         * if 422 page showing: "(i18n|neutral) key `column` is not found in `$inputs[]`, in` CpModel\XXXXXXX` model.",
         * it means that the key `column` is not found in the excel file and it's required to be exist.
         */

        $datalist_compined = array_map(fn($dl) => array_combine($header, $dl), $datalist);
        $ids = [];

        foreach ($datalist_compined as $key => $item) {
          $id = (int) $item["id_{$suffix}"];
          unset($item["id_{$suffix}"]); // we got the id, remove it from the $item
          // pre($item);

          if (in_array($action, ['update'])) {
            if ($id > 0) {
              $i++;
              $this->model->store(inputs: $item, id: $id);
            }
            else {
              $key++;
              $errors[] = "Error while importing: the line (#{$key}) doesn't have a valid value for `id_{$suffix}` column.";
            }
          }
          else if (in_array($action, ['delete'])) {
            if ($id > 0) { $i++; $ids[] = $id; }
            else {
              $errors[] = "Error while importing: the line (#{$key}) doesn't have a valid value for `id_{$suffix}` column.";
            }
          }
          else {
            // insert. Ignore the id.
            $i++;
            $this->model->store(inputs: $item);
          }
        }

        if ($action == 'delete' and count($ids) > 0) { $this->model->delete($ids); }
      }

      session('temp', ['i' => $i, 'errors' => $errors]);
      redirect("{$this->redirectTo}/import");
    }
  }

}