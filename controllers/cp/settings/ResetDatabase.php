<?php

namespace CpController;

class ResetDatabase extends Cp {

  private $attempts = 5;
  private $times;

  public function __construct() {
    parent::__construct();

    $this->times = (session('cp', 'reset-database-times') === null)
      ? session('cp', 'reset-database-times', $this->attempts)
      : session('cp', 'reset-database-times');
  }

  public function index() {
    self::View::render('settings/reset-database/index', [
      'title' => 'Reset database',
      'times' => $this->times,
    ]);
  }

  public function cancel() {
    session('cp', 'reset-database-times', $this->attempts);
    redirect('cp/settings/reset-database');
  }

  public function hit() {
    if ($this->times > 0) {
      $this->times--;
      session('cp', 'reset-database-times', $this->times);

      if ($this->times > 0) {
        redirect('cp/settings/reset-database');
      }
      else {
        // Horror has been started 💀 ...
        $tables     = $this->db_type == 'sqlite3' ? [...\R::inspect(), 'sqlite_sequence'] : \R::inspect();
        $schemas    = [...glob('schemas/*.php'), ...glob('schemas/*/*')];
        $superuser  = [
          'username' => $this->use_encrypia == 'true' ? \Encrypia::blind('admin') : 'admin',
          'password' => $this->use_encrypia == 'true' ? \Encrypia::blind('pass') : 'pass'
        ];

        // wipe database
        array_map(fn($table) => \R::wipe(strtoupper($table)), $tables);

        // rebuild database
        foreach ($schemas as $schema) {
          $schema_name = pathinfo($schema, PATHINFO_FILENAME);
          (new("Schema\\{$schema_name}"))->install();
          if ($schema_name === "CpUsers") {
            (new \Schema\CpUsers())->write($superuser['username'], $superuser['password']);
          }
        }

        // reset attempts
        session('cp', 'reset-database-times', $this->attempts);

        // logout
        redirect('cp/logout');
      }
    }
  }

}
