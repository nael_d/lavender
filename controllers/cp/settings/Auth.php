<?php

namespace CpController;

class Auth extends CP {

  protected $redirectTo = '/cp/settings/users';
  private $auth;
  private $gauth;
  private $login_max_attempts;
  private $login_lockout_duration;
  public $inputs;

  public function __construct() {
    parent::__construct();

    /**
     * Session keys:
     * - login_attempts: int (default 0)  - current login attempts counter.
     * - id_cp_user: int                  - current logged-in user ID; redirects to /cp if set.
     * - login_id: int                    - current incomplete login user ID.
     * - login_username: string           - current logged-in username.
     * - login_status: string             - reports current login status.
     * - login_gauth_secret: string       - current logged-in user's Google Auth secret; used for OTP.
     * - login_error: string              - reports current login error.
     */

    $this->auth                   = new \CpModel\Auth;
    $this->login_max_attempts     = env('cp_login_max_attempts');
    $this->login_lockout_duration = env('cp_login_lockout_duration');
    $this->inputs                 = sanitizer($_POST);
    $this->gauth                  = (bool) (new \CpModel\Settings)->preference_get('gauth')['setting_value'];

    // init login_attempts session key with zero if null for being incrumented.
    session('cp', 'login_attempts', session('cp', 'login_attempts') ?? 0);
  }

  // Cp Login

  public function logout() {
    session_remove('cp', [
      'login_attempts',
      'id_cp_user',
      'login_id',
      'login_username',
      'login_status',
      'login_gauth_secret',
      'login_error',
    ]);

    redirect('cp/login');
  }

	public function get_gauth() {
		return $this->gauth;
	}

  public function login() {
    if (__REQUEST__ == 'get') {
      if (session('cp', 'id_cp_user') == null) {
        // not logged in yet

        $loginStatus  = session('cp', 'login_status');
        $loginTime    = session('cp', 'login_time');

        if ($loginStatus == 'too-many-attempts-error' && $loginTime + $this->login_lockout_duration >= time()) {
          // Locking login form out for the `$this->login_lockout_duration` seconds.
          self::View::render('auth/login-freezed');
        }
        else {
          $keysToRemove = $loginStatus === 'too-many-attempts-error'
            ? ['login_attempts', 'login_time']
            : ['login_page', 'login_id', 'login_username', 'login_gauth_secret'];

          session_remove('cp', ['login_status', ...$keysToRemove]);
          self::View::render('auth/login');
        }
      }
      else {
        // logged in and opened login page? why? kick him away!
        redirect('cp');
      }
    }
    else {
      $attempts = session('cp', 'login_attempts');

      if (\Csrf\Csrf::validate($this->inputs['csrf_token'] ?? '')) {
        $login = $this->auth->get(
          where: [
            'cp_user_username' => $this->inputs['cp_user_username'],
          ],
        );

        if (count($login) > 0) {
          $login = current($login);

          session('cp', [
            'login_id'            => $login['id_cp_user'],
            'login_status'        => 'more-auth-required',
            'login_username'      => $login['cp_user_username'],
            'login_gauth_secret'  => $this->gauth ? ($login['cp_user_gauth_secret'] == '' ? '' : $login['cp_user_gauth_secret']) : '',
          ]);
        }
        else {
          // failed logging-in attempt due to mismatched username.
          $attempts++;

          if ($attempts < $this->login_max_attempts) {
            // in allowed attempts credit.
            session('cp', 'login_status', 'auth-error');
            session('cp', 'login_error', 'Failed to login due to mismatched username.');
          }
          else {
            // too many attempts, lock the login form for the next `$this->login_lockout_duration` seconds.
            session('cp', 'login_status', 'too-many-attempts-error');
            session('cp', 'login_time', time());
          }

          session_remove('cp', ['login_id', 'login_username', 'login_gauth_secret']);
        }
      }
      else {
        // failed logging-in attempt: due to failed csrf validation.
        $attempts++;
        session('cp', 'login_status', 'csrf-error');
        session('cp', 'login_error', 'Request is not authorized.');
        session_remove('cp', ['login_id', 'login_username', 'login_gauth_secret']);
      }

      if ($attempts == session('cp', 'login_attempts')) {
        // logging-in attempt is successful, no failed attempts are counted.
        // let's redirect to the next step of the login process.
        redirect('cp/login/use');
      }
      else {
        // increment login attempts counter. let's redirect to cp/login to output errors.
        session('cp', 'login_attempts', $attempts);
        redirect('cp/login');
      }
    }
  }

  public function use() {
    if (__REQUEST__ == 'get') {
      if (session('cp', 'id_cp_user')) {
        redirect('cp');
      }
      else if (!session('cp', 'login_id')) {
        redirect('cp/logout');
      }
      else {
        $loginStatus  = session('cp', 'login_status');
        $loginCases   = [
          'too-many-attempts-error',
          'csrf-error',
          'otp-required',
          'more-auth-required',
          'password-required',
        ];

        if (in_array($loginStatus, $loginCases)) {
          // regenerate session id to prevent session fixation attacks.
          session_regenerate_id(true);

          // if one of these values are set, allow the user to go back to the /logn/use page.
          if (count($this->current_path_array) == 3) {
            session('cp', 'login_status', 'more-auth-required');
            session_remove('cp', ['login_attempts', 'login_time', 'login_error']);
            self::View::render('auth/login-options');
          }
          else if ($this->current_path_array[3] == 'password') {
            if ($loginStatus == 'too-many-attempts-error') {
              if (session('cp', 'login_time') + $this->login_lockout_duration >= time()) {
                // Locking login form out for the `$this->login_lockout_duration` seconds.
                self::View::render('auth/login-freezed');
              }
              else {
                session_remove('cp', ['login_attempts', 'login_time']);
                session('cp', 'login_status', 'password-required');
                self::View::render('auth/password-required');
              }
            }
            else {
              self::View::render('auth/password-required');
              session_remove('cp', 'login_error');
            }
          }
          else {
            redirect('cp/logout');
          }
        }
        else {
          redirect('cp/logout');
        }
      }
    }
    else {
      // regenerate session id to prevent session fixation attacks.
      session_regenerate_id(true);

      // reset all previous login errors.
      session_remove('cp', 'login_error');

      if (isset($this->inputs['option'])) {
        // This is POST from the login-options form.
        if ($this->inputs['option'] == 'gauth') {
          // Google Authenticator login option selected.
          session('cp', 'login_status', session('cp', 'login_gauth_secret') == '' ? 'otp-not-set' : 'otp-required');
          redirect('cp/login/use/otp');
        }
        else if ($this->inputs['option'] == 'password') {
          // password login option selected.
          session('cp', 'login_status', 'password-required');
          redirect('cp/login/use/password');
        }
        else {
          // invalid login option selected.
          // redirect to the login page.
          redirect('cp/logout');
        }
      }
      else if (isset($this->inputs['cp_user_password'])) {
        // This is POST from the password-required form.

        $attempts = session('cp', 'login_attempts');

        if (\Csrf\Csrf::validate($this->inputs['csrf_token'] ?? '')) {
          // Valid CSRF token.
          $login = $this->auth->get(
            where: [
              'cp_user_username' => session('cp', 'login_username'),
              'cp_user_password' => $this->inputs['cp_user_password'],
            ],
          );

          if (count($login) > 0) {
            // password is correct.
            session('cp', 'id_cp_user', session('cp', 'login_id'));
            session_remove('cp', ['login_id', 'login_username', 'login_gauth_secret']);
            redirect('cp');
          }
          else {
            // failed logging-in attempt due to mismatched password.
            $attempts++;

            if ($attempts < $this->login_max_attempts) {
              // in allowed attempts credit.
              session('cp', 'login_error', 'Failed to login due to mismatched password.');
            }
            else {
              // too many attempts, lock the login form out for the next `$this->login_lockout_duration` seconds.
              session('cp', 'login_status', 'too-many-attempts-error');
              session('cp', 'login_time', time());
            }

            session('cp', 'login_attempts', $attempts);
            redirect('cp/login/use/password');
          }
        }
        else {
          // failed logging-in attempt due to failed csrf validation.
          $attempts++;
          session('cp', 'login_error', 'Request is not authorized.');
          redirect('cp/login/use/password');
        }
      }
    }
  }

  public function otp() {
    if (session('cp', 'id_cp_user')) {
      // logged in already.
      redirect('cp');
    }
    else if (!session('cp', 'login_id')) {
      // login process is not started yet.
      redirect('cp/logout');
    }
    else {
      if (!$this->gauth) {
        // Google Authenticator is not enabled.
        redirect('cp/login/use');
      }
      else {
        $loginStatus = session('cp', 'login_status');

        if (__REQUEST__ == 'get') {
          // regenerate session id to prevent session fixation attacks.
          session_regenerate_id(true);

          if ($loginStatus == 'too-many-attempts-error') {
            if (session('cp', 'login_time') + $this->login_lockout_duration >= time()) {
              // Locking login form out for the `$this->login_lockout_duration` seconds.
              self::View::render('auth/login-freezed');
            }
            else {
              session_remove('cp', ['login_attempts', 'login_time']);
              session('cp', 'login_status', 'otp-required');
              self::View::render('auth/otp-required');
            }
          }
          else if ($loginStatus == 'otp-not-set') {
            $secret = random_bytes(20);
            session('cp', 'login_gauth_secret', (new \Selective\Base32\Base32)->encode($secret));
            $this->auth->neutral = ['gauth_secret']; // to tell Ornata to just use 'secret' column to store.
            $this->auth->store(
              inputs: ['cp_user_gauth_secret' => session('cp', 'login_gauth_secret')],
              id: session('cp', 'login_id')
            );

            $data = [
              'img' => \Sonata\GoogleAuthenticator\GoogleQrUrl::generate(
                session('cp', 'login_username'),
                session('cp', 'login_gauth_secret'),
                $this->base_title
              ),
              'secret' => (new \Sonata\GoogleAuthenticator\GoogleAuthenticator)->getCode(session('cp', 'login_gauth_secret')),
            ];

            self::View::render('auth/otp-not-set', $data);
            session('cp', 'login_status', 'otp-required');
          }
          else if ($loginStatus == 'otp-required') {
            // pre("The code now is => " . (new \Sonata\GoogleAuthenticator\GoogleAuthenticator)->getCode(session('cp', 'login_gauth_secret')));
            self::View::render('auth/otp-required');
            session_remove('cp', 'login_error');
          }
          else {
            redirect('cp/logout');
          }
        }
        else {
          if ($loginStatus == 'otp-required') {
            // regenerate session id to prevent session fixation attacks.
            session_regenerate_id(true);
            $attempts = session('cp', 'login_attempts');

            if (\Csrf\Csrf::validate($this->inputs['csrf_token'] ?? '')) {
              // Valid CSRF token.
              $gauth  = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
              $otp    = $this->inputs['otp_code'];

              if ($gauth->checkCode(session('cp', 'login_gauth_secret'), $otp)) {
                session('cp', 'id_cp_user', session('cp', 'login_id'));
                session_remove('cp', ['login_gauth_secret', 'login_page', 'login_username', 'login_attempts', 'login_time']);
                redirect('cp');
              }
              else {
                // failed logging-in attempt due to mismatched OTP code.
                $attempts++;

                if ($attempts < $this->login_max_attempts) {
                  // in allowed attempts credit.
                  session('cp', 'login_error', 'Incorrect OTP code.');
                }
                else {
                  // too many attempts, lock the login form out for the next `$this->login_lockout_duration` seconds.
                  session('cp', 'login_status', 'too-many-attempts-error');
                  session('cp', 'login_time', time());
                }

                session('cp', 'login_attempts', $attempts);
                redirect('cp/login/use/otp');
              }
            }
            else {
              // failed logging-in attempt: due to failed csrf validation.
              $attempts++;
              session('cp', 'login_error', 'Request is not authorized.');
              redirect('cp/login/use/otp');
            }
          }
          else {
            redirect('cp/logout');
          }
        }
      }
    }
  }

  // Cp Users

  public function users_get(int|null $id = null) {
    $this->auth->activeOnly = false;
    $users = $this->auth->get($id);

    if (!$id) {
      self::View::render('settings/users/index', [
        'title' => 'CP Accounts',
        'items' => $users,
      ]);
    }
    else {
      if ($id > 0 and count($users) > 0) {
        self::View::render('settings/users/view', [
          'title' => 'CP Accounts',
          'item'  => $users[0],
        ]);
      }
      else {
        redirect($this->redirectTo);
      }
    }
  }

  public function users_create() {
    self::View::render('settings/users/view', [
      'title' => 'Create CP account',
    ]);
  }

  public function users_store(int|null $id = null) {
    $this->auth->store(inputs: $this->inputs, id: $id);
    redirect("{$this->redirectTo}/{$id}");
  }

}
