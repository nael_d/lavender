<?php

namespace CpController;

class Auth extends CP {

  protected $redirectTo = '/cp/settings/users';
  private $auth;
  private $gauth;
  private $login_max_attempts;
  private $login_lockout_duration;
  public $inputs;
  private $rp;

  public function __construct() {
    parent::__construct();

    /**
     * Session keys:
     * - login_attempts: int (default 0)  - current login attempts counter.
     * - id_cp_user: int                  - current logged-in user ID; redirects to /cp if set.
     * - login_id: int                    - current incomplete login user ID.
     * - login_username: string           - current logged-in username.
     * - login_status: string             - reports current login status.
     * - login_gauth_secret: string       - current logged-in user's Google Auth secret; used for OTP.
     * - login_error: string              - reports current login error.
     */

    $this->auth                   = new \CpModel\Auth;
    $this->login_max_attempts     = env('cp_login_max_attempts');
    $this->login_lockout_duration = env('cp_login_lockout_duration');
    $this->inputs                 = sanitizer($_POST);
    $this->gauth                  = (bool) (new \CpModel\Settings)->preference_get('gauth')['setting_value'];

    $this->rp = [
      'name'  => "{$this->base_title} CP",                  // Human-friendly name of your site.
      'id'    => parse_url($this->base_path)['host'],  // Domain name (important for matching credentials).
    ];

    // init login_attempts session key with zero if null for being incrumented.
    session('cp', 'login_attempts', session('cp', 'login_attempts') ?? 0);
  }

  private function base64UrlDecode($data) {
    // Replace URL-safe characters with standard Base64 characters.
    $data = strtr($data, '-_', '+/');

    // Pad with '=' characters if needed.
    $padding = 4 - (strlen($data) % 4);

    if ($padding !== 4) {
      $data .= str_repeat('=', $padding);
    }

    return base64_decode($data);
  }

  // Cp Login

  public function logout() {
    session_remove('cp', [
      'login_attempts',
      'id_cp_user',
      'login_id',
      'login_username',
      'login_status',
      'login_gauth_secret',
      'login_error',
    ]);

    redirect('cp/login');
  }

	public function get_gauth() {
		return $this->gauth;
	}

  public function login() {
    if (__REQUEST__ == 'get') {
      if (session('cp', 'id_cp_user') == null) {
        // not logged in yet

        $loginStatus  = session('cp', 'login_status');
        $loginTime    = session('cp', 'login_time');

        if ($loginStatus == 'too-many-attempts-error' && $loginTime + $this->login_lockout_duration >= time()) {
          // Locking login form out for the `$this->login_lockout_duration` seconds.
          self::View::render('auth/login-freezed');
        }
        else {
          $keysToRemove = $loginStatus === 'too-many-attempts-error'
            ? ['login_attempts', 'login_time']
            : ['login_page', 'login_id', 'login_username', 'login_gauth_secret'];

          session_remove('cp', ['login_status', ...$keysToRemove]);
          self::View::render('auth/login');
          session_remove('cp', 'login_error'); // remove errors after rendering the login page.
        }
      }
      else {
        // logged in and opened login page? why? kick him away!
        redirect('cp');
      }
    }
    else {
      $attempts = session('cp', 'login_attempts');

      if (\Csrf\Csrf::validate($this->inputs['csrf_token'] ?? '')) {
        $login = $this->auth->get(
          where: [
            'cp_user_username' => $this->inputs['cp_user_username'],
          ],
        );

        if (count($login) > 0) {
          $login = current($login);

          session('cp', [
            'login_id'            => $login['id_cp_user'],
            'login_status'        => 'more-auth-required',
            'login_username'      => $login['cp_user_username'],
            'login_gauth_secret'  => $this->gauth ? ($login['cp_user_gauth_secret'] == '' ? '' : $login['cp_user_gauth_secret']) : '',
          ]);
        }
        else {
          // failed logging-in attempt due to mismatched username.
          $attempts++;

          if ($attempts < $this->login_max_attempts) {
            // in allowed attempts credit.
            session('cp', 'login_status', 'auth-error');
            session('cp', 'login_error', 'Failed to login due to mismatched username.');
          }
          else {
            // too many attempts, lock the login form for the next `$this->login_lockout_duration` seconds.
            session('cp', 'login_status', 'too-many-attempts-error');
            session('cp', 'login_time', time());
          }

          session_remove('cp', ['login_id', 'login_username', 'login_gauth_secret']);
        }
      }
      else {
        // failed logging-in attempt: due to failed csrf validation.
        $attempts++;
        session('cp', 'login_status', 'csrf-error');
        session('cp', 'login_error', 'Request is not authorized.');
        session_remove('cp', ['login_id', 'login_username', 'login_gauth_secret']);
      }

      if ($attempts == session('cp', 'login_attempts')) {
        // logging-in attempt is successful, no failed attempts are counted.
        // let's redirect to the next step of the login process.
        redirect('cp/login/use');
      }
      else {
        // increment login attempts counter. let's redirect to cp/login to output errors.
        session('cp', 'login_attempts', $attempts);
        redirect('cp/login');
      }
    }
  }

  public function use() {
    if (__REQUEST__ == 'get') {
      if (session('cp', 'id_cp_user')) {
        redirect('cp');
      }
      else if (!session('cp', 'login_id')) {
        redirect('cp/logout');
      }
      else {
        $loginStatus  = session('cp', 'login_status');
        $loginCases   = [
          'too-many-attempts-error',
          'csrf-error',
          'otp-required',
          'more-auth-required',
          'webauthn-required',
          'password-required',
        ];

        if (in_array($loginStatus, $loginCases)) {
          // regenerate session id to prevent session fixation attacks.
          session_regenerate_id(true);

          // if one of these values are set, allow the user to go back to the /login/use page.
          if (count($this->current_path_array) == 3) {
            session('cp', 'login_status', 'more-auth-required');
            session_remove('cp', ['login_attempts', 'login_time', 'login_error']);
            self::View::render('auth/login-options');
          }
          else if ($this->current_path_array[3] == 'password') {
            if ($loginStatus == 'too-many-attempts-error') {
              if (session('cp', 'login_time') + $this->login_lockout_duration >= time()) {
                // Locking login form out for the `$this->login_lockout_duration` seconds.
                self::View::render('auth/login-freezed');
              }
              else {
                session_remove('cp', ['login_attempts', 'login_time']);
                session('cp', 'login_status', 'password-required');
                self::View::render('auth/password-required');
              }
            }
            else {
              self::View::render('auth/password-required');
              session_remove('cp', 'login_error');
            }
          }
          else if ($this->current_path_array[3] == 'webauthn') {
            $this->auth->set_webauthn()->activeOnly = true;
            $user_creds = $this->auth->get(
              where: ['cred_id_user' => session('cp', 'login_id')],
            );

            if (count($user_creds) == 0) {
              session('cp', 'login_error', 'No passkeys found.');
            }
            else {
              $creds = [
                'userVerification'  => 'required',
                'timeout'           => 60000,
                'challenge'         => base64_encode(random_bytes(32)),
                'rpId'              => parse_url($this->base_path)['host'],
                'allowCredentials'  => array_map(fn($c) => [
                  'type'        => 'public-key',
                  'id'          => $c['cred_credential_id'],
                  'transports'  => ['internal'],
                ], $user_creds),
                // 'authenticatorSelection' => [ // Add this
                //   'authenticatorAttachment' => 'platform',
                //   'requireResidentKey' => true,
                //   'userVerification' => 'required'
                // ],
              ];
              // dpre($creds);

              session('cp', 'webauthn', json_encode($creds));
            }

            self::View::render('auth/login-webauthn');
            session_remove('cp', 'login_error');
          }
          else {
            redirect('cp/logout');
          }
        }
        else {
          redirect('cp/logout');
        }
      }
    }
    else {
      // regenerate session id to prevent session fixation attacks.
      session_regenerate_id(true);

      // reset all previous login errors.
      session_remove('cp', 'login_error');

      // dpre($this->inputs);
      if (isset($this->inputs['option'])) {
        // This is POST from the login-options form.
        if ($this->inputs['option'] == 'gauth') {
          // Google Authenticator login option selected.
          session('cp', 'login_status', session('cp', 'login_gauth_secret') == '' ? 'otp-not-set' : 'otp-required');
          redirect('cp/login/use/otp');
        }
        else if ($this->inputs['option'] == 'webauthn') {
          // webauthn login option selected.
          session('cp', 'login_status', 'webauthn-required');
          redirect('cp/login/use/webauthn');
        }
        else if ($this->inputs['option'] == 'password') {
          // password login option selected.
          session('cp', 'login_status', 'password-required');
          redirect('cp/login/use/password');
        }
        else {
          // invalid login option selected.
          // redirect to the login page.
          // pre("Unknown");
          redirect('cp/logout');
        }
      }
      else if (isset($this->inputs['cp_user_password'])) {
        // This is POST from the password-required form.

        $attempts = session('cp', 'login_attempts');

        if (\Csrf\Csrf::validate($this->inputs['csrf_token'] ?? '')) {
          // Valid CSRF token.
          $login = $this->auth->get(
            where: [
              'cp_user_username' => session('cp', 'login_username'),
              'cp_user_password' => $this->inputs['cp_user_password'],
            ],
          );

          if (count($login) > 0) {
            // password is correct.
            session('cp', 'id_cp_user', session('cp', 'login_id'));
            session_remove('cp', ['login_id', 'login_username', 'login_gauth_secret']);
            redirect('cp');
          }
          else {
            // failed logging-in attempt due to mismatched password.
            $attempts++;

            if ($attempts < $this->login_max_attempts) {
              // in allowed attempts credit.
              session('cp', 'login_error', 'Failed to login due to mismatched password.');
            }
            else {
              // too many attempts, lock the login form out for the next `$this->login_lockout_duration` seconds.
              session('cp', 'login_status', 'too-many-attempts-error');
              session('cp', 'login_time', time());
            }

            session('cp', 'login_attempts', $attempts);
            redirect('cp/login/use/password');
          }
        }
        else {
          // failed logging-in attempt due to failed csrf validation.
          $attempts++;
          session('cp', 'login_error', 'Request is not authorized.');
          redirect('cp/login/use/password');
        }
      }
      else if (file_get_contents('php://input')) {
        $input              = json_decode(file_get_contents('php://input'));

        if ($input->type == 'login-webauthn') {
          $this->auth->set_webauthn()->activeOnly = true;
          $input              = $input->data;
          $clientDataJSON     = $this->base64UrlDecode($input->response->clientDataJSON);
          $authenticatorData  = $this->base64UrlDecode($input->response->authenticatorData);
          $signature          = $this->base64UrlDecode($input->response->signature);
          $webauthn           = new \lbuchs\WebAuthn\WebAuthn($this->rp['name'], $this->rp['id']);
          $webauthnOptions    = json_decode(session('cp', 'webauthn'));
          $id_user            = session('cp', 'login_id');
          $user_creds         = $this->auth->get($id_user);
  
          /**
           *  We did this because theoretically the user cred_credential_id contains +/=
           *    and to match it with the $input->id or $input->rawId: we must modify it by removing those and replace +/ with -_
           *  Actually if still $user_creds returns results, that means: yes, that device is pre-registered.
           *  So, we're able to use $input->rawId because it matches the cred_credential_id
           *    and no need to map it and do more stuff. We've got the information that this cred_credential_id matches $input->rawId.
           */
          $user_creds = array_filter($user_creds, function ($c) use($input) {
            return rtrim(strtr($c['cred_credential_id'], '+/', '-_'), '=') == $input->rawId;
          });
  
          $user_creds = count($user_creds) > 0 ? current($user_creds) : $user_creds;
  
          $verify = $webauthn->processGet(
            $clientDataJSON,
            $authenticatorData,
            $signature,
            base64_decode($user_creds['cred_public_key']),
            base64_decode($webauthnOptions->challenge)
          );
  
          if ($verify) {
            if ($webauthn->getSignatureCounter() == $user_creds['cred_sign_count'] + 1) {
              // this is the new login performed by the real user from his real device.
              $this->auth->webauthn_increment_count($webauthn->getSignatureCounter(), $id_user);
              session('cp', 'id_cp_user', $id_user);
              echo json_encode(['success' => true, 'details' => "Successfully authenticated credentials. Redirecting ..."]);
            }
            else {
              echo json_encode(['success' => false, 'details' => "Authenticating credentials failed."]);
            }
          }
          else {
            echo json_encode(['success' => false, 'details' => "Mismatching credentials."]);
          }
  
          session_remove('cp', 'webauthn');
        }
      }
    }
  }

  public function otp() {
    if (session('cp', 'id_cp_user')) {
      // logged in already.
      redirect('cp');
    }
    else if (!session('cp', 'login_id')) {
      // login process is not started yet.
      redirect('cp/logout');
    }
    else {
      if (!$this->gauth) {
        // Google Authenticator is not enabled.
        redirect('cp/login/use');
      }
      else {
        $loginStatus = session('cp', 'login_status');

        if (__REQUEST__ == 'get') {
          // regenerate session id to prevent session fixation attacks.
          session_regenerate_id(true);

          if ($loginStatus == 'too-many-attempts-error') {
            if (session('cp', 'login_time') + $this->login_lockout_duration >= time()) {
              // Locking login form out for the `$this->login_lockout_duration` seconds.
              self::View::render('auth/login-freezed');
            }
            else {
              session_remove('cp', ['login_attempts', 'login_time']);
              session('cp', 'login_status', 'otp-required');
              self::View::render('auth/otp-required');
            }
          }
          else if ($loginStatus == 'otp-not-set') {
            $secret = random_bytes(20);
            session('cp', 'login_gauth_secret', (new \Selective\Base32\Base32)->encode($secret));
            $this->auth->neutral = ['gauth_secret']; // to tell Ornata to just use 'secret' column to store.
            $this->auth->store(
              inputs: ['cp_user_gauth_secret' => session('cp', 'login_gauth_secret')],
              id: session('cp', 'login_id')
            );

            $data = [
              'img' => \Sonata\GoogleAuthenticator\GoogleQrUrl::generate(
                session('cp', 'login_username'),
                session('cp', 'login_gauth_secret'),
                $this->base_title
              ),
              'secret' => (new \Sonata\GoogleAuthenticator\GoogleAuthenticator)->getCode(session('cp', 'login_gauth_secret')),
            ];

            self::View::render('auth/otp-not-set', $data);
            session('cp', 'login_status', 'otp-required');
          }
          else if ($loginStatus == 'otp-required') {
            // pre("The code now is => " . (new \Sonata\GoogleAuthenticator\GoogleAuthenticator)->getCode(session('cp', 'login_gauth_secret')));
            self::View::render('auth/otp-required');
            session_remove('cp', 'login_error');
          }
          else {
            redirect('cp/logout');
          }
        }
        else {
          if ($loginStatus == 'otp-required') {
            // regenerate session id to prevent session fixation attacks.
            session_regenerate_id(true);
            $attempts = session('cp', 'login_attempts');

            if (\Csrf\Csrf::validate($this->inputs['csrf_token'] ?? '')) {
              // Valid CSRF token.
              $gauth  = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
              $otp    = $this->inputs['otp_code'];

              if ($gauth->checkCode(session('cp', 'login_gauth_secret'), $otp)) {
                session('cp', 'id_cp_user', session('cp', 'login_id'));
                session_remove('cp', ['login_gauth_secret', 'login_page', 'login_username', 'login_attempts', 'login_time']);
                redirect('cp');
              }
              else {
                // failed logging-in attempt due to mismatched OTP code.
                $attempts++;

                if ($attempts < $this->login_max_attempts) {
                  // in allowed attempts credit.
                  session('cp', 'login_error', 'Incorrect OTP code.');
                }
                else {
                  // too many attempts, lock the login form out for the next `$this->login_lockout_duration` seconds.
                  session('cp', 'login_status', 'too-many-attempts-error');
                  session('cp', 'login_time', time());
                }

                session('cp', 'login_attempts', $attempts);
                redirect('cp/login/use/otp');
              }
            }
            else {
              // failed logging-in attempt: due to failed csrf validation.
              $attempts++;
              session('cp', 'login_error', 'Request is not authorized.');
              redirect('cp/login/use/otp');
            }
          }
          else {
            redirect('cp/logout');
          }
        }
      }
    }
  }

  // Cp Users

  public function users_get(int|null $id = null) {
    $this->auth->activeOnly = false;
    $users = $this->auth->get($id);

    if (!$id) {
      self::View::render('settings/users/index', [
        'title' => 'CP Accounts',
        'items' => $users,
      ]);
    }
    else {
      if ($id > 0 and count($users) > 0) {
        self::View::render('settings/users/view', [
          'title' => 'CP Accounts',
          'item'  => $users[0],
        ]);
      }
      else {
        redirect($this->redirectTo);
      }
    }
  }

  public function users_create() {
    self::View::render('settings/users/view', [
      'title' => 'Create CP account',
    ]);
  }

  public function users_store(int|null $id = null) {
    $this->auth->store(inputs: $this->inputs, id: $id);
    redirect("{$this->redirectTo}/{$id}");
  }

  // Cp Users Passkeys

  public function webauthn_get(int|null $id = null) {
    $this->auth->set_webauthn();
    $this->auth->activeOnly = false;
    $creds = $this->auth->get($id, where: ['cred_id_user' => $this->cp_user['id_cp_user']]);

    if (!$id) {
      self::View::render('settings/webauthn/index', [
        'title' => 'Passkeys',
        'items' => $creds,
      ]);
    }
    else {
      if ($id > 0 and count($creds) > 0) {
        $creds = current($creds);
        self::View::render('settings/webauthn/view', [
          'title' => $creds['cred_device_name'],
          'item'  => $creds,
        ]);
      }
      else {
        redirect('cp/settings/webauthn');
      }
    }
  }

  public function webauthn_create() {
    session_regenerate_id(true);

    if (__REQUEST__ == 'get') {
      // remove any previous webauthn session data.
      session_remove('cp', 'webauthn');

      // Generate a challenge: a random string used to ensure the request is unique.
      $challenge = base64_encode(random_bytes(32));

      // Define the User information. This is the user account that the credential will be registered to.
      $user = [
        'id'          => base64_encode($this->cp_user['id_cp_user']), // user ID; this is usually the primary key of the user record.
        'name'        => $this->cp_user['cp_user_username'],                  // Username; this is usually the username or email address.
        'displayName' => $this->cp_user['cp_user_username'],                  // Username; this is usually the username or email address.
      ];

      // Define the public key credential parameters. This is the type of credential to be created.
      $pubKeyCredParams = [
        ['type' => 'public-key', 'alg' => -7],    // -7 represents ES256.
        ['type' => 'public-key', 'alg' => -257],  // -257 represents RS256.
      ];

      $user_creds = $this->auth->set_webauthn()->get(
        where: ['cred_id_user' => $this->cp_user['id_cp_user']],
      );

      $user_creds = array_map(function($cred) {
        return [
          'id'          => $cred['cred_credential_id'], // the credential ID is already `base64` encoded.
          'type'        => 'public-key', // The type of the credential.
          'transports'  => ['internal', /*'usb', 'nfc', 'ble'*/], // Optional: specify the transports supported by the authenticator.
        ];
      }, $user_creds);

      // Create the PublicKeyCredentialCreationOptions array. These options are sent to the browser for the registration process.
      $publicKeyCredentialCreationOptions = [
        'challenge'           => $challenge,        // The random challenge generated earlier.
        'rp'                  => $this->rp,         // Website's details.
        'user'                => $user,             // The current user's details.
        'pubKeyCredParams'    => $pubKeyCredParams, // List of accepted cryptographic algorithms.
        'timeout'             => 60000,             // Timeout (in milliseconds) for the registration process.
        'attestation'         => 'none',            // 'none' means no attestation data is requested.
        'excludeCredentials'  => $user_creds,       // An array where you can list credentials already registered.
        'authenticatorSelection' => [               // Add this
          'authenticatorAttachment' => 'platform',
          'requireResidentKey' => true,
          'userVerification' => 'required'
        ],
      ];

      session('cp', 'webauthn', $publicKeyCredentialCreationOptions);

      self::View::render('settings/webauthn/create', [
        'title' => 'Create Passkey',
        'creds' => json_encode($publicKeyCredentialCreationOptions),
      ]);
    }
    else {
      // Read the raw JSON POST data from the client.
      $input = json_decode(file_get_contents('php://input'));

      // Get the registration options from the session.
      $webauthnOptions = session('cp', 'webauthn');

      // When you sent the registration options, you should have saved them in the session.
      if (!$webauthnOptions) {
        echo json_encode(['success' => false, 'details' => 'Missing registration options.']);
        exit;
      }

      try {
        // Initialize the WebAuthn server.
        $webauthn             = new \lbuchs\WebAuthn\WebAuthn($webauthnOptions['rp']['name'], $webauthnOptions['rp']['id']);

        $attestationObject    = $this->base64UrlDecode($input->response->attestationObject);
        $clientDataJSON       = base64_decode($input->response->clientDataJSON);

        $newCredential        = $webauthn->processCreate($clientDataJSON, $attestationObject, $this->base64UrlDecode($webauthnOptions['challenge']));
        $signatureCounter     = $newCredential->signatureCounter ?? 0;
        $credentialId         = base64_encode($newCredential->credentialId);        // this to be stored in the users_creds table
        $credentialPublicKey  = base64_encode($newCredential->credentialPublicKey); // this to be stored in the users_creds table

        $user_creds = $this->auth->set_webauthn()->get(
          where: [
            'cred_id_user'  => $this->cp_user['id_cp_user'],
            'cred_credential_id'  => $credentialId,
          ],
        );

        if (count($user_creds) > 0) {
          // If the credential is already registered, return an error.
          echo json_encode(['success' => false, 'details' => 'Credential already exists.']);
          exit;
        }
        else {
          // If the credential is not registered, store it in the database.
          $id_cred = $this->auth->set_webauthn()->store([
            'cred_id_user'        => $this->cp_user['id_cp_user'],
            'cred_credential_id'  => $credentialId,
            'cred_public_key'     => $credentialPublicKey,
            'cred_sign_count'     => $signatureCounter,
            'cred_device_name'    => 'My new device',
          ]);

          echo json_encode(['success' => true, 'details' => "Credential registered successfully. Redirecting ...", 'id_cred' => $id_cred]);
        }
      }
      catch (\lbuchs\WebAuthn\WebAuthnException $e) {
        // If verification fails or any error occurs, catch it and return the error.
        echo json_encode(['success' => false, 'details' => $e->getMessage()]);
      }

      session_remove('cp', 'webauthn');
    }
  }

  public function webauthn_store(int|null $id = null) {
    $this->auth->set_webauthn();
    if (count($this->auth->get($id, where: ['cred_id_user' => $this->cp_user['id_cp_user']])) > 0) {
      // valid
      $this->auth->neutral = ['device_name', 'is_active'];
      $this->auth->store($this->inputs, id: $id);
    }

    redirect("/cp/settings/webauthn/{$id}");
  }

}
