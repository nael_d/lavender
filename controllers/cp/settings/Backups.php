<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebController|CpController|ApiController
namespace CpController;

class Backups Extends CP {

  private $model        = '';
  private $redirectTo   = 'cp/settings/backups';
  private $title        = "Backups";
  private $title_single = "";
  private $title_column = '';

  public function __construct() {
    parent::__construct();

    if (!is_dir('.backups/databases')) {
      mkdir('.backups/databases', 0755, true);
      redirect($this->redirectTo);
    }
  }

  public function get() {
    $dirPath = '.backups/databases';
    $fileList = [];
    $iterator = new \FilesystemIterator(
      $dirPath,
      \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::UNIX_PATHS
    );

    foreach ($iterator as $fileInfo) {
      $fileList[] = [
        'filename' => $fileInfo->getFilename(),
        'created_at' => $fileInfo->getCTime(),
        'size' => $fileInfo->getSize(),
      ];
    }

    usort($fileList, function($a, $b) {
      return $b['created_at'] <=> $a['created_at'];
    });

    self::View::render('settings/backups/index', [
      'title' => 'Backups',
      'items' => $fileList,
    ]);
  }

  public function create() {
    $backup = $this->backup(); // form Core

    switch ($backup['status']) {
      case 'ok':
        redirect($this->redirectTo);
        break;
      case 'error':
        switch ($backup['reason']) {
          case 2:
            $this->error(
              details: "Error #2: Command <kbd>mysqldump</kbd> is not found.
                <br />Ensure that it's installed or consult your system administrator.",
              show_btn: true
            );
            break;
          case 126:
            $this->error(details: "Error #126: Permission problem or command not executable. Check file permissions.", show_btn: true);
            break;
          case 128:
            $this->error(details: "Error #128: Invalid argument. Please review your input and try again.", show_btn: true);
            break;
          case 130:
            $this->error(details: "Error #130: Command terminated. Please avoid interruptions during execution.", show_btn: true);
            break;
          case 255:
            $this->error(details: "Error #255: Database access denied. Please check your credentials and ensure you have permission to export the data.", show_btn: true);
            break;
          case 1:
          default:
            $this->error(details: "Error #1: General failure. Please try again later or contact your developer.", show_btn: true);
            break;
        }
      break;
    }
  }
  
  public function delete($backup) {
    unlink(".backups/databases/{$backup}");
    redirect($this->redirectTo);
  }
  
  public function download($backup) {
    redirect(".backups/databases/{$backup}");
  }
  
  public function wipe() {
    foreach (array_diff(scandir('.backups/databases'), ['.', '..']) as $key => $backup) {
      unlink(".backups/databases/{$backup}");
    }

    redirect($this->redirectTo);
  }

}