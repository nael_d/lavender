<?php

namespace CpController;

class Home extends CP {

  public function __construct() {
    parent::__construct();
    $this->load_model(['CpModel\CpTodos', 'CpModel\Maillist']);
  }

  public function index() {
    // regenerate session id to prevent session fixation.
    session_regenerate_id(true);
    session_remove('cp', 'login_status');

    $stats    = [];
    $todos    = new \CpModel\CpTodos;
    $maillist = new \CpModel\Maillist;
    $maillist->activeOnly = true;
    \Cache::enable();
    \Cache::setNamespace('cp/');

    if ($this->cp_user['id_cp_user'] == 1) {
      $ip_info          = \Cache::getOrSet('ip_info', fn() => json_decode(file_get_contents('https://ipinfo.io/json')));
      // $ip_user          = !__LOCALHOST__ ? $_SERVER['REMOTE_ADDR'] : (\Cache::getOrSet('ip_user', fn() => file_get_contents("https://api64.ipify.org")));
      $weather_api      = '1ffbc88e9daf4bccba9193039232710';
      $stats['weather'] = \Cache::getOrSet('weather', fn() => json_decode(file_get_contents("https://api.weatherapi.com/v1/current.json?key={$weather_api}&q={$ip_info->ip}&aqi=yes")) ?? new \stdClass);
      $stats['stats']   = [
        'ip_info'         => (array) $ip_info,
        'server_cpuload'  => round(\Cache::getOrSet('server_cpuload', fn() => server_cpuload()), 2),
        'server_uptime'   => humanize_uptime(\Cache::getOrSet('server_uptime', fn() => server_uptime()) ?? null),
        'server_ramusage' => _filesize(\Cache::getOrSet('server_ramusage', fn() => server_ramusage()), 'mb'),
        'disk_usage'      => round(\Cache::getOrSet('server_used_100', fn() => server_diskusage()['used_100']), 2) . "% (" . (\Cache::getOrSet('server_used_to_total', server_diskusage()['used_to_total'])) . ")",
        'database_usage'  => \Cache::getOrSet('database_usage', $this->getDatabaseSizes()[env('database')]),
        'timezone'        => date_default_timezone_get(),
        'website_status'  => $this->settings->preference_get_value('website-status')->status,
        'scheduled_mails' => count($maillist->get_mails(0)),
      ];
    }


    self::View::render('home/index', [
      'title'   => 'Dashboard',
      'todos'   => $todos->get(where: ['cp_todo_id_user' => $this->cp_user['id_cp_user']]),
      ...$stats,
    ]);
  }

}
