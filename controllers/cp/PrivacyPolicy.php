<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpController;

class PrivacyPolicy Extends Cp {

  private $model;
  private $redirectTo   = 'cp/privacy-policy';
  private $title        = "Privacy policy";

  public function __construct() {
    parent::__construct();
    $this->load_model('CpModel\Settings');
    $this->model = new \CpModel\Settings;
  }

  public function index() {
    self::View::render('privacy-policy/view', [
      'title' => $this->title,
      'item'  => $this->model->preference_get('privacy-policy'),
    ]);
  }

  public function store() {
    $this->model->store(inputs: $_POST, id: 'privacy-policy');
    redirect($this->redirectTo);
  }

}