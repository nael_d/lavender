<?php

require 'core/system/startup.php';

$route = $app->route;

require_once './routes/core.php';
require_once './routes/cronjobs.php';
require_once './routes/schemas.php';
require_once './routes/cp.php';
require_once './routes/api.php';
require_once './routes/web.php';

$route->end();
