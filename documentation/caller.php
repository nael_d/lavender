<?php

$action = strip_tags(trim(strtolower($_GET["action"])));

if ($action == 'version-checker') {
  $localfile  = $_SERVER['DOCUMENT_ROOT'] . "\js\documentation.js";
  $remotefile = "https://gitlab.com/nael_d/ornata/-/raw/master/documentation/js/documentation.js?ref_type=heads&inline=false";

  $remotefile = @file_get_contents($remotefile);
  if (!$remotefile) { echo json_encode(['status' => 'not-connected']); die(); }

  echo json_encode([
    'status' => md5($remotefile) == md5(@file_get_contents($localfile)) ? 'latest' : 'new-version-available',
  ]);
}
else if ($action == 'latest-version') {
  $tags_list = json_decode(@file_get_contents('https://gitlab.com/api/v4/projects/20682065/repository/tags?order_by=updated'));

  if (!$tags_list) { echo json_encode(['status'=> 'not-connected']); die(); }
  echo json_encode(['status' => 'ok', 'result' => $tags_list[0]->name]);
}
