<?php

namespace Schema;

class Maillist extends Schema implements SchemaBlueprint {

  public function __construct() {
    parent::__construct();
  }

  public function install() {
    $this->design_mails();
    $this->design_subs();
  }

  public function design_mails() {
    $this
      ->table('maillist_mails')
      ->prefix('maillist_mail')
      ->integer('id_*')->autoIncrement()->unsigned()->primaryKey()
      ->string('*_title')
      ->longtext('*_message')
      ->string('*_date_created')
      ->string('*_date_updated')->allowNull()
      ->string('*_date_scheduled')
      ->tinyint('*_is_sent')->defaultValue(0)
      ->tinyint('*_is_active')->unsigned()->defaultValue(1)
      ->tinyint('*_is_deleted')->unsigned()->defaultValue(0)
      ->build()
    ;
  }

  public function design_subs() {
    $this
      ->table('maillist_subs')
      ->prefix('maillist_sub')
      ->integer('id_*')->autoIncrement()->unsigned()->primaryKey()
      ->string('*_email')
      ->string('*_date_created')
      ->string('*_date_updated')->allowNull()
      ->string('*_ip')
      ->string('*_useragent')
      ->string('*_sections')->allowNull()
      ->tinyint('*_is_active')->unsigned()->defaultValue(1)
      ->tinyint('*_is_delete')->unsigned()->defaultValue(0)
      ->build()
    ;
  }

}
