<?php

namespace Schema;

class Api extends Schema implements SchemaBlueprint {

  public function __construct() {
    parent::__construct();
  }

  public function install() {
    $this->design();
  }

  public function design() {
    $this
      ->table('apis')
      ->prefix('api')
      ->integer('id_*')->autoIncrement()->unsigned()->primaryKey()
      ->longtext('*_belongs_to')
      ->string('*_key')
      ->string('*_password')->allowNull()
      ->longtext('*_permissions')->allowNull()
      ->string('*_date_created')
      ->string('*_date_updated')->allowNull()
      ->string('*_date_expired')->allowNull()
      ->tinyint('*_is_active')->defaultValue(1)->unsigned()
      ->tinyint('*_is_deleted')->defaultValue(0)->unsigned()
      ->build()
    ;
  }

}