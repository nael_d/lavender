<?php

namespace Schema;

class Tools extends Schema implements SchemaBlueprint {

  public function __construct() {
    parent::__construct();
  }

  public function install() {
    $this->design_todos();
  }

  public function design_todos() {
    $this
      ->table('cp_todos')
      ->prefix('cp_todo')
      ->integer('id_*')->autoIncrement()->unsigned()->primaryKey()
      ->tinyint('*_id_user')->unsigned()
      ->string('*_title')
      ->string('*_date_created')
      ->tinyint('*_is_active')->defaultValue(1)->unsigned()
      ->tinyint('*_is_deleted')->defaultValue(0)->unsigned()
      ->build()
    ;
  }

}