<?php

namespace Schema;

class Slides extends Schema implements SchemaBlueprint {

  public function __construct() {
    # code...
  }

  public function install() {
    $this->design();
  }

  public function design() {
    $this
      ->table('slides')
      ->prefix('slide')
      ->integer('id_*')->autoIncrement()->unsigned()->primaryKey()
      ->longtext('*_title')
      ->string('*_image')->allowNull()
      ->string('*_date_created')
      ->string('*_date_updated')->allowNull()
      ->tinyint('*_is_active')->defaultValue(1)->unsigned()
      ->tinyint('*_is_deleted')->defaultValue(0)->unsigned()
      ->build()
    ;
  }

}