<?php

namespace Schema;

class CpUsers extends Schema implements SchemaBlueprint {

  public function __construct() {
    parent::__construct();
  }

  public function install() {
    $this->design();
    $this->design_creds();
  }

  public function design() {
    $this
      ->table('cp_users')
      ->prefix('cp_user')
      ->integer('id_*')->autoIncrement()->unsigned()->primaryKey()
      ->string('*_username', 50)
      ->string('*_password', 50)
      ->string('*_gauth_secret', 50)->allowNull()
      ->string('*_mobile_number', 50)->allowNull()
      ->string('*_telegram_number', 50)->allowNull()
      ->string('*_date_created')
      ->string('*_date_updated')->allowNull()
      ->tinyint('*_is_darkmode')->defaultValue(0)->unsigned()
      ->tinyint('*_is_active')->defaultValue(1)->unsigned()
      ->tinyint('*_is_deleted')->defaultValue(0)->unsigned()
      ->build()
    ;
    return $this;
  }

  public function design_creds() {
    $this
      ->table('cp_users_creds')
      ->prefix('cred')
      ->integer('id_*')->autoIncrement()->unsigned()->primaryKey()
      ->string('*_id_user', 255)
      ->string('*_credential_id', 255)
      ->string('*_public_key', 255)
      ->integer('*_sign_count')->unsigned()
      ->string('*_device_name', 255)
      ->string('*_date_created')
      ->string('*_date_updated')->allowNull()
      ->tinyint('*_is_active')->defaultValue(1)->unsigned()
      ->tinyint('*_is_deleted')->defaultValue(0)->unsigned()
      ->build()
    ;
    return $this;
  }

  public function write(string $username, string $password) {
    $this
      ->table('cp_users')
      ->prefix('cp_user')
      ->insert(['*_username' => sanitizer($username), '*_password' => sanitizer($password)])
    ;
  }

}