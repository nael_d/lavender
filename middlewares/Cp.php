<?php

namespace Middleware;

class Cp extends \Middleware {

  public function __construct() {
    parent::__construct();
  }

  public function check_login() {
    $routes = [
      '/cp/login/',
      '/cp/login/use/',
      '/cp/login/use/otp/',
      '/cp/login/use/password/',
      '/cp/login/use/webauthn/',
      '/cp/logout/',
    ];

    if (!session('cp', 'id_cp_user')) {
      if (!in_array(app('request')->path, $routes)) {
        if (__REQUEST__ == 'get') {
          redirect('cp/login');
        }
        else {
          echo json_encode(['status' => 'error', 'reason' => 'not-logged-in']);
          die;
        }
      }
    }
  }

  public function check_super_admin() {
    if (session('cp', 'id_cp_user') != '1') {
      redirect('cp/settings');
    }
  }

}