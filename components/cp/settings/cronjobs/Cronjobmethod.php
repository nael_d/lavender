<?php

namespace CpComponent;

class Cronjobmethod extends \Corviz\Crow\Component {
  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }

  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('settings/cronjobs/widgets/_method');
  }
}
