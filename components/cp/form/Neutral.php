<?php

namespace CpComponent;

class Neutral extends \Corviz\Crow\Component {

  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }

  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('layout/partials/form/_neutral');
  }
}
