<?php

namespace CpComponent;

class Select extends \Corviz\Crow\Component {

  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }

  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('layout/widgets/form/_select');
  }
}
