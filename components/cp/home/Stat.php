<?php

namespace CpComponent;

class Stat extends \Corviz\Crow\Component {

  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }
  
  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('home/partials/_stat');
  }
}
