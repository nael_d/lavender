<?php

namespace CpComponent;

class Clockwall extends \Corviz\Crow\Component {

  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }
  
  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('home/widgets/_clockwall');
  }
}