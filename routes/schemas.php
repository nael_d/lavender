<?php

/**
 * To call class in objective way: use ['Class', 'Method']
 * To call class in static way: use 'Class@index'
 */

$route->group('/schemas', function () {
  $this->get('/build', function () {
    $schemas = [...glob('schemas/*.php'), ...glob('schemas/*/*')];
    foreach ($schemas as $schema) {
      $schema_name = pathinfo($schema, PATHINFO_FILENAME);
      (new ("Schema\\{$schema_name}"))->install();
    }
  });

  $this->get('/*', function () {
    die("Installing Schemas through schemas routes is abandoned. Use `schema` task in Ornata CLI.");
  });

  /**
   * This routes are being disabled. To install schemas, use Ornata CLI `ornata schema` command task.
   * Although installing schemas individually is deprecated, let's examine the pre-defined out-dated routes:
   * $this->get('/cp_users', ['Schema\\CpUsers', 'install']);
   * $this->get('/settings', ['Schema\\Settings', 'install']);
   * $this->get('/slides',   ['Schema\\Slides', 'install']);
  */
});