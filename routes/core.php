<?php

$route->get('/lang/{lang}', function ($lang) {
  $langs              = session('core', 'langs');
  $fallback_lang_web  = session('core', 'fallback_lang_web');

  session('web', [
    'current_lang' => in_array($lang, $langs) ? $lang : $fallback_lang_web,
  ]);

  redirect(session('web', 'current_path_full'));
});

$route->group('/core', function () {
  $this->group('/setup', function () {
    $this->post('/install', function () {
      // checking that there's no .env file, otherwise store it as backup
      env_backup();

      ini_set('error_reporting', E_ALL);
      $obj = json_decode(file_get_contents("php://input")); // returns object

      foreach (json_decode($obj->langs) as $language => $lang) {
        if (!is_dir("./i18n/{$lang}")) {
          mkdir("./i18n/{$lang}", 0755);
        }
        $f = fopen("./i18n/{$lang}/ignored", 'w');
        fclose($f);
      }

      $env = '';

      foreach ($obj as $key => $value) {
        if (in_array($key, ['cp_user_username', 'cp_user_password'])) continue;
        $env .= "$key=$value\r\n";
      }

      $env_file = fopen('.env', 'w');
      fwrite($env_file, $env);
      fclose($env_file);

      if ($obj->database != '') {
        $superuser_username = $obj->cp_user_username;
        $superuser_password = $obj->cp_user_password;

        if ($obj->use_encrypia == 'true') {
          Encrypia::setKey($obj->encrypia_key);
          $superuser_username = Encrypia::blind($obj->cp_user_username);
          $superuser_password = Encrypia::blind($obj->cp_user_password);
        }

        if (str_contains($obj->database, '/')) {
          // sqlite3
          if (!is_dir('./databases')) { mkdir('./databases', 0755); }
          if (file_exists('./databases/db.sqlite3')) {
            copy('./databases/db.sqlite3', "./databases/db-backup-" . rand_str() . ".sqlite3");
            file_put_contents('./databases/db.sqlite3', "");
          }
          else {
            $db = fopen('./databases/db.sqlite3', 'w');
            fclose($db);
          }
        }
        else {
          // mysql
        }

        $schemas = [...glob('schemas/*.php'), ...glob('schemas/*/*')];
        foreach ($schemas as $schema) {
          $schema_name = pathinfo($schema, PATHINFO_FILENAME);
          (new("Schema\\{$schema_name}"))->install();
        }
        (new Schema\CpUsers())->write($superuser_username, $superuser_password);
      }

      if (file_exists('views/welcome.php')) {
        rename('views/welcome.php', 'views/welcome-disabled.php');
        echo json_encode(['status' => 'ok']);
      }
      else {
        echo json_encode(['status' => 'error', 'reason' => "Unable to complete Ornata Setup."]);
      }
    });
  });
});
