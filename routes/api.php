<?php

/**
 * To call class in objective way: use ['Class', 'Method']
 * To call class in static way: use 'Class@index'
 */

$route->group('/api', function () {
  $this->group('/v1', function () {
    $this->get('/tables', ['ApiController\Api', 'tables']);
    $this->get('/get',    ['ApiController\Home', 'get']);
  });
});