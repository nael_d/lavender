<?php

/**
 * To call class in objective way: use ['Class', 'Method']
 * To call class in static way: use 'Class@index'
 */

$superuser_routes = [
  '/cp/settings/users',
  '/cp/settings/gauth',
  '/cp/settings/apis',
  '/cp/settings/excel',
  '/cp/settings/backups',
  '/cp/settings/website-status',
  '/cp/settings/reset-database',
];

$route->before('/cp', ['Middleware\Cp', 'check_login']);
foreach ($superuser_routes as $r) {
  $route->before($r,  ['Middleware\Cp', 'check_super_admin']);
}

$route->group('/cp', function () {
  $this->get_post('/login',                       ['CpController\Auth', 'login']);
  $this->get_post('/login/use/otp',               ['CpController\Auth', 'otp']);
  $this->get_post(['/login/use', '/login/use/*'], ['CpController\Auth', 'use']);
  $this->get_post('/logout',                      ['CpController\Auth', 'logout']);

  $this->get('/', ['CpController\Home', 'index']);

  $this->group('/home', function () {
    $this->group('/todos', function () {
      $this->post('/store',     ['CpController\CpTodos',  'todos_store']);
      $this->post('/activity',  ['CpController\CpTodos',  'todos_activity']);
      $this->post('/delete',    ['CpController\CpTodos',  'todos_delete']);
    });
  });

  $this->group('/bulk',  function () {
    $this->post('/',          ['CpController\CP', 'bulk']);
    $this->post('/transmit',  ['CpController\CP', 'transmit']);
    $this->post('/remove',    ['CpController\CP', 'remove']);
  });

  $this->group('/mail', function () {
    $this->get('/',               ['CpController\Mail', 'index']);
    $this->get('/unread',         ['CpController\Mail', 'unread']);
    $this->get('/trash',          ['CpController\Mail', 'trash']);
    $this->get('/(\d+)',          ['CpController\Mail', 'read']);
    $this->get('/(\d+)/delete',   ['CpController\Mail', 'delete_one']);
    $this->get('/(\d+)/restore',  ['CpController\Mail', 'restore_one']);
  });

  $this->group('/maillist', function () {
    $this->get(['/'],                       ['CpController\Maillist', 'index']);
    $this->get(['/(\d+)'],                  ['CpController\Maillist', 'review']);
    $this->get(['/create'],                 ['CpController\Maillist', 'create']);
    $this->post(['/store', '/(\d+)/store'], ['CpController\Maillist', 'store']);
    $this->get(['/sent', 'scheduled'],      ['CpController\Maillist', 'list']);
  });

  $this->group('/settings', function () {
    $this->get('/', ['CpController\Settings', 'index']);

    $this->group('/users', function () {
      $this->get(['/', '/(\d+)'],             ['CpController\Auth', 'users_get']);
      $this->get('/create',                   ['CpController\Auth', 'users_create']);
      $this->post(['/store', '/(\d+)/store'], ['CpController\Auth', 'users_store']);
    });

    $this->group('/backups', function () {
      $this->get('/',                                           ['CpController\Backups', 'get']);
      $this->get('/create',                                     ['CpController\Backups', 'create']);
      $this->get('/wipe',                                       ['CpController\Backups', 'wipe']);
      $this->get('/([a-zA-Z0-9-_]+(\.[a-zA-Z0-9]+)?)/download', ['CpController\Backups', 'download']);
      $this->get('/([a-zA-Z0-9-_]+(\.[a-zA-Z0-9]+)?)/delete',   ['CpController\Backups', 'delete']);
    });

    $this->group('/gauth', function () {
      $this->get('/', ['CpController\Settings', 'gauth']);
    });

    $this->group('/dark-mode', function () {
      $this->get('/', ['CpController\Settings', 'darkmode']);
    });

    $this->group('/apis', function () {
      $this->get(['/', '/(\d+)'],             ['CpController\Apis', 'get']);
      $this->get('/create',                   ['CpController\Apis', 'create']);
      $this->post(['/store', '/(\d+)/store'], ['CpController\Apis', 'store']);
    });

    $this->group('/transfer', function () {
      $this->get('/',             ['CpController\Transfer', 'index']);
      $this->get_post('/import',  ['CpController\Transfer', 'import']);

      $this->group('/export', function () {
        $this->get_post('/',      ['CpController\Transfer', 'export']);
        $this->post('/generate',  ['CpController\Transfer', 'generate']);
      });
    });

    $this->group('/cronjobs', function () {
      $this->get('/',                 ['CpController\Cronjobs', 'index']);
      $this->get('/([a-zA-Z]+)',      fn() => redirect('cp/settings/cronjobs'));
      $this->get('/([a-zA-Z]+)/view', ['CpController\Cronjobs', 'get']);
    });

    $this->group('/website-status', function () {
      $this->get('/',       ['CpController\WebsiteStatus', 'index']);
      $this->post('/store', ['CpController\WebsiteStatus', 'store']);
    });

    $this->group('/reset-database', function () {
      $this->get('/',       ['CpController\ResetDatabase', 'index']);
      $this->get('/hit',    ['CpController\ResetDatabase', 'hit']);
      $this->get('/cancel', ['CpController\ResetDatabase', 'cancel']);
    });

    $this->group('/webauthn', function () {
      $this->get(['/', '/(\d+)'],             ['CpController\Auth', 'webauthn_get']);
      $this->get_post('/create',              ['CpController\Auth', 'webauthn_create']);
      $this->post(['/store', '/(\d+)/store'], ['CpController\Auth', 'webauthn_store']);
    });
  });

  $this->group('/privacy-policy', function () {
    $this->get('/',       ['CpController\PrivacyPolicy', 'index']);
    $this->post('/store', ['CpController\PrivacyPolicy', 'store']);
  });

  $this->group('/terms-of-service', function () {
    $this->get('/',       ['CpController\TermsOfService', 'index']);
    $this->post('/store', ['CpController\TermsOfService', 'store']);
  });

  $this->group('/about-us', function () {
    $this->get('/',       ['CpController\AboutUs', 'index']);
    $this->post('/store', ['CpController\AboutUs', 'store']);
  });

  $this->group('/contact-us', function () {
    $this->get('/',       ['CpController\ContactUs', 'index']);
    $this->post('/store', ['CpController\ContactUs', 'store']);
  });

  $this->group('/slides', function () {
    $this->get(['/', '/(\d+)'],             ['CpController\Slides', 'get']);
    $this->get('/create',                   ['CpController\Slides', 'create']);
    $this->post(['/store', '/(\d+)/store'], ['CpController\Slides', 'store']);
  });

  //

});