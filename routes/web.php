<?php

/**
 * To call class in objective way: use ['Class', 'Method']
 * To call class in static way: use 'Class@index'
 */

$route->get('/', ['WebController\Welcome', 'index']);
